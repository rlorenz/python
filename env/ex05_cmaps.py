'''
May 2013 - JFE 
This script shows how to customise color maps
'''

#import the required libraries
from pylab import *

#load N limitation data from Fisher et al., GBC 2012
#we flip because contour functions put the 0,0 corner in lower-left
nlim=np.loadtxt('./data/gimms_nlim.txt')-1

#replace missing data by nan
nlim[np.where(nlim<0.)]=np.nan

#show()
imshow(nlim,cmap='jet')
colorbar(orientation='horizontal')
title("cmap='jet'")
show()

#create new figure with reversed colormap
figure()
imshow(nlim,cmap=cm.jet_r)
colorbar(orientation='horizontal')
title('cmap=cm.jet_r')
show()

#create new figure with other colorbar
figure()
imshow(nlim,cmap=cm.BuPu)
colorbar(orientation='horizontal')
title('cmap=cm.BuPu')
show()

#create a customised colorbar by sampling an existing

#one can sample a colour map over the interval (0. , 1.)
print cm.jet(.5)

#create a custom colour map by sampling an existing one
mycmap=cm.jet(np.arange(0,.5,.1))
#transform the array of colors into a colour map
mycmap=mpl.colors.ListedColormap(mycmap)

figure()
imshow(nlim,cmap=mycmap)
colorbar(orientation='horizontal',ticks=[0,20,40,60,80,100],drawedges=True)
title('cmap=mycmap')
show()

#create a very custome colour map
mycmap2=[cm.jet(.4),'m','silver','#CC0033']
mycmap2=mpl.colors.ListedColormap(mycmap2)
figure()
imshow(nlim,cmap=mycmap2)
colorbar(orientation='horizontal',ticks=[0,25,50,75,100],drawedges=True)
title('cmap=mycmap2')
show()




