'''
May 2013 - JFE 
This script presents the subplot function: how to create subplots
'''

#import the required libraries
from pylab import *

#create a figure object
fig=figure()

#create subplots on grid 2x2

for pltno in range(1,5,1):
  ax=fig.add_subplot(2,2,pltno)
  
#add subplot number
  midx=np.mean(ax.get_xlim())
  midy=np.mean(ax.get_ylim())

  ax.text(midx,midy,str(pltno))


#set a title on ax
ax.set_title('Active axis')

fig.show()

#one can define the size of the figure 
fig1=figure(figsize=(10,10))

axes=[]
for pltno in range(1,26):
  axes.append(fig1.add_subplot(5,5,pltno))
  

for ii,axx in enumerate(axes):
  midx=np.mean(axx.get_xlim())
  midy=np.mean(axx.get_ylim())

  axx.text(midx,midy,'I can still plot something here',ha='center',va='center')

fig1.show()

###
# Use help(ax) to find more on available methods 
###
