'''
May 2013 - JFE 
This script presents the subplot function: how to plot things
'''

#import the required libraries
from pylab import *


#load N limitation data from Fisher et al., GBC 2012
nlim=np.loadtxt('./data/gimms_nlim.txt')
#replace missing data by nan
nlim[np.where(nlim==0.)]=np.nan

#load fossil-fuel emissions data from CDIAC - skipping the explanation header
cdiac=np.loadtxt('./data/global.1751_2009.dat',skiprows=34)



#plotting part

fig=figure(figsize=(10,10))
#fig.subplots_adjust(hspace=.05)
axghg=fig.add_subplot(2,1,1)

#
# All previous plotting and style options are methods of the ax object
#

#plot time series
axghg.plot(cdiac[:,0],cdiac[:,1],color='black',lw=4,label='Total CO$_2$ emissions',ls='-')

#create a shaded area
axghg.fill_between(cdiac[:,0],cdiac[:,1],cdiac[:,2],color='silver')
axghg.fill_between(cdiac[:,0],cdiac[:,2],color='gray')

#put axes labels, title and grid
axghg.set_xticks(range(1750,2010,10),minor=True)
axghg.set_xticks(range(1750,2010,50))

#change scale of y labels
lbls=(axghg.get_yticks()/1000.).astype('S')
axghg.set_yticklabels(lbls)


axghg.set_ylabel('Emissions [Pg C a$^{-1}$]')
axghg.grid(True)

#set axis limits
axghg.set_xlim(1750,2009)

#draw legend
leg=axghg.legend(loc='upper left').draw_frame(False)

############################
axnlim=fig.add_subplot(2,1,2)
im=axnlim.imshow(nlim,cmap=cm.BrBG_r)

axnlim.set_yticks(range(0,361,20),minor=True)
axnlim.set_yticks(range(0,361,60))
axnlim.set_yticklabels(['90$\degree$N','60$\degree$N','30$\degree$N','0$\degree$','30$\degree$S','60$\degree$S','90$\degree$S'])

axnlim.set_xticks(range(0,721,20),minor=True)
axnlim.set_xticks(range(0,721,120))
axnlim.set_xticklabels(['180$\degree$','120$\degree$W','60$\degree$W','0$\degree$','60$\degree$E','120$\degree$E','180$\degree$'])

#create small axis for colorbar - define lower left corner, width and heigth, in relative dim of the figure
cax=fig.add_axes([.90,.05,.03,.45])
clb=colorbar(im,cax=cax)
#cax.set_title('NP limitation')
clb.set_label('NP limitation [%]')
fig.show()
