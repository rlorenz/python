'''
May 2013 - JFE 
This script presents some common graphical options.
'''

#import the required libraries
import matplotlib.pyplot as plt
import numpy as np


#load fossil-fuel emissions data from CDIAC - skipping the explanation header
cdiac=np.loadtxt('./data/global.1751_2009.dat',skiprows=34)

plt.plot(cdiac[:,0],cdiac[:,1],'k-')
plt.plot(cdiac[:,0],cdiac[:,1],'r--',lw=6,label='CDIAC data')

plt.xlabel('Year')
plt.ylabel('Emissions [Tg C a$^{-1}$]')
plt.grid(True)


leg=plt.legend(loc='upper left')

leg.draw_frame(False)


#display the figure
plt.show()
