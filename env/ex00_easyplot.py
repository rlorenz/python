'''
May 2013 - JFE 
This script presents an easy example of drawing a curve with matplotlib.
'''

#import the required libraries
import matplotlib.pyplot as plt
import numpy as np


#load fossil-fuel emissions data from CDIAC - skipping the explanation header
cdiac=np.loadtxt('./data/global.1751_2009.dat',skiprows=34)

plt.plot(cdiac[:,0],cdiac[:,1])


#display the figure
plt.show()
plt.savefig('fig1.pdf')
plt.savefig('fig1.eps')
plt.savefig('fig1.png',dpi=300)
plt.savefig('fig1.jpg')
