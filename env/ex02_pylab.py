'''
May 2013 - JFE 
This script presents pylab and how to do shaded areas.
'''

#import the required libraries
from pylab import *


#load fossil-fuel emissions data from CDIAC - skipping the explanation header
cdiac=np.loadtxt('./data/global.1751_2009.dat',skiprows=34)

#plot the total
plot(cdiac[:,0],cdiac[:,1],color='black',lw=4,label='Total',ls='-.')

#create a shaded area
fill_between(cdiac[:,0],cdiac[:,1],cdiac[:,2],color='silver')
fill_between(cdiac[:,0],cdiac[:,2],color='gray')

#put axes labels, title and grid
xlabel('Year',size='medium')
ylabel('Emissions [Tg C a$^{-1}$]',size='x-small')
title('Fossil-fuel emissions',size='xx-large')
grid(True)

#set axis limits
xlim(1750,2009)
#draw legend
leg=legend(loc='upper left')

leg.draw_frame(False)


#display the figure
show()
