'''
May 2013 - JFE 
This script presents the imshow function
'''

#import the required libraries
from pylab import *


#load N limitation data from Fisher et al., GBC 2012
nlim=np.loadtxt('./data/gimms_nlim.txt')

#replace missing data by nan
nlim[np.where(nlim==0.)]=np.nan

#plot the array
imshow(nlim)

#insert a colorbar
colorbar()

#show()
show()


###
# Do not forget to check help(imshow) to get details on different options
###
