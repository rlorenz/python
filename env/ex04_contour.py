'''
May 2013 - JFE 
This script presents the contour and contourf functions
'''

#import the required libraries
from pylab import *


#load N limitation data from Fisher et al., GBC 2012
#we flip because contour functions put the 0,0 corner in lower-left
nlim=np.flipud(np.loadtxt('./data/gimms_nlim.txt'))

#replace missing data by nan
nlim[np.where(nlim==0.)]=np.nan

#show()
show()

contourf(nlim,levels=[0,25,50,75,100])
colorbar(orientation=False)
#plot the array
contour(nlim,levels=[0,25,50,75,100],colors='black')

show()
