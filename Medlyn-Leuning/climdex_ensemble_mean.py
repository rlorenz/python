#!/usr/bin/python
'''
# File Name : climdex_ensemble_mean.py
# Creation Date : 12/11/2014
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : calculate ensemble mean of all climdex indices
#		for multi-ensemble runs
'''
###-------------------------------------------------------
import numpy as np
import netCDF4 as ncdf
from scipy.io import netcdf_file
import os
from subprocess import call

##---------------------##
## user specifications ##
##-------------------- ##
exp='LEUNING'
year_start=2012
year_end=2099
ensembles=['NC', 'NC1', 'NC2', 'NC3', 'NC4']

if exp=="LEUNING":
    #runids=['vaeha','vafna','vafoa','vafpa','vafqa']
    runids=['vaehc','vafnc','vafoc','vafpc','vafqc']
elif exp=="MEDLYN":
    #runids=['vaehb','vafnb','vafob','vafpb','vafqb']
    runids=['vaehd','vafnd','vafod','vafpd','vafqd']

index=['CDD', 'CSDI', 'CWD', 'DTR', 'FD', 'GSL', 'ID', 'PRCPTOT', 'R10mm', 'R20mm', 'R95p', 'R99p', 'Rnnmm', 'Rx1day', 'Rx5day', 'SDII', 'SU', 'TN10p', 'TN50p', 'TN90p', 'TNn', 'TNx', 'TR', 'TX10p', 'TX50p', 'TX90p', 'TXn', 'TXx', 'WSDI']

indir0='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/'+ensembles[0]+'/climdex_index/'
indir1='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/'+ensembles[1]+'/climdex_index/'
indir2='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/'+ensembles[2]+'/climdex_index/'
indir3='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/'+ensembles[3]+'/climdex_index/'
indir4='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/'+ensembles[4]+'/climdex_index/'

odir='/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn-RCP85/'+exp+'/ENS_MEAN/climdex_index/'
if (os.access(odir,os.F_OK)==False):
            os.makedirs(odir)

call("module load cdo",shell=True)
##---------------------##

for i in range(len(index)):
    call("cdo ensmean "+indir0+runids[0]+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc ' +indir1+runids[1]+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc ' +indir2+runids[2]+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc ' +indir3+runids[3]+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc ' +indir4+runids[4]+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc ' +odir+exp+'_rcp85_readprc_'+str(year_start)+'-'+str(year_end)+'_'+index[i]+'.nc',shell=True)
