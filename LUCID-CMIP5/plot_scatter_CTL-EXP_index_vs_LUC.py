#!/usr/bin/python

'''
File Name : plot_scatter_CTL-EXP_index_vs_LUC.py
Creation Date: 2414 Oct 2015
Last Modified: 
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from LUCID-CMIP5 project
Downloaded to storm servers: /srv/ccrc/data44/z3441306/LUCID-CMIP5/
by Ruth Lorenz

5 GCMs are : CanESM2, HadGEM2-ES, IPSL-CM5A-LR, MIROC-ESM, MPI-ESM-LR
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: L2A26, L2A85
(see Brovkin et al. 2013, J.Clim for more info)
This script compares rcp's (which is either rcp26 or rcp85)
and LUCID experiments (L2A26 or L2A85) in one time period

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data44/z3441306/LUCID-CMIP5/$model/$experiment/climdex_index/*.nc

call within outdir because of pdfcrop!
'''

# Load modules for this script
import sys
import numpy as np
import netCDF4 as nc
from scipy import stats
import matplotlib.pyplot as plt
import pylab

###
# Define input
###
indir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
outdir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5_plots/ENS_MEAN/'
#models = ['CanESM2', 'HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
models = [ 'HadGEM2-ES','IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
#models = ['CanESM2']
experiments = ['rcp26','L2A26']
#experiments = ['rcp85','L2A85']
indices = ['TX90p','TN90p','TNn','TXx','WSDI','Rx1day','Rx5day','R95p','R99p','R10mm','CDD','CWD']
units=['%','%','$^\circ$C','$^\circ$C','days','mm','mm','mm','mm','days','days','days']

# start and end of 1st run in analysis
R0_start = 2005
R0_end = 2100
# start and end of 2nd run in analysis
R1_start = 2005
R1_end = 2100
# define analysis start and end year
A_start = 20900100
A_end = 20990100

#choose significance level for KS-test
sig = 0.05
#define format of figures
plottype = ".pdf"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    diff_list = []
    diff_trop_list = []
    diff_temp_list = []
    diff_bor_list = []
    luc_list = []
    luc_trop_list = []
    luc_temp_list = []
    luc_bor_list = []
    # Loop over models to read all data
    for mod in range(len(models)):
	###
        # Load netcdf files
        if (models[mod]=="HadGEM2-ES") and (experiments[1]=="L2A26"):
		R1_end = 2099
	else:
		R1_end = 2100
	path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_r1i1p1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_r1i1p1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'.nc'
        # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
        ifile = nc.Dataset(path0)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time0 = ifile.variables['time'][:]
        var0 = ifile.variables['Annual'][(time0>=A_start) & (time0<=A_end),:,:]
        # print information about variable
        #print type(var0)
        #print var0.shape
	#print ifile.variables['Annual'].dimensions
	#print time0
        # read Annual value of ETCCDI Index for 2nd run (expA)
        ifile = nc.Dataset(path1)
        lats1 = ifile.variables['lat'][:]
        lons1 = ifile.variables['lon'][:]
	time1 = ifile.variables['time'][:]
        varA = ifile.variables['Annual'][(time1>=A_start) & (time1<=A_end),:,:]
	#make sure both experiments are on the same grid
	if lats.shape != lats1.shape:
		print 'latitudes are different'
		sys.exit
	else:
		del lats1
	if lons.shape != lons1.shape:
		print 'longitudes are different'
		sys.exit
	else:
		del lons1

	ntim0 = var0.shape[0]
        ntim1 = varA.shape[0]
	if ntim0 != ntim1:
		print 'time periods are different! ntim0=' +ntim0+ ' ntim1='+ntim1
		sys.exit

        # read land use change mask
        path_luc = indir + models[mod] +'/'+ experiments[0]+'/treeFrac_change_'+models[mod]+'_'+experiments[0]+'_2099-2006.nc'
        #print path_luc
        ifile = nc.Dataset(path_luc)
        luc=ifile.variables['LUC'][:]

        #calculate difference between ctl and experiments
        diffA = var0 - varA

    	#average data over time
    	var0_avg = np.ma.mean(var0, axis=0)
    	varA_avg = np.ma.mean(varA, axis=0)
    	diffA_avg = np.ma.mean(diffA, axis=0)
        #print varA_avg.shape, diffA_avg.shape

    	#test if difference is statistically significant
    	#ks_2samp gives 2 values: KS statistic and two tailed p-value
    	#if p-value is below signigicant level we can reject the hypothesis
    	#that the distributions of the two samples are the same
   	ks_A = np.ndarray((2,len(lats),len(lons)))
    	H_A = np.ndarray((len(lats),len(lons)))
    	for x in range(len(lats)):
        	for y in range(len(lons)):
    			ks_A[:,x,y] = stats.ks_2samp(var0[:,x,y], varA[:,x,y])
    		        if ks_A[1,x,y] < sig:
    		            H_A[x,y] = 1
    		        else:
    		            H_A[x,y] = 0

        #mask data where not statistically significant
        diffA_avg_sig = np.ma.masked_where(H_A==0, diffA_avg)

        ind_trop = np.where((lats < 30) & (lats > -30))[0]
        ind_temp = np.where((lats < -30) & (lats > -60) | (lats < 60) & (lats > 30))[0]
        #ind_tempN = np.where((lats < 60) & (lats > 30))[0]
        ind_bor = np.where((lats > 60))[0]

        diffA_avg_sig_trop = diffA_avg_sig[ind_trop,:]
        diffA_avg_sig_temp = diffA_avg_sig[ind_temp,:]
        #diffA_avg_sig_tempN = diffA_avg_sig[ind_tempN,:]
        diffA_avg_sig_bor = diffA_avg_sig[ind_bor,:]

        #mask data where luc very small
        luc_mask = np.ma.masked_where(abs(luc)<=5, luc)
        luc_mask_trop = luc_mask[ind_trop,:]
        luc_mask_temp = luc_mask[ind_temp,:]
        luc_mask_bor = luc_mask[ind_bor,:]
        #store data for each model in list
        diff_list.append(diffA_avg_sig)
        diff_trop_list.append(diffA_avg_sig_trop)
        diff_temp_list.append(diffA_avg_sig_temp)
        #diff_tempN_list.append(diffA_avg_sig_tempN)
        diff_bor_list.append(diffA_avg_sig_bor)

        luc_list.append(luc_mask)
        luc_trop_list.append(luc_mask_trop)
        luc_temp_list.append(luc_mask_temp)
        luc_bor_list.append(luc_mask_bor)
    #print diff_list, luc_list

    #plot scatter index versus luc magnitude
    plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_scatter_diff_index_vs_luc_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' LUC '+str(A_start)[:4]+'-'+str(A_end)[:4]

    plt.scatter(luc_list[0],diff_list[0],color='b',marker='o',label=models[0])
    plt.scatter(luc_list[1],diff_list[1],color='c',marker='v',label=models[1])
    plt.scatter(luc_list[2],diff_list[2],color='m',marker='*',label=models[2])
    plt.scatter(luc_list[3],diff_list[3],color='k',marker='s',label=models[3])

    plt.title(title)
    plt.xlabel('land use change magnitude (change in forest)')
    plt.ylabel('change in '+indices[ind])

    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    #plt.show()
    plt.savefig(plotname+plottype)
    plt.close()

    #plot only tropical grid points
    plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_scatter_diff_trop_index_vs_luc_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' tropical LUC '+str(A_start)[:4]+'-'+str(A_end)[:4]

    plt.scatter(luc_trop_list[0],diff_trop_list[0],color='b',marker='o',label=models[0])
    plt.scatter(luc_trop_list[1],diff_trop_list[1],color='c',marker='v',label=models[1])
    plt.scatter(luc_trop_list[2],diff_trop_list[2],color='m',marker='*',label=models[2])
    plt.scatter(luc_trop_list[3],diff_trop_list[3],color='k',marker='s',label=models[3])

    plt.title(title)
    plt.xlabel('land use change magnitude (change in tropical forest)')
    plt.ylabel('change in '+indices[ind])

    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    #plt.show()
    plt.savefig(plotname+plottype)
    plt.close()

    #plot only temperate grid points
    plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_scatter_diff_temp_index_vs_luc_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' temperate LUC '+str(A_start)[:4]+'-'+str(A_end)[:4]

    plt.scatter(luc_temp_list[0],diff_temp_list[0],color='b',marker='o',label=models[0])
    plt.scatter(luc_temp_list[1],diff_temp_list[1],color='c',marker='v',label=models[1])
    plt.scatter(luc_temp_list[2],diff_temp_list[2],color='m',marker='*',label=models[2])
    plt.scatter(luc_temp_list[3],diff_temp_list[3],color='k',marker='s',label=models[3])

    plt.title(title)
    plt.xlabel('land use change magnitude (change in temperate forest)')
    plt.ylabel('change in '+indices[ind])

    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    #plt.show()
    plt.savefig(plotname+plottype)
    plt.close()

    #plot only boreal grid points
    plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_scatter_diff_bor_index_vs_luc_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' boreal LUC '+str(A_start)[:4]+'-'+str(A_end)[:4]

    plt.scatter(luc_bor_list[0],diff_bor_list[0],color='b',marker='o',label=models[0])
    plt.scatter(luc_bor_list[1],diff_bor_list[1],color='c',marker='v',label=models[1])
    plt.scatter(luc_bor_list[2],diff_bor_list[2],color='m',marker='*',label=models[2])
    plt.scatter(luc_bor_list[3],diff_bor_list[3],color='k',marker='s',label=models[3])

    plt.title(title)
    plt.xlabel('land use change magnitude (change in boreal forest)')
    plt.ylabel('change in '+indices[ind])

    leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
    leg.draw_frame(False)

    #plt.show()
    plt.savefig(plotname+plottype)
    plt.close()
