#!/usr/bin/python
'''
File Name : calc_LUC_signal_allGCM.py
Creation Date : 10-06-2015
Last Modified : Wed 10 Jun 2015 10:26:55 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : calculate land use change signal to be able to screen gridpoints
	 were all models experince the same change, e.g. deforestation


'''

# Load modules for this script
#from scipy.io import netcdf_file
import numpy as np
import datetime as dt
import netCDF4 as nc
from netcdftime import utime
###
# Define input
###
indir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
outdir0 = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
#models = ['CanESM2', 'HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
models = ['HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM','MPI-ESM-LR']
#models = ['CanESM2']
experiments = ['rcp26','L2A26']
#experiments = ['rcp85','L2A85']
lu = 'treeFrac'

# define analysis start and end year
A_start = 2006
A_end = 2099

# Loop over models to read all data
for mod in range(len(models)):
	outdir = outdir0 + models[mod] +'/'+experiments[0]
	# Load netcdf files
	if (models[mod]=="HadGEM2-ES"):
		R0_start = 200601
		R0_end = 209912
	elif (models[mod]=="CanESM2"):
		R0_start = 2005
		R0_end = 2100
	elif (models[mod]=="IPSL-CM5A-LR"):
		R0_start = 200601
		R0_end = 230012		
	else:
		R0_start = 200601
		R0_end = 210012
		
	if (models[mod]=="CanESM2"):
		lu_var = 'FRAC'
		lat_var = 'lat'
		lon_var = 'lon'
		time_var = 'TIME'
	else:
		lu_var = 'treeFrac'
		lat_var = 'lat'
		lon_var = 'lon'
		time_var = 'time'
	path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ lu+'_'+ models[mod]+'_'+experiments[0]+'_r1i1p1_'+str(R0_start)+'-'+str(R0_end)+'_remapcon2_1x1.nc'

        # read data from netcdf, read lats, lons and time as well
        ifile = nc.Dataset(path0)
        lat = ifile.variables[lat_var][:]
        lon = ifile.variables[lon_var][:]
        time0 = ifile.variables[time_var]
	print ifile.variables[lu_var].dimensions
	print time0, min(time0), max(time0)
	if (models[mod]=="CanESM2"):
		years=np.floor(time0[:])
		print years
	else:
		cdftime = utime(time0.units)
		dates=cdftime.num2date(time0[:])
		years=np.asarray([dates[i].year for i in xrange(len(dates))])
	var0 = ifile.variables[lu_var][(years>=A_start) & (years<=A_end),:,:]
	print var0.shape
	if (models[mod]=="CanESM2"): #from fraction into percent 
		var0=var0*100
	# calculate land use change from 2006 to 2100,
	# which is equivalent to difference between rcp??-L2A??
	luc = var0[var0.shape[0]-1,:,:]-var0[0,:,:]
	print luc.shape
	# only include areas where luc>5%
	luc[abs(luc)<5]=-9999
	# find indices where afforestation
	luc_mask_aff = np.ma.copy(luc)
	luc_mask_aff[luc_mask_aff<=0]=-9999
	# find indices where deforestation
	luc_mask_def = np.ma.copy(luc)
	luc_mask_def[luc_mask_def>=0]=-9999
	
	# save luc mask as netcdf
	########### WRITTING OUT NCFILES#################
	print '%s/%s_change_%s_%s_%s-%s_remapcon2_1x1.nc'%(outdir,lu,models[mod],experiments[0],A_end,A_start)
        fout=nc.Dataset('%s/%s_change_%s_%s_%s-%s_remapcon2_1x1.nc'%(outdir,lu,models[mod],experiments[0],A_end,A_start),mode='w')
        fout.createDimension('lat',lat.shape[0])
        fout.createDimension('lon',lon.shape[0])

        latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
        setattr(latout,'Longname','Latitude')
        setattr(latout,'units','degrees_north')

        lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
        setattr(lonout,'Longname','Longitude')
        setattr(lonout,'units','degrees_east')

        LUCout=fout.createVariable('LUC','f8',('lat','lon'),fill_value=-9999)
        setattr(LUCout,'Longname','TreeFrac Land Use Change')
        setattr(LUCout,'units','%')
        setattr(LUCout,'description','LUC<5% mask from 2006-2099 in '+experiments[0])

        AFFout=fout.createVariable('LUC_AFF','f8',('lat','lon'),fill_value=-9999)
        setattr(AFFout,'Longname','Afforestation')
        setattr(AFFout,'units','%')
        setattr(AFFout,'description','Afforestation mask from 2006-2099 in '+experiments[0])

        DEFout=fout.createVariable('LUC_DEF','f8',('lat','lon'),fill_value=-9999)
        setattr(DEFout,'Longname','Deforestation')
        setattr(DEFout,'units','%')
        setattr(DEFout,'description','Deforestation mask from 2006-2099 in '+experiments[0])

        latout[:]=lat[:]
        lonout[:]=lon[:]
	LUCout[:]=luc[:]
	AFFout[:]=luc_mask_aff[:]
	DEFout[:]=luc_mask_def[:]
	
	# Set global attributes
        setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
        setattr(fout,"contact","r.lorenz@unsw.edu.au")
        setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
        setattr(fout,"Script","calc_LUC_signal_allGCM_remapcon.py")
        setattr(fout,"Input files located in:", indir)
        fout.close()
