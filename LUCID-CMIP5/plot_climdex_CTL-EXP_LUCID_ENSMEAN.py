#!/usr/bin/python

'''
File Name : plot_climdex_CTL-EXP_LUCID_ENSMEAN.py
Creation Date: 24 Oct 2014
Last Modified: 11/05/2015
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from LUCID-CMIP5 project
Downloaded to storm servers: /srv/ccrc/data44/z3441306/LUCID-CMIP5/
by Ruth Lorenz

5 GCMs are : CanESM2, HadGEM2-ES, IPSL-CM5A-LR, MIROC-ESM, MPI-ESM-LR
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: L2A26, L2A85
(see Brovkin et al. 2013, J.Clim for more info)
This script compares rcp's (which is either rcp26 or rcp85)
and LUCID experiments (L2A26 or L2A85) in one time period

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data44/z3441306/LUCID-CMIP5/$model/$experiment/climdex_index/*_remapcon2_1x1.nc
these files are already remapped to common grid for all models using "cdo remapcon2"
We remap onto 1x1 but plot contours instead of pixels

call within outdir because of pdfcrop!
'''

# Load modules for this script
import pdb 
import sys
#from scipy.io import netcdf_file
from scipy import stats
import numpy as np
import netCDF4 as nc
import utils
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
outdir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5_plots/ENS_MEAN/'
#models = ['CanESM2', 'HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
models = ['HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
experiments = ['rcp26','L2A26']
#experiments = ['rcp85','L2A85']
indices = ['TX90p','TN90p','TNn','TXx','WSDI','Rx1day','Rx5day','R95p','R99p','R10mm','CDD','CWD']
units=['%','%','$^\circ$C','$^\circ$C','days','mm','mm','mm','mm','days','days','days']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 2005
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 2005
R1_end = 2100
# define analysis start and end year
A_start = 20700100
A_end = 20990100

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # Load netcdf files
        if (models[mod]=="HadGEM2-ES") and (experiments[1]=="L2A26"):
		R1_end = 2099
	else:
		R1_end = 2100
	path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_r1i1p1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'_remapcon2_1x1.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_r1i1p1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'_remapcon2_1x1.nc'
        # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
        ifile = nc.Dataset(path0)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time0 = ifile.variables['time'][:]
        var0 = ifile.variables['Annual'][(time0>=A_start) & (time0<=A_end),:,:]
        # print information about variable
        #print type(var0)
        #print var0.shape
        #print ifile.variables['Annual'].dimensions
        # read Annual value of ETCCDI Index for 2nd run (expA)
        ifile = nc.Dataset(path1)
	lats1 = ifile.variables['lat'][:]
        lons1 = ifile.variables['lon'][:]
        time1 = ifile.variables['time'][:]
        varA = ifile.variables['Annual'][(time1>=A_start) & (time1<=A_end),:,:]
        ###
        #make sure both experiments are on the same grid
        if lats.shape != lats1.shape:
                print 'latitudes are different'
                sys.exit
        else:
                del lats1
        if lons.shape != lons1.shape:
                print 'longitudes are different'
                sys.exit
        else:
                del lons1

	# check that time periods are the same
        ntim0 = var0.shape[0]
        ntim1 = varA.shape[0]
        if ntim0 != ntim1:
                print 'time periods are different! ntim0=' +str(ntim0)+ ' ntim1='+str(ntim1)
                sys.exit

        # read land use change mask
        path_luc = indir + models[mod] +'/'+ experiments[0]+'/treeFrac_change_'+models[mod]+'_'+experiments[0]+'_2099-2006_remapcon2_1x1.nc'
        #print path_luc
        ifile = nc.Dataset(path_luc)
        luc_mask=ifile.variables['LUC'][:]
        print type(luc_mask)
        #def_mask=ifile.variables['LUC_DEF']
        #print type(def_mask), def_mask.shape,min(min(def_mask))
        #aff_mask=ifile.variables['LUC_AFF']
        #print type(aff_mask), aff_mask.shape,min(min(aff_mask))
    	#calculate difference between ctl and experiments
    	diffA = var0 - varA

    	#average data over time to plot map
	if mod==0:
		#var0_avg=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		#varA_avg=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		#diffA_avg=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		var0_avg_def=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		varA_avg_def=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		diffA_avg_def=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		var0_avg_aff=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		varA_avg_aff=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
		diffA_avg_aff=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
    	var0_avg = np.ma.mean(var0, axis=0)
    	varA_avg = np.ma.mean(varA, axis=0)
    	diffA_avg = np.ma.mean(diffA, axis=0)

        #mask by LUC mask for deforested areas
        diffA_avg_def[mod,:,:]=np.ma.masked_where(luc_mask>=-5,diffA_avg)
        #print diffA_avg_def[mod,:,:]
        varA_avg_def[mod,:,:]=np.ma.masked_where(luc_mask>=-5,varA_avg)
        var0_avg_def[mod,:,:]=np.ma.masked_where(luc_mask>=-5,var0_avg)
        #mask by LUC mask for afforested areas
        diffA_avg_aff[mod,:,:]=np.ma.masked_where(luc_mask<=5,diffA_avg)
        varA_avg_aff[mod,:,:]=np.ma.masked_where(luc_mask<=5,varA_avg)
        var0_avg_aff[mod,:,:]=np.ma.masked_where(luc_mask<=5,var0_avg)

    print type(varA_avg_def), varA_avg_def.shape,#varA_avg_def
    print type(diffA_avg_def), diffA_avg_def.shape

    #calculate ensemble mean
    var0_ens_avg_def = np.ma.mean(var0_avg_def, axis=0)
    varA_ens_avg_def = np.ma.mean(varA_avg_def, axis=0)
    diffA_ens_avg_def = np.ma.mean(diffA_avg_def, axis=0)

    var0_ens_avg_aff = np.ma.mean(var0_avg_aff, axis=0)
    varA_ens_avg_aff = np.ma.mean(varA_avg_aff, axis=0)
    diffA_ens_avg_aff = np.ma.mean(diffA_avg_aff, axis=0)

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    #ks_A = np.ndarray((2,len(lats),len(lons)))
    #H_A = np.ndarray((len(lats),len(lons)))
    #for x in range(len(lats)):
    #    for y in range(len(lons)):
    #        ks_A[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varA_ens_tp[:,x,y])
    #        if ks_A[1,x,y] < sig:
    #            H_A[x,y] = 1
    #        else:
    #            H_A[x,y] = 0

    # check where at least 4 out of 5 models agree on sign of change
    def_agree_A=np.ndarray((len(lats),len(lons)))
    aff_agree_A=np.ndarray((len(lats),len(lons)))
    for x in range(len(lats)):
    	for y in range(len(lons)):
            def_A = sum(np.ma.count(np.ma.where(diffA_avg_def[:,x,y]<0),0))
            aff_A = sum(np.ma.count(np.ma.where(diffA_avg_aff[:,x,y]>0),0))
       	    if def_A >= 4:
                def_agree_A[x,y]=1
            else:
            	def_agree_A[x,y]=0
       	    if aff_A >= 4:
                aff_agree_A[x,y]=1
            else:
            	aff_agree_A[x,y]=0

    #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    print type(mask_ind),mask_ind.shape
    masked_lats = lats[mask_ind]

    masked_varA_def = varA_ens_avg_def[mask_ind,:]
    masked_varA_aff = varA_ens_avg_aff[mask_ind,:]

    print type(diffA_ens_avg_def),diffA_ens_avg_def.shape
    masked_diffA_def = diffA_ens_avg_def[mask_ind,:]
    masked_HA_def = def_agree_A[mask_ind,:]

    masked_diffA_aff = diffA_ens_avg_aff[mask_ind,:]
    masked_HA_aff = aff_agree_A[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if A_end < 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif indices[ind] == 'R95p':
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif indices[ind] == 'WSDI':
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif (indices[ind] == 'CWD'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        else:
            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
    elif A_end > 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx') or (indices[ind] == 'TNn'):
            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif (indices[ind] == 'R95p') or (indices[ind] == 'R99p'):
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif indices[ind] == 'WSDI':
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif (indices[ind] == 'CWD'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        else:
            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
#[-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9]

    # define colorbar depending on index
    if  (indices[ind] == 'Rx1day') or (indices[ind] == 'Rx5day') or (indices[ind] == 'R95p') or (indices[ind] == 'R99p') or (indices[ind] == 'R10mm') or (indices[ind] == 'CWD'):      
        colbar='BrBG'
    elif (indices[ind] == 'WSDI'):
        colbar='RdBu_r'
    elif (indices[ind] == 'CDD'):
        colbar='BrBG_r'
    else:
        colbar='RdBu_r'

    # plot CTL
    plotname = outdir + experiments[1]+'/climdex_figures/'+indices[ind]+'_'+experiments[1]+'_DEF_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units[ind]+'] '+experiments[1]+' deforested '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_varA_def,masked_lats, lons, title = title)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference rcp-L2A
    plotname = outdir + experiments[1]+'/climdex_figures/'+indices[ind]+'_diff_DEF_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' deforested '+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (indices[ind] == 'TXx') and (A_start==19710100):
        utils.draw_reg(masked_diffA,masked_lats, lons, title = title, subpanel = "a)", colors=colbar,levels=lev, sig=masked_HA_def, name=['CNA','SAF','MED','NAU'],lonmin=[-110,-10,-10.0,110],lonmax=[-85,51.99,40,155],latmin=[25,-35,30,-30],latmax=[50,-11.365,45,-10])
    elif (indices[ind] == 'TX90p') and (A_start==19710100):
	utils.draw(masked_diffA_def,masked_lats, lons, title = title, subpanel="c)", colors=colbar,levels=lev, sig=masked_HA_def)
    else :
        utils.draw(masked_diffA_def,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA_def, masko=True)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference rcp-L2A
    plotname = outdir + experiments[1]+'/climdex_figures/'+indices[ind]+'_diff_AFF_'+experiments[0]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' afforested '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffA_aff,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA_aff, masko=True)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
