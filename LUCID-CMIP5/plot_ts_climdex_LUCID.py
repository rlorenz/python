#!/usr/bin/python

'''
File Name : plot_ts_climdex_LUCID.py
Creation Date: 04 Jun 2015
Last Modified: 
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from LUCID-CMIP5 project
Downloaded to storm servers: /srv/ccrc/data44/z3441306/LUCID-CMIP5/
by Ruth Lorenz

5 GCMs are : CanESM2, HadGEM2-ES, IPSL-CM5A-LR, MIROC-ESM, MPI-ESM-LR
all have different resolution and biases

Experiments: L2A26, L2A85
(see Brovkin et al. 2013, J.Clim for more info)
This script compares rcp's (which is either rcp26 or rcp85)
and LUCID experiments (L2A26 or L2A85)

Read climdex indices calculated with /home/z3441306/fclimdex/etccdi/fclimdex_RCM/fclimdex.exe
or adjusted versions for different calendars
in /srv/ccrc/data44/z3441306/LUCID-CMIP5/$model/$experiment/climdex_index/*_remapcon2_1x1.nc
these files are already remapped to common grid for all models using "cdo remapcon2"
We remap onto 1x1 but plot contours instead of pixels

Regions are: Amazonia (AMZ), East Australia (EAU), Europe (EUR), North America(NAM),
South Africa (SAF), Sahel(SAH), South America(SAM)

call within outdir because of pdfcrop!
'''

# Load modules for this script
import sys
import pdb 
import netCDF4 as nc
#from scipy.io import netcdf_file
from scipy import stats
from scipy import signal
import numpy as np
import pandas as pd
from pandas import Series
import matplotlib
import matplotlib.pyplot as plt
#import utils
import os
from subprocess import call

sys.path.insert(0, '/home/z3441306/my_python')
from point_inside_polygon import point_inside_polygon
###
# Define input
###
indir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
outdir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5_plots/ENS_MEAN/'
#models = ['CanESM2', 'HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
models = ['CanESM2', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
#models = ['HadGEM2-ES']
#experiments = ['rcp26','L2A26']
experiments = ['rcp85','L2A85']
indices = ['TX90p','TN90p','TNn','TXx','WSDI','Rx1day','Rx5day','R95p','R99p','R10mm','CDD','CWD']
units=['%','%','$^\circ$C','$^\circ$C','days','mm','mm','mm','mm','days','days','days']

# start and end of 1st run in analysis
R0_start = 2005
R0_end = 2100
# start and end of 2nd run in analysis
R1_start = 2005
R1_end = 2100
# define analysis start and end year
A_start = 20060100
A_end = 20990100

#define regions
regions = ['AMZ','EAU','EUR','NAM','SAF','SAH','SAM']
lat_max = np.array([21.25,-11,60,50])
lat_min =  np.array([-26.25,-40,30,25])
lon_max =  np.array([333.750,160,40,300])
lon_min = np.array([275,135,-10,230])

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
	print models[mod]
	###
        # Load netcdf files
        if (models[mod]=="HadGEM2-ES") and (experiments[1]=="L2A26"):
		R1_end = 2099
	else:
		R1_end = 2100
	path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_r1i1p1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_r1i1p1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'.nc'
        # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
        ifile = nc.Dataset(path0)
        #var = ifile.variables['Annual']
        #miss_val=var.missing_value
        #rcp = np.ma.masked_values(var[:],miss_val)
	lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time0 = ifile.variables['time'][:]
        rcp = ifile.variables['Annual'][(time0>=A_start) & (time0<=A_end),:,:]
        miss_val=ifile.variables['Annual'].missing_value
        ifile.close()
        # print information about variable
        #print type(rcp)
        #print rcp[1,0,0]
	#print rcp.shape
	#print ifile.variables['Annual'].dimensions
	#print time0
        print miss_val
        # read Annual value of ETCCDI Index for 2nd run (expA)
        ifile = nc.Dataset(path1)
        #var1 = ifile.variables['Annual']
        #miss_val1=var1.missing_value
        #L2A = np.ma.masked_values(var1[:],miss_val1)
        lats1 = ifile.variables['lat'][:]
        lons1 = ifile.variables['lon'][:]
	time1 = ifile.variables['time'][:]
        L2A = ifile.variables['Annual'][(time1>=A_start) & (time1<=A_end),:,:]
        miss_val1=ifile.variables['Annual'].missing_value
        ifile.close()
        print miss_val1
	#make sure both experiments are on the same grid
	if lats.shape != lats1.shape:
		print 'latitudes are different'
		sys.exit
	else:
		del lats1
	if lons.shape != lons1.shape:
		print 'longitudes are different'
		sys.exit
	else:
		del lons1
        ###
    	# cut data into analysis period
    	#ind01 = np.where(time0==A_start)[0][0]
    	#ind02 = np.where(time0==A_end)[0][0]+1
    	#rcp_tp = rcp[ind01:ind02,:,:]
        #ind11 = np.where(time1==A_start)[0][0]
        #ind12 = np.where(time1==A_end)[0][0]+1
    	#L2A_tp = L2A[ind11:ind12,:,:]

	ntim0 = rcp.shape[0]
        ntim1 = L2A.shape[0]
	if ntim0 != ntim1:
		print 'time periods are different! ntim0=' +ntim0+ ' ntim1='+ntim1
		sys.exit

        #detrend timeseries
	print 'detrend timeseries'
        if isinstance(rcp,np.ma.core.MaskedArray):
            print ("numpy masked array")
            rcp_tp_dtr = np.ma.empty((ntim0,len(lats),len(lons)))
            for x in range(len(lats)):
        	for y in range(len(lons)):  
                    rcp_tmp = rcp[:,x,y]
                    rcp_tp_dtr[:,x,y] = signal.detrend(rcp_tmp[~rcp_tmp.mask].data, axis=0, type='linear',bp=0)
        elif isinstance(rcp, np.ndarray):
            print ("numpy array")
            rcp_tp_dtr = signal.detrend(rcp, axis=0, type='linear',bp=0)
        else:
            print("wrong data type")

        if isinstance(L2A, np.ma.core.MaskedArray):
            L2A_tp_dtr = np.ma.empty((ntim1,len(lats),len(lons)))
            for x in range(len(lats)):
        	for y in range(len(lons)):  
                    L2A_tmp = L2A[:,x,y]
                    L2A_tp_dtr[:,x,y] = signal.detrend(L2A_tmp[~L2A_tmp.mask].data, axis=0, type='linear',bp=0)
        else:
            L2A_tp_dtr = signal.detrend(L2A, axis=0, type='linear',bp=0)

        #calculate standard deviation
        #keepdims:If this is set to True, the axes which are reduced are left in the result as dimensions with size one
        rcp_tp_dtr_std = np.ma.std(rcp_tp_dtr,axis=0,keepdims=False)
        L2A_tp_dtr_std = np.ma.std(L2A_tp_dtr,axis=0,keepdims=False)

        #calculate 30y moving window average
	print 'calculate 30y moving window average'
	rcp_tp_mvavg = np.ndarray((ntim0,len(lats),len(lons)))
	L2A_tp_mvavg = np.ndarray((ntim0,len(lats),len(lons)))
    	for x in range(len(lats)):
        	for y in range(len(lons)):
			rcp_tp_tmp = np.copy(rcp[:,x,y])
			rcp_tp_tmp[rcp_tp_tmp==miss_val]=np.nan
			L2A_tp_tmp = np.copy(L2A[:,x,y])
			L2A_tp_tmp[L2A_tp_tmp==miss_val1]=np.nan

        		rcp_tp_mvavg[:,x,y] = pd.stats.moments.rolling_mean(rcp_tp_tmp, window = 30, min_periods=20, center=True)
			L2A_tp_mvavg[:,x,y] = pd.stats.moments.rolling_mean(L2A_tp_tmp, window = 30, min_periods=20, center=True)

        print rcp_tp_mvavg.shape, L2A_tp_mvavg.shape
        newtim = rcp_tp_mvavg.shape[0]
        plt.imshow(L2A_tp_mvavg[10,:,:])
        plt.show()

        #mask ocean
        #filemask=
        #lm=filemask.variables['sftlf'][0,0,:,:]
        #ocean_mask = np.where(lm>=1)[0]
        #rcp_tp_mvavg_mask = rcp_tp_mvavg[:,ocean_mask]
        #L2A_tp_mvavg_mask = L2A_tp_mvavg[:,ocean_mask]
        rcp_tp_mvavg_mask = np.copy(rcp_tp_mvavg)
        L2A_tp_mvavg_mask = np.copy(L2A_tp_mvavg)

        #calculate regional average
        #Loop over regions
        if (mod == 0):
            rcp_tp_mvavg_reg_avg = np.ndarray((len(regions),len(models),newtim))
            L2A_tp_mvavg_reg_avg = np.ndarray((len(regions),len(models),newtim))
#        for reg in range(len(regions)):
	for reg in range(0, 1):
		print regions[reg]
		print lat_min[reg], lat_max[reg], lon_min[reg], lon_max[reg]
		for ilat in range(len(lats)):
			for ilon in range(len(lons)):
				print lats[ilat], lons[ilon]
				if (point_inside_polygon(lons[ilon],lats[ilat],[(lon_min[reg],lat_min[reg]),(lon_max[reg],lat_min[reg]),(lon_max[reg].lat_max[reg]),(lon_min[reg],lat_max[reg]))]):
                                        print("Inside polygon")
					rcp_tp_mvavg_mask[:, ilat, ilon] = rcp_tp_mvavg[:, ilat, ilon]
					L2A_tp_mvavg_mask[:, ilat, ilon] = L2A_tp_mvavg[:, ilat, ilon]
				else:
                                        print("Outside polygon")
					rcp_tp_mvavg_mask[:, ilat, ilon] = np.NaN
					L2A_tp_mvavg_mask[:, ilat, ilon] = np.NaN
            #mask_ind_lats = np.where((lats < lat_max[reg]) & (lats > lat_min[reg]))[0]
            #mask_ind_lons = np.where((lons > lon_min[reg]) & (lons <  lon_max[reg]))[0]
            #rcp_tp_mvavg_mask_reg = rcp_tp_mvavg_mask[:, mask_ind_lats,:]
            #rcp_tp_mvavg_mask_reg = rcp_tp_mvavg_mask_reg[:, :, mask_ind_lons]
            #L2A_tp_mvavg_mask_reg = L2A_tp_mvavg_mask[:, mask_ind_lats,:]
            #L2A_tp_mvavg_mask_reg = L2A_tp_mvavg_mask_reg[:, :, mask_ind_lons]
                plt.imshow(rcp_tp_mvavg_mask[10,:,:])
                plt.show()
		rcp_tp_mvavg_mask_reg_resh = np.reshape(rcp_tp_mvavg_mask,(newtim,len(lats) * len(lons)))
		L2A_tp_mvavg_mask_reg_resh = np.reshape(L2A_tp_mvavg_mask,(newtim,len(lats) * len(lons)))

		rcp_tp_mvavg_reg_avg[reg,mod,:] = np.ma.mean(rcp_tp_mvavg_mask_reg_resh,axis=1)
		L2A_tp_mvavg_reg_avg[reg,mod,:] = np.ma.mean(L2A_tp_mvavg_mask_reg_resh,axis=1)

    #test if difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
		ks_A = np.ndarray((2))
		if (mod == 0) and (reg==0):
			H_A = np.ndarray((len(regions),len(models),newtim))
			#test over 10y segments if statistically different
		for t in range(0,newtim-11):
			ks_A[:] = stats.ks_2samp(rcp_tp_mvavg_reg_avg[reg,mod,t:t+10], L2A_tp_mvavg_reg_avg[reg,mod,t:t+10])
		if ks_A[1] < sig:
			H_A[reg,mod,t+10] = 1
		else:
			H_A[reg,mod,t+10] = 0

	#draw a time series for each index
	print 'plot data'
	xax = np.linspace(2006, 2100, ntim0)
	plt.plot(xax, rcp_tp_mvavg_reg_avg[reg,0,:],c='r',lw=2)
	plt.plot(xax, rcp_tp_mvavg_reg_avg[reg,1,:],c='b',lw=2)
	plt.plot(xax, rcp_tp_mvavg_reg_avg[reg,2,:],c='c',lw=2)
	plt.plot(xax, rcp_tp_mvavg_reg_avg[reg,3,:],c='m',lw=2)
	#plt.plot(xax, rcp_tp_mvavg_reg_avg[reg,4,:],c='k',lw=2)
	plt.plot(xax, L2A_tp_mvavg_reg_avg[reg,0,:],ls='--',c='r',lw=2)
	plt.plot(xax, L2A_tp_mvavg_reg_avg[reg,1,:],ls='--',c='b',lw=2)
	plt.plot(xax, L2A_tp_mvavg_reg_avg[reg,2,:],ls='--',c='c',lw=2)
	plt.plot(xax, L2A_tp_mvavg_reg_avg[reg,3,:],ls='--',c='m',lw=2)
	#plt.plot(xax, L2A_tp_mvavg_reg_avg[reg,4,:],ls='--',c='k',lw=2)
	plt.title('%s'%indices[ind] + ' 30y rolling means %s'%regions[reg])
	#axes
	#plt.axis([2005, 2100, -0.5, 1.5])
	#plt.legend([models[0], models[1],models[2],models[3],models[4], models[0], models[1],models[2],models[3],models[4]], loc=2, fontsize = 10)
        plt.legend([models[0], models[1],models[2],models[3], models[0], models[1],models[2],models[3]], loc=2, fontsize = 10)
	plt.savefig(outdir+ '%s'%  indices[ind]+ '_' + '%s'%regions[reg] +'_'+ '%s'%experiments[0]+'_' + '%s'%experiments[1]+'_movavgTS' + '.pdf')
	plt.close()






