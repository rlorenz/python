#!/usr/bin/python

'''
File Name : plot_climdex_CTL-EXP_LUCID_ENSMEAN.py
Creation Date: 24 Oct 2014
Last Modified: 11/05/2015
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from LUCID-CMIP5 project
Downloaded to storm servers: /srv/ccrc/data44/z3441306/LUCID-CMIP5/
by Ruth Lorenz

5 GCMs are : CanESM2, HadGEM2-ES, IPSL-CM5A-LR, MIROC-ESM, MPI-ESM-LR
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: L2A26, L2A85
(see Brovkin et al. 2013, J.Clim for more info)
This script compares rcp's (which is either rcp26 or rcp85)
and LUCID experiments (L2A26 or L2A85) in one time period

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data44/z3441306/LUCID-CMIP5/$model/$experiment/climdex_index/*_remapcon2_1x1.nc
these files are already remapped to common grid for all models using "cdo remapcon2"
We remap onto 1x1 but plot contours instead of pixels

call within outdir because of pdfcrop!
'''

# Load modules for this script
import sys
import pdb 
from scipy import stats
import numpy as np
import netCDF4 as nc
import utils
import os
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data44/z3441306/LUCID-CMIP5/'
outdir0 = '/srv/ccrc/data44/z3441306/LUCID-CMIP5_plots/'
#models = ['CanESM2', 'HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
models = [ 'IPSL-CM5A-LR', 'MIROC-ESM', 'MPI-ESM-LR']
#models = ['CanESM2']
#experiments = ['rcp26','L2A26']
experiments = ['rcp85','L2A85']
indices = ['TX90p','TN90p','TNn','TXx','Rx1day','Rx5day']
units=['%','%','$^\circ$C','$^\circ$C','mm','mm']
normalize="FALSE"

seasons=['DJF','MAM','JJA','SON']

# start and end of 1st run in analysis
R0_start = 2005
R0_end = 2100
# start and end of 2nd run in analysis
R1_start = 2005
R1_end = 2100
# define analysis start and end year
A_start = 20700100
A_end = 20990100

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    for seas in range(len(seasons)):
        print seasons[seas]
        if (seasons[seas] == "DJF"):
            variable1 = "December"
            variable2 = "January"
            variable3 = "February"
        elif (seasons[seas] == "MAM"):
            variable1 = "March"
            variable2 = "April"
            variable3 = "May"
        elif (seasons[seas] == "JJA"):
            variable1 = "June"
            variable2 = "July"
            variable3 = "August"
        else:
            variable1 = "September"
            variable2 = "October"
            variable3 = "November"
        # Loop over models to read all data
        for mod in range(len(models)):
            outdir = outdir0 + models[mod] 
            ###
            # Load netcdf files
            if (models[mod]=="HadGEM2-ES") and (experiments[1]=="L2A26"):
                    R1_end = 2099
            else:
                    R1_end = 2100
            path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_r1i1p1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'.nc'
            path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_r1i1p1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'.nc'
            # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
            ifile = nc.Dataset(path0)
            lats = ifile.variables['lat'][:]
            lons = ifile.variables['lon'][:]
            time0 = ifile.variables['time'][:]
    
            var01 = ifile.variables[variable1][(time0>=A_start) & (time0<=A_end),:,:]
            var02 = ifile.variables[variable2][(time0>=A_start) & (time0<=A_end),:,:]
            var03 = ifile.variables[variable3][(time0>=A_start) & (time0<=A_end),:,:]
            var0 = np.ma.empty([3,var01.shape[0],var01.shape[1],var01.shape[2]], dtype=type(var01))
            var0[0,:,:,:]=var01
            var0[1,:,:,:]=var02
            var0[2,:,:,:]=var03
            # print information about variable
            #print type(var0)
            #print var0.shape
            #print ifile.variables['Annual'].dimensions
            #print time0
            # read Annual value of ETCCDI Index for 2nd run (expA)
            ifile = nc.Dataset(path1)
            lats1 = ifile.variables['lat'][:]
            lons1 = ifile.variables['lon'][:]
            time1 = ifile.variables['time'][:]
            varA1 = ifile.variables[variable1][(time1>=A_start) & (time1<=A_end),:,:]
            varA2 = ifile.variables[variable2][(time1>=A_start) & (time1<=A_end),:,:]
            varA3 = ifile.variables[variable3][(time1>=A_start) & (time1<=A_end),:,:]
            varA = np.ma.empty([3,varA1.shape[0],varA1.shape[1],varA1.shape[2]], dtype=type(varA1))
            varA[0,:,:,:]=varA1
            varA[1,:,:,:]=varA2
            varA[2,:,:,:]=varA3 
            #print type(varA)
            #print varA.shape

            #make sure both experiments are on the same grid
            if lats.shape != lats1.shape:
                    print 'latitudes are different'
                    sys.exit
            else:
                    del lats1
            if lons.shape != lons1.shape:
                    print 'longitudes are different'
                    sys.exit
            else:
                    del lons1

            ntim0 = var0.shape[1]
            ntim1 = varA.shape[1]
            if ntim0 != ntim1:
                    print 'time periods are different! ntim0=' +ntim0+ ' ntim1='+ntim1
                    sys.exit

            # read land use change mask
            path_luc = indir + models[mod] +'/'+ experiments[0]+'/treeFrac_change_'+models[mod]+'_'+experiments[0]+'_2099-2006.nc'
            #print path_luc
            ifile = nc.Dataset(path_luc)
            def_mask=ifile.variables['LUC_DEF'][:]
            #print type(def_mask), def_mask.shape
            aff_mask=ifile.variables['LUC_AFF'][:]
            #print type(aff_mask), aff_mask.shape

            #calculate seasonal values depending on index
            if (indices[ind]=="TXx") or (indices[ind]=="TNx"):
                var0_seas = np.ma.max(var0,axis=0)
                varA_seas = np.ma.max(varA,axis=0)
            elif (indices[ind]=="TXn") or (indices[ind]=="TNn"):
                var0_seas = np.ma.min(var0,axis=0)
                varA_seas = np.ma.min(varA,axis=0)
            elif (indices[ind]=="TX90p") or (indices[ind]=="TN90p") or (indices[ind]=="TX10p") or (indices[ind]=="TN10p"):#this is not the correct way to do this for percentile based indices!!!
                var0_seas = np.ma.mean(var0,axis=0)
                varA_seas = np.ma.mean(varA,axis=0)
            elif (indices[ind]=="Rx1day") or (indices[ind]=="Rx5day"):
                var0_seas = np.ma.max(var0,axis=0)
                varA_seas = np.ma.max(varA,axis=0)
            else:
                var0_seas = np.ma.mean(var0,axis=0)
                varA_seas = np.ma.mean(varA,axis=0)
    
            #calculate difference between ctl and experiments
            diffA_seas = var0_seas - varA_seas
    
            #average data over time to plot map
            var0_avg = np.ma.mean(var0_seas, axis=0)
            varA_avg = np.ma.mean(varA_seas, axis=0)
            diffA_avg = np.ma.mean(diffA_seas, axis=0)
            #print varA_avg.shape, diffA_avg.shape
            #test if ensemble mean difference is statistically significant
            #ks_2samp gives 2 values: KS statistic and two tailed p-value
            #if p-value is below signigicant level we can reject the hypothesis
            #that the distributions of the two samples are the same
            ks_A = np.ndarray((2,len(lats),len(lons)))
            H_A = np.ndarray((len(lats),len(lons)))
            for x in range(len(lats)):
                    for y in range(len(lons)):
                            ks_A[:,x,y] = stats.ks_2samp(var0_seas[:,x,y], varA_seas[:,x,y])
                            if ks_A[1,x,y] < sig:
                                H_A[x,y] = 1
                            else:
                                H_A[x,y] = 0

            #mask by LUC mask for deforested areas
            diffA_avg_def=np.ma.masked_where(def_mask>=0,diffA_avg)
            H_A_def=np.ma.masked_where(def_mask>=0,H_A)
            varA_avg_def=np.ma.masked_where(def_mask>=0,varA_avg)
            #print type(varA_avg_def), varA_avg_def.shape
            #print type(diffA_avg_def), diffA_avg_def.shape
            #mask by LUC mask for afforested areas
            diffA_avg_aff=np.ma.masked_where(aff_mask<=0,diffA_avg)
            H_A_aff=np.ma.masked_where(aff_mask<=0,H_A)
            varA_avg_aff=np.ma.masked_where(aff_mask<=0,varA_avg)

            #mask very high northern and southern latitudes for plotting
            mask_ind = np.where((lats < 80) & (lats > -60))[0]
            #print type(mask_ind),mask_ind.shape
            masked_lats = lats[mask_ind]

            masked_varA_def = varA_avg_def[mask_ind,:]
            masked_diffA_def = diffA_avg_def[mask_ind,:]
            masked_HA_def = H_A_def[mask_ind,:]

            masked_varA_aff = varA_avg_aff[mask_ind,:]
            masked_diffA_aff = diffA_avg_aff[mask_ind,:]
            masked_HA_aff = H_A_aff[mask_ind,:]

            # plot data using utils.py (based on Jeffs from python workshop)
            # save as eps because problem with hatching when using pdf (defined in header)
            # define levels for contour plots
            if A_end < 20500100:
                    if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
                            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
                    elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
                            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
                    elif indices[ind] == 'R95p':
                            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
                    elif indices[ind] == 'WSDI':
                            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
                    elif (indices[ind] == 'CWD'):
                            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
                    else:
                            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
            elif A_end > 20500100:
                    if (indices[ind] == 'TXx') or (indices[ind] == 'TNx')or (indices[ind] == 'TNn'):
                            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
                    elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
                            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
                    elif (indices[ind] == 'R95p') or (indices[ind] == 'R99p'):
                            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
                    elif indices[ind] == 'WSDI':
                            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
                    elif (indices[ind] == 'CWD'):
                            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
                    else:
                            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
    #[-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9]

            # define colorbar depending on index
            if  (indices[ind] == 'Rx1day') or (indices[ind] == 'Rx5day') or (indices[ind] == 'R95p') or (indices[ind] == 'R99p') or (indices[ind] == 'R10mm') or (indices[ind] == 'CWD'):      
                    colbar='BrBG'
            elif (indices[ind] == 'WSDI'):
                    colbar='RdBu_r'
            elif (indices[ind] == 'CDD'):
                    colbar='BrBG_r'
            else:
                    colbar='RdBu_r'

            # plot CTL
            if (os.access(outdir+ '/'+ experiments[1]+'/climdex_figures/',os.F_OK)==False):
                os.makedirs(outdir+ '/'+ experiments[1]+'/climdex_figures/')
            plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_'+experiments[1]+'_DEF_'+seasons[seas]+'_'+str(A_start)[:4]+'-'+str(A_end)[:4]
            #print plotname + plottype
            title = indices[ind]+' ['+units[ind]+'] '+experiments[1]+' deforested '+str(A_start)[:4]+'-'+str(A_end)[:4] +' '+seasons[seas]
            utils.draw(masked_varA_def,masked_lats, lons, title = title)
            #utils.plt.show()
            utils.plt.savefig(plotname+plottype)
            call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
            call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

            # plot difference rcp-L2A
            plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_diff_DEF_'+experiments[0]+'-'+experiments[1]+'_KSsig_'+seasons[seas]+'_'+str(A_start)[:4]+'-'+str(A_end)[:4]
            #print plotname + plottype
            title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' deforested '+str(A_start)[:4]+'-'+str(A_end)[:4] +' '+seasons[seas]
            utils.draw(masked_diffA_def,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA_def, masko=True)
            #utils.plt.show()
            utils.plt.savefig(plotname+plottype)
            call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
            call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

            # plot difference rcp-L2A
            plotname = outdir +'/'+ experiments[1]+'/climdex_figures/'+indices[ind]+'_diff_AFF_'+experiments[0]+'-'+experiments[1]+'_KSsig_'+seasons[seas]+'_'+str(A_start)[:4]+'-'+str(A_end)[:4]
            #print plotname + plottype
            title = indices[ind]+' ['+units[ind]+'] '+experiments[0]+'-'+experiments[1]+' afforested '+str(A_start)[:4]+'-'+str(A_end)[:4] +' '+seasons[seas]
            utils.draw(masked_diffA_aff,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA_aff, masko=True)
            #utils.plt.show()
            utils.plt.savefig(plotname+plottype)
            call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
            call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
