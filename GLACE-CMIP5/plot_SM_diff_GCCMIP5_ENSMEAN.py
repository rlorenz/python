#!/usr/bin/python
'''
File Name : plot_SM_diff_GC1B85-GC1A85.py
Creation Date : 14-01-2015
Last Modified : Mon 20 Jul 2015 14:03:04 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : Plot difference in soil moisture between ExpB and ExpA
	for individual models to help interpreting results


'''
# Load modules for this script
import numpy as np
from scipy.io import netcdf_file
from scipy import stats
import datetime as dt
import netCDF4 as nc
import utils
from subprocess import call
from netcdftime import utime

###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
variables = ['mrso']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1955
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1951
R1_end = 2100

# define analysis start and end year
A_start = 1971
A_end = 2000

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over models to read all data
for mod in range(len(models)):
# Load netcdf files
    path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'01-'+str(R0_end)+'12_maskocean_remapcon.nc'
    path1 = indir + models[mod] +'/'+ experiments[1]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[1]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean_remapcon.nc'
    path2 = indir + models[mod] +'/'+ experiments[2]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[2]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean_remapcon.nc'
# read variable for 1st run (ctl), read lats and lons as well
    ifile = netcdf_file(path0)
    var = ifile.variables[variables[0]]
    miss_val=var._FillValue
    if (models[mod] == "EC-EARTH"):
        var0=np.ma.masked_values(var[:,0,:,:],miss_val)
    else:
        var0=np.ma.masked_values(var[:],miss_val)
    lats = ifile.variables['lat'].data
    lons = ifile.variables['lon'].data
    time = ifile.variables['time']
    if (models[mod] == "ECHAM6"):
        dates=time.data
        years=np.floor(dates/10000)
    else:
        cdftime = utime(time.units)
        dates=cdftime.num2date(time.data)
        years=np.asarray([dates[i].year for i in xrange(len(dates))])
    ifile.close()
    # read variable for 2nd run (expA)
    ifile = netcdf_file(path1)
    varA = ifile.variables[variables[0]]
    miss_valA=varA._FillValue
    ifile.close()
    # read variable for 3rd run (expB)
    ifile = netcdf_file(path2)
    varB = ifile.variables[variables[0]]
    miss_valB=varB._FillValue
    ifile.close()
    #first cut expA and expB into same length as CTL
    t_diff=(R0_start-R1_start)*12
    if (models[mod] == "EC-EARTH"):
        varA_cut = np.ma.masked_values(varA[t_diff:,0,:,:],miss_valA)
        varB_cut = np.ma.masked_values(varB[t_diff:,0,:,:],miss_valB)
    else:
        varA_cut = np.ma.masked_values(varA[t_diff:,:,:],miss_valA)
        varB_cut = np.ma.masked_values(varB[t_diff:,:,:],miss_valB)
    ntim = varB_cut.shape[0]
    # cut data into analysis period
    ind1 = np.where(years==A_start)[0][0]
    ind2 = np.where(years==A_end)[0][0]+12
    if mod==0:
        var0_tp=np.ma.empty([len(models),ind2-ind1,len(lats),len(lons)],dtype=float)
        varA_tp=np.ma.empty([len(models),ind2-ind1,len(lats),len(lons)],dtype=float)
        varB_tp=np.ma.empty([len(models),ind2-ind1,len(lats),len(lons)],dtype=float)
    var0_tp_tmp = np.ma.copy(var0)
    var0_tp[mod,:,:,:] = var0_tp_tmp[ind1:ind2,:,:]
    varA_tp_tmp = np.ma.copy(varA_cut)
    varA_tp[mod,:,:,:] = varA_tp_tmp[ind1:ind2,:,:]
    varB_tp_tmp = np.ma.copy(varB_cut)
    varB_tp[mod,:,:,:] = varB_tp_tmp[ind1:ind2,:,:]
    #average data over time
    var0_tp_mean = np.ma.mean(var0_tp[mod,:,:,:], axis=0)
    varA_tp_mean = np.ma.mean(varA_tp[mod,:,:,:], axis=0)
    varB_tp_mean = np.ma.mean(varB_tp[mod,:,:,:], axis=0)
    if mod==0:
        diff_A_CTL=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
        diff_B_A=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
        diff_A_CTL_abs=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
        diff_B_A_abs=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)
    # difference relative in percent
    diff_A_CTL[mod,:,:] = ((varA_tp_mean-var0_tp_mean)/var0_tp_mean)*100
    diff_B_A[mod,:,:] = ((varB_tp_mean-varA_tp_mean)/varA_tp_mean)*100
    # absolute difference in mm water column
    diff_A_CTL_abs[mod,:,:] = (varA_tp_mean-var0_tp_mean)
    diff_B_A_abs[mod,:,:] = (varB_tp_mean-varA_tp_mean)
    print type(diff_B_A)
#calculate ensemble mean
diff_A_CTL_ens=np.ma.mean(diff_A_CTL, axis=0)
diff_B_A_ens=np.ma.mean(diff_B_A, axis=0)
diff_A_CTL_abs_ens=np.ma.mean(diff_A_CTL, axis=0)
diff_B_A_abs_ens=np.ma.mean(diff_B_A, axis=0)
var0_ens_tp=np.ma.mean(var0_tp, axis=0)
varA_ens_tp=np.ma.mean(varA_tp, axis=0)
varB_ens_tp=np.ma.mean(varB_tp, axis=0)

#test if difference is statistically significant
#ks_2samp gives 2 values: KS statistic and two tailed p-value
#if p-value is below signigicant level we can reject the hypothesis
#that the distributions of the two samples are the same
#ks_A = np.ndarray((2,len(lats),len(lons)))
#ks_B = np.ndarray((2,len(lats),len(lons)))
#ks_BA = np.ndarray((2,len(lats),len(lons)))
#H_A = np.ndarray((len(lats),len(lons)))
#H_B = np.ndarray((len(lats),len(lons)))
#H_BA = np.ndarray((len(lats),len(lons)))
#for x in range(len(lats)):
#    for y in range(len(lons)):
#        ks_A[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varA_ens_tp[:,x,y])
#        if ks_A[1,x,y] < sig:
#            H_A[x,y] = 1
#        else:
#            H_A[x,y] = 0
#        ks_BA[:,x,y] = stats.ks_2samp(varA_ens_tp[:,x,y], varB_ens_tp[:,x,y])
#        if ks_BA[1,x,y] < sig:
#            H_BA[x,y] = 1
#        else:
#            H_BA[x,y] = 0

# check where at least 5 out of 6 models agree on sign of change
H_agree_A=np.ndarray((len(lats),len(lons)))
H_agree_B_A=np.ndarray((len(lats),len(lons)))
for x in range(len(lats)):
    for y in range(len(lons)):
        neg_A = sum(np.ma.count(np.ma.where(diff_A_CTL[:,x,y]<0),0))
        pos_A = sum(np.ma.count(np.ma.where(diff_A_CTL[:,x,y]>0),0))
        agree_A=max(pos_A,neg_A)
        if agree_A >= 5:
            H_agree_A[x,y]=1
        else:
            H_agree_A[x,y]=0
        neg_B_A = sum(np.ma.count(np.ma.where(diff_B_A[:,x,y]<0),0))
        pos_B_A = sum(np.ma.count(np.ma.where(diff_B_A[:,x,y]>0),0))
        agree_B_A=max(pos_B_A,neg_B_A)
        if agree_B_A >= 5:
            H_agree_B_A[x,y]=1
        else:
            H_agree_B_A[x,y]=0

#mask very high northern and southern latitudes for plotting
mask_ind = np.where((lats < 80) & (lats > -60))[0]
masked_lats = lats[mask_ind]

masked_diff_A_CTL = diff_A_CTL_ens[mask_ind,:]
masked_diff_A_CTL_abs = diff_A_CTL_abs_ens[mask_ind,:]
#masked_HA = H_A[mask_ind,:]
masked_agree_HA = H_agree_A[mask_ind,:]
masked_diff_B_A = diff_B_A_ens[mask_ind,:]
masked_diff_B_A_abs = diff_B_A_abs_ens[mask_ind,:]
#masked_HBA = H_BA[mask_ind,:]
masked_agree_HBA = H_agree_B_A[mask_ind,:]

# plot data using utils.py (based on Jeffs from python workshop)
lev_a = [-110,-90,-70,-50,-30,-10,10,30,50,70,90,110]
lev = [-61,-29,-13,-5,-1,1,5,13,29,61]
colbar='BrBG'
# plot difference GC1A85-CTL
plotname = outdir + experiments[1]+'/'+variables[0]+'_diff_'+experiments[1]+'-CTL_'+str(A_start)+'-'+str(A_end)
print plotname + plottype
title = 'Ensemble soil moisture [%] SMclim-CTL '+str(A_start)+'-'+str(A_end)
utils.draw(masked_diff_A_CTL,masked_lats, lons, title = title, colors=colbar,levels=lev,sig=masked_agree_HA)
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

# plot difference GC1B85-GC1A85
plotname = outdir + experiments[2]+'/'+variables[0]+'_diff_'+experiments[2]+'-'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)
print plotname + plottype
title = 'Ensemble soil moisture [%] SMtrend-SMclim '+str(A_start)+'-'+str(A_end)
utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="g)", colors=colbar ,levels=lev,sig=masked_agree_HBA)
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
     
# plot difference GC1A85-CTL
plotname = outdir + experiments[1]+'/'+variables[0]+'_diff_abs_'+experiments[1]+'-CTL_'+str(A_start)+'-'+str(A_end)
print plotname + plottype
title = 'Ensemble soil moisture [mm] SMclim-CTL '+str(A_start)+'-'+str(A_end)
utils.draw(masked_diff_A_CTL_abs,masked_lats, lons, title = title, colors=colbar,levels=lev_a,sig=masked_agree_HA )
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

# plot difference GC1B85-GC1A85
plotname = outdir + experiments[2]+'/'+variables[0]+'_diff_abs_'+experiments[2]+'-'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)
print plotname + plottype
title = 'Ensemble soil moisture [mm] SMtrend-SMclim '+str(A_start)+'-'+str(A_end)
utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="g)", colors=colbar ,levels=lev_a,sig=masked_agree_HBA)
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
 
#also save ensemble differences in netcdf for later panelling
fout=nc.Dataset('%s/SM_diffs_ENSMEAN_ANN_%s-%s.nc'%(outdir,str(A_start)[:4],str(A_end)[:4]),mode='w')
fout.createDimension('lat',masked_lats.shape[0])
fout.createDimension('lon',lons.shape[0])

latout=fout.createVariable('lat','f8',('lat'),fill_value=miss_val)
setattr(latout,'Longname','Latitude')
setattr(latout,'units','degrees_north')

lonout=fout.createVariable('lon','f8',('lon'),fill_value=miss_val)
setattr(lonout,'Longname','Longitude')
setattr(lonout,'units','degrees_east')

diffAout=fout.createVariable('ExpA-CTL','f8',('lat','lon'),fill_value=miss_val)
setattr(diffAout,'Longname','Difference between ExpA and CTL')
setattr(diffAout,'units','%')
setattr(diffAout,'description','ExpA-CTL SM in percent from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

diffAabsout=fout.createVariable('ExpA-CTL_abs','f8',('lat','lon'),fill_value=miss_val)
setattr(diffAabsout,'Longname','Difference between ExpA and CTL')
setattr(diffAabsout,'units','mm')
setattr(diffAabsout,'description','ExpA-CTL SM in absolute values from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

sigAout=fout.createVariable('sigExpA-CTL','f8',('lat','lon'),fill_value=miss_val)
setattr(sigAout,'Longname','Significance in difference between ExpA and CTL')
setattr(sigAout,'units','-')
setattr(sigAout,'description','Statistical significance ExpA-CTL SM from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

diffBAout=fout.createVariable('ExpB-ExpA','f8',('lat','lon'),fill_value=miss_val)
setattr(diffBAout,'Longname','Difference between ExpB and ExpA')
setattr(diffBAout,'units','%')
setattr(diffBAout,'description','ExpB-ExpA SM in percent from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

diffBAabsout=fout.createVariable('ExpB-ExpA_abs','f8',('lat','lon'),fill_value=miss_val)
setattr(diffBAabsout,'Longname','Difference between ExpB and ExpA')
setattr(diffBAabsout,'units','mm')
setattr(diffBAabsout,'description','ExpB-ExpA SM in absolute values from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

sigBAout=fout.createVariable('sigExpB-ExpA','f8',('lat','lon'),fill_value=miss_val)
setattr(sigBAout,'Longname','Significance in difference between ExpB and ExpA')
setattr(sigBAout,'units','-')
setattr(sigBAout,'description','Statistical significance ExpB-ExpA SM from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

latout[:]=masked_lats[:]
lonout[:]=lons[:]
diffAout[:]=masked_diff_A_CTL[:]
diffAabsout[:]=masked_diff_A_CTL_abs[:]
diffBAout[:]=masked_diff_B_A[:]
diffBAabsout[:]=masked_diff_B_A_abs[:]
sigAout[:]=masked_agree_HA[:]
sigBAout[:]=masked_agree_HBA[:]

# Set global attributes
setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
setattr(fout,"contact","r.lorenz@unsw.edu.au")
setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout,"Script","plot_SM_diff_GCCMIP5_ENSMEAN.py")
setattr(fout,"Input files located in:", indir)
fout.close()
