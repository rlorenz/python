#!/usr/bin/python
'''
File Name : plot_SM_diff_GC1B85-GC1A85.py
Creation Date : 14-01-2015
Last Modified : Mon 20 Jul 2015 13:54:02 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : Plot difference in soil moisture between ExpB and ExpA
	for individual models to help interpreting results


'''
# Load modules for this script
import numpy as np
from scipy.io import netcdf_file
from scipy import stats
import datetime as dt
import netCDF4 as nc
import utils
from subprocess import call
from netcdftime import utime

###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
variables = ['mrso']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1955
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1951
R1_end = 2100

# define analysis start and end year
A_start = 1971
A_end = 2000

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".pdf"

# Loop over models to read all data
for mod in range(len(models)):
# Load netcdf files
    path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'01-'+str(R0_end)+'12_maskocean.nc'
    path1 = indir + models[mod] +'/'+ experiments[1]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[1]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean.nc'
    path2 = indir + models[mod] +'/'+ experiments[2]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[2]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean.nc'
# read variable for 1st run (ctl), read lats and lons as well
    ifile = netcdf_file(path0)
    var = ifile.variables[variables[0]]
    miss_val=var._FillValue
    print miss_val
    if (models[mod] == "EC-EARTH"):
        var0=np.ma.masked_values(var[:,0,:,:],miss_val)
    else:
        var0=np.ma.masked_values(var[:],miss_val)
    lats = ifile.variables['lat'].data
    lons = ifile.variables['lon'].data
    time = ifile.variables['time']
    if (models[mod] == "ECHAM6"):
        dates=time.data
        years=np.floor(dates/10000)
    else:
        cdftime = utime(time.units)
        dates=cdftime.num2date(time.data)
        years=np.asarray([dates[i].year for i in xrange(len(dates))])
    ifile.close()
    # read variable for 2nd run (expA)
    ifile = netcdf_file(path1)
    varA = ifile.variables[variables[0]]
    miss_valA=varA._FillValue
    print miss_valA
    ifile.close()

    # read variable for 3rd run (expB)
    ifile = netcdf_file(path2)
    varB = ifile.variables[variables[0]]
    miss_valB=varB._FillValue
    ifile.close()
    #first cut expA and expB into same length as CTL
    t_diff=(R0_start-R1_start)*12
    if (models[mod] == "EC-EARTH"):
        varA_cut = np.ma.masked_values(varA[t_diff:,0,:,:],miss_valA)
        varB_cut = np.ma.masked_values(varB[t_diff:,0,:,:],miss_valB)
    else:
        varA_cut = np.ma.masked_values(varA[t_diff:,:,:],miss_valA)
        varB_cut = np.ma.masked_values(varB[t_diff:,:,:],miss_valB)
    ntim = varB_cut.shape[0]

    # cut data into analysis period
    ind1 = np.where(years==A_start)[0][0]
    ind2 = np.where(years==A_end)[0][0]+12
    var0_tp = np.ma.copy(var0)
    var0_tp = var0_tp[ind1:ind2,:,:]
    varA_tp = np.ma.copy(varA_cut)
    varA_tp = varA_tp[ind1:ind2,:,:]
    varB_tp = np.ma.copy(varB_cut)
    varB_tp = varB_tp[ind1:ind2,:,:]

    #test if difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_A = np.ndarray((2,len(lats),len(lons)))
    ks_B = np.ndarray((2,len(lats),len(lons)))
    ks_BA = np.ndarray((2,len(lats),len(lons)))
    H_A = np.ndarray((len(lats),len(lons)))
    H_B = np.ndarray((len(lats),len(lons)))
    H_BA = np.ndarray((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_A[:,x,y] = stats.ks_2samp(var0_tp[:,x,y], varA_tp[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_BA[:,x,y] = stats.ks_2samp(varA_tp[:,x,y], varB_tp[:,x,y])
            if ks_BA[1,x,y] < sig:
                H_BA[x,y] = 1
            else:
                H_BA[x,y] = 0

    #average data over time to plot map
    var0_tp_mean = np.ma.mean(var0_tp, axis=0)
    varA_tp_mean = np.ma.mean(varA_tp, axis=0)
    varB_tp_mean = np.ma.mean(varB_tp, axis=0)

    # difference relative in percent
    diff_A_CTL = ((varA_tp_mean-var0_tp_mean)/var0_tp_mean)*100
    diff_B_A = ((varB_tp_mean-varA_tp_mean)/varA_tp_mean)*100
    # absolute difference in mm water column
    diff_A_CTL_abs = (varA_tp_mean-var0_tp_mean)
    diff_B_A_abs = (varB_tp_mean-varA_tp_mean)

    #mask where not statistically significant
    diff_A_CTL_sig=np.ma.masked_where(H_A==0,diff_A_CTL)
    diff_B_A_sig=np.ma.masked_where(H_BA==0,diff_B_A)
    diff_A_CTL_abs_sig=np.ma.masked_where(H_A==0,diff_A_CTL_abs)
    diff_B_A_abs_sig=np.ma.masked_where(H_BA==0,diff_B_A_abs)

   #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    masked_diff_A_CTL = diff_A_CTL_sig[mask_ind,:]
    masked_diff_A_CTL_abs = diff_A_CTL_abs_sig[mask_ind,:]

    masked_HA = H_A[mask_ind,:]
    masked_diff_B_A = diff_B_A_sig[mask_ind,:]
    masked_diff_B_A_abs = diff_B_A_abs_sig[mask_ind,:]
    masked_HBA = H_BA[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    #lev = [-110,-90,-70,-50,-30,-10,10,30,50,70,90,110]
    lev = [-61,-29,-13,-5,-1,1,5,13,29,61]
    lev_a = [-110,-90,-70,-50,-30,-10,10,30,50,70,90,110]
    colbar='BrBG'
    # plot difference GC1A85-CTL
    plotname = outdir +models[mod]+'/'+ experiments[1]+'/'+variables[0]+'_diff_'+experiments[1]+'-CTL_'+str(A_start)+'-'+str(A_end)
    print plotname + plottype
    title = models[mod]+' soil moisture [%] SMclim-CTL '+str(A_start)+'-'+str(A_end)
    utils.draw(masked_diff_A_CTL,masked_lats, lons, title = title, colors=colbar,levels=lev)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir +models[mod]+'/'+ experiments[2]+'/'+variables[0]+'_diff_'+experiments[2]+'-'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)
    print plotname + plottype
    title = models[mod]+' soil moisture [%] SMtrend-SMclim '+str(A_start)+'-'+str(A_end)
    if (models[mod] == 'ACCESS'):
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="a)", colors=colbar,levels=lev)
    elif (models[mod] == 'CESM'):
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="b)", colors=colbar,levels=lev)
    elif (models[mod] == 'EC-EARTH'):        
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="c)", colors=colbar,levels=lev)
    elif (models[mod] == 'ECHAM6'):        
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="d)", colors=colbar,levels=lev)
    elif (models[mod] == 'GFDL'):        
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="e)", colors=colbar,levels=lev)
    elif (models[mod] == 'IPSL'):        
        utils.draw(masked_diff_B_A,masked_lats, lons, title = title, subpanel="f)", colors=colbar,levels=lev)
    else:
    	utils.draw(masked_diff_B_A,masked_lats, lons, title = title, colors=colbar ,levels=lev)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
     
    # plot absolute difference GC1A85-CTL
    plotname = outdir +models[mod]+'/'+ experiments[1]+'/'+variables[0]+'_diff_abs_'+experiments[1]+'-CTL_'+str(A_start)+'-'+str(A_end)
    print plotname + plottype
    title = models[mod]+' soil moisture [mm] SMclim-CTL '+str(A_start)+'-'+str(A_end)
    utils.draw(masked_diff_A_CTL_abs,masked_lats, lons, title = title, colors=colbar,levels=lev_a)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir +models[mod]+'/'+ experiments[2]+'/'+variables[0]+'_diff_abs_'+experiments[2]+'-'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)
    print plotname + plottype
    title = models[mod]+' soil moisture [mm] SMtrend-SMclim '+str(A_start)+'-'+str(A_end)
    if (models[mod] == 'ACCESS'):        
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="a)", colors=colbar,levels=lev_a)
    elif (models[mod] == 'CESM'):        
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="b)", colors=colbar,levels=lev_a)
    elif (models[mod] == 'EC-EARTH'):
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="c)", colors=colbar,levels=lev_a)
    elif (models[mod] == 'ECHAM6'):
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="d)", colors=colbar,levels=lev_a)
    elif (models[mod] == 'GFDL'):
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="e)", colors=colbar,levels=lev_a)
    elif (models[mod] == 'IPSL'):
        utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, subpanel="f)", colors=colbar,levels=lev_a)
    else:
    	utils.draw(masked_diff_B_A_abs,masked_lats, lons, title = title, colors=colbar ,levels=lev_a)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
     
    #also save ensemble differences in netcdf for later panelling
    fout=nc.Dataset('%s/%s/SM_diffs_%s_ANN_%s-%s.nc'%(outdir,models[mod],models[mod],str(A_start)[:4],str(A_end)[:4]),mode='w')
    fout.createDimension('lat',masked_lats.shape[0])
    fout.createDimension('lon',lons.shape[0])

    latout=fout.createVariable('lat','f8',('lat'),fill_value=miss_val)
    setattr(latout,'Longname','Latitude')
    setattr(latout,'units','degrees_north')

    lonout=fout.createVariable('lon','f8',('lon'),fill_value=miss_val)
    setattr(lonout,'Longname','Longitude')
    setattr(lonout,'units','degrees_east')

    diffAout=fout.createVariable('ExpA-CTL','f8',('lat','lon'),fill_value=miss_val)
    setattr(diffAout,'Longname','Difference between ExpA and CTL')
    setattr(diffAout,'units','%')
    setattr(diffAout,'description','ExpA-CTL SM in absolute values from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigAout=fout.createVariable('sigExpA-CTL','f8',('lat','lon'),fill_value=miss_val)
    setattr(sigAout,'Longname','Significance in difference between ExpA and CTL')
    setattr(sigAout,'units','-')
    setattr(sigAout,'description','Statistical significance ExpA-CTL SM from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBAout=fout.createVariable('ExpB-ExpA','f8',('lat','lon'),fill_value=miss_val)
    setattr(diffBAout,'Longname','Difference between ExpB and ExpA')
    setattr(diffBAout,'units','%')
    setattr(diffBAout,'description','ExpB-ExpA SM in percent from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBAabsout=fout.createVariable('ExpB-ExpA_abs','f8',('lat','lon'),fill_value=miss_val)
    setattr(diffBAabsout,'Longname','Difference between ExpB and ExpA')
    setattr(diffBAabsout,'units','mm')
    setattr(diffBAabsout,'description','ExpB-ExpA SM in absolute values from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigBAout=fout.createVariable('sigExpB-ExpA','f8',('lat','lon'),fill_value=miss_val)
    setattr(sigBAout,'Longname','Significance in difference between ExpB and ExpA')
    setattr(sigBAout,'units','-')
    setattr(sigBAout,'description','Statistical significance ExpB-ExpA SM from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    latout[:]=masked_lats[:]
    lonout[:]=lons[:]
    diffAout[:]=masked_diff_A_CTL[:]
    diffBAout[:]=masked_diff_B_A[:]
    diffBAabsout[:]=masked_diff_B_A_abs[:]
    sigAout[:]=masked_HA[:]
    sigBAout[:]=masked_HBA[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
    setattr(fout,"contact","r.lorenz@unsw.edu.au")
    setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout,"Script","plot_SM_diff_GCCMIP5_allGCM.py")
    setattr(fout,"Input files located in:", indir)
    fout.close()
