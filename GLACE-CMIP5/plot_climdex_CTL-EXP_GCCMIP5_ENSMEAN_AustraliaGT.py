#!/usr/bin/python

'''
File Name : plot_climdex_CTL-EXP_GCCMIP5_ENSMEAN.py
Creation Date: 24 Oct 2014
Last Modified: 30 Jan 2015
Author: Ruth Lorenz (r.lorenz@unsw.edu.au), modified by Georgia Tsambos
 - pictures are copied into my own directory
 - only australia is shown
Purpose:
Script that reads and analyses netcdfs from GLACE-CMIP5 project
Data hosted at ETH IAC
Downloaded to storm servers: /srv/ccrc/data32/z3441306/GLACE-CMIP5/
by Ruth Lorenz

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data32/z3441306/GLACE-CMIP5/$model/$experiment/climdex_index/*_remapcon.nc
these files are already remapped to common grid for all models using "cdo remapcon"
Coarsest grid of all GCMs is used (96x96 gridpoints from IPSL)

call within outdir because of pdfcrop!
'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
from scipy import stats
import numpy as np
import utils
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/Australia/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
indices = [ 'TX90p','TN90p', 'TNx', 'TXx','WSDI','R95p','R10mm','CDD','CWD']
#indices = [ 'R10mm']
units = ['(%)', '(%)', '(C' + u'\u00B0' + ')', '(C' + u'\u00B0' + ')', '(days)', '(mm)', '(days)', '(days)', '(days)']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1954
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1950
R1_end = 2100
# define analysis start and end year
A_start = 20550100
A_end = 20840100
#normalize data by mean and std?
normalize = False

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # Load netcdf files
        path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'_remapcon.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'_remapcon.nc'
        path2 = indir + models[mod] +'/'+ experiments[2]+'/climdex_index/'+ models[mod]+'_'+experiments[2]+'_1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'_remapcon.nc'
        # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
        ifile = netcdf_file(path0)
        var0 = ifile.variables['Annual'].data
        lats = ifile.variables['lat'].data
        lons = ifile.variables['lon'].data
        time = ifile.variables['time'].data
        # print information about variable
        #print type(var0)
        #print var0.shape
        #print ifile.variables['Annual'].dimensions
        # read Annual value of ETCCDI Index for 2nd run (expA)
        ifile = netcdf_file(path1)
        varA = ifile.variables['Annual'].data
        # read Annual value of ETCCDI Index for 3rd run (expB)
        ifile = netcdf_file(path2)
        varB = ifile.variables['Annual'].data
        ###
        # cut expA and expB into same length as CTL
        t_diff=R0_start-R1_start
        varA_cut = varA[t_diff:,:,:]
        varB_cut = varB[t_diff:,:,:]
        ntim = varB_cut.shape[0] #no of years

        # if normaliz =TRUE, normalize by removing mean and standard deviation of CTL
        if mod == 0:
            var0_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            varA_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            varB_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            diffA = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            diffB = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            diffBA = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
        if (normalize==True):
            #if (indices[ind] == 'TXx') or (indices[ind] =='TNx') or (indices[ind] =='R10mm') or (indices[ind] =='CDW') or (indices[ind] =='CDD'):
        #Take the mean and std at each grid point
            print ("Normalize the data")
            mean0 = np.ma.mean(var0, axis=0)
            std0 = np.ma.std(var0, axis=0)
            # normalize by mean and std from CTL
            var0_norm[mod,:,:,:] = (var0 - mean0) / std0
            varA_norm[mod,:,:,:] = (varA_cut - mean0) / std0
            varB_norm[mod,:,:,:] = (varB_cut - mean0) / std0
        else:
            var0_norm[mod,:,:,:] = np.ma.copy(var0)
            varA_norm[mod,:,:,:] = np.ma.copy(varA_cut)
            varB_norm[mod,:,:,:] = np.ma.copy(varB_cut)
        #calculate difference between ctl and experiments
        diffA[mod,:,:,:] = varA_norm[mod,:,:,:] - var0_norm[mod,:,:,:]
        diffB[mod,:,:,:] = varB_norm[mod,:,:,:] - var0_norm[mod,:,:,:]
        diffBA[mod,:,:,:] = varB_norm[mod,:,:,:] - varA_norm[mod,:,:,:]

    #calculate ensemble mean (ie. across all models)
    var0_ens = np.mean(var0_norm, axis=0)
    varA_ens = np.mean(varA_norm, axis=0)
    varB_ens = np.mean(varB_norm, axis=0)
    diffA_ens = np.mean(diffA, axis=0)
    diffB_ens = np.mean(diffB, axis=0)
    diffBA_ens = np.mean(diffBA, axis=0)

    # cut ensemble means into analysis period
    ind1 = np.where(time==A_start)[0][0]#?
    ind2 = np.where(time==A_end)[0][0]+1
    var0_ens_tp = var0_ens[ind1:ind2,:,:] #matrix variables are time, lat, lon
    varA_ens_tp = varA_ens[ind1:ind2,:,:]
    varB_ens_tp = varB_ens[ind1:ind2,:,:]
    diffA_ens_tp = diffA_ens[ind1:ind2,:,:]
    diffB_ens_tp = diffB_ens[ind1:ind2,:,:]
    diffBA_ens_tp = diffBA_ens[ind1:ind2,:,:]

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailes p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_A = np.ndarray((2,len(lats),len(lons)))
    ks_B = np.ndarray((2,len(lats),len(lons)))
    ks_BA = np.ndarray((2,len(lats),len(lons)))
    H_A = np.ndarray((len(lats),len(lons)))
    H_B = np.ndarray((len(lats),len(lons)))
    H_BA = np.ndarray((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_A[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varA_ens_tp[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_B[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varB_ens_tp[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0
            ks_BA[:,x,y] = stats.ks_2samp(varA_ens_tp[:,x,y], varB_ens_tp[:,x,y])
            if ks_BA[1,x,y] < sig:
                H_BA[x,y] = 1
            else:
                H_BA[x,y] = 0

    #average data over time to plot map
    diffA_ens_tp_avg = np.mean(diffA_ens_tp, axis=0)
    diffB_ens_tp_avg = np.mean(diffB_ens_tp, axis=0)
    diffBA_ens_tp_avg = np.mean(diffBA_ens_tp, axis=0)# variables are lats, lons

    #mask latitudes and longitudes so that only Australia is showing
    mask_ind = np.where((lats < 0) & (lats > -45))[0]
    mask_ind2 = np.where((lons < 165) & (lons > 100))[0]
   # print mask_ind, mask_ind2
    masked_lats = lats[mask_ind]
    masked_lons = lons[mask_ind2]
   # print masked_lats, masked_lons
   # print(diffA_ens_tp_avg.shape)

    masked_diffA_lats = diffA_ens_tp_avg[mask_ind,:]
    masked_diffA = masked_diffA_lats[:,mask_ind2]
   
    masked_HA_lats = H_A[mask_ind,:]
    masked_HA = masked_HA_lats[:,mask_ind2]

    masked_diffB_lats = diffB_ens_tp_avg[mask_ind,:]
    masked_diffB = masked_diffB_lats[:,mask_ind2]

    masked_HB_lats = H_B[mask_ind,:]
    masked_HB = masked_HB_lats[:,mask_ind2]

    masked_diffBA_lats = diffBA_ens_tp_avg[mask_ind,:]
    masked_diffBA = masked_diffBA_lats[:,mask_ind2]

    masked_HBA_lats = H_BA[mask_ind,:]
    masked_HBA = masked_HBA_lats[:,mask_ind2]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if A_end < 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
            lev = [-2, -1.5, -1, -0.5,  0, 0.5, 1, 1.5, 2]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-26,-22,-18,-14,-10,-6,-2,2,6,10,14,18,22,26]
        elif indices[ind] == 'R95p':
            lev = [-39,-33,-27,-21,-15,-9,-3,3,9,15,21,27,33,39]
        elif indices[ind] == 'WSDI':
            lev = [-52,-44,-36,-28,-20,-12,-4,4,12,20,28,36,44,52]
        else:
            lev = [-18,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,18]
    elif A_end > 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
            lev = [-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6]
            #lev = [ -1, -0.75, -0.5, -0.25, 0,  0.25, 0.5, 0.75, 1]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-18,-15,-12,-9,-6,-3,0,3,6,9,12,15,18]
        elif indices[ind] == 'R95p':
            lev = [-30,-25,-20,-15,-10,-5,0,5,10,15,20,25,30]
            #lev = [-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4, 0.5]
        elif indices[ind] == 'WSDI':
            lev = [-48,-40,-32,-24,-16,-8,0,8,16,24,32,40,48]
        elif indices[ind] == 'R10mm':
            lev = [-3,-2.5,-2,-1.5, -1, -0.5,0, 0.5, 1, 1.5, 2,2.5,3]
        elif (indices[ind] == 'CDD'):
            lev = [-9,-7.5,-6,-4.5,-3,-1.5,0,1.5,3,4.5,6,7.5,9]
        elif (indices[ind] == 'CWD'):
            lev = [-3,-2.5,-2,-1.5, -1, -0.5,0, 0.5, 1, 1.5, 2,2.5,3]
        else:
            lev = [-200,-160,-120,-80,-40,0,40,80,120,160,200]

    # define colorbar depending on index
    if  (indices[ind] == 'R95p') or (indices[ind] == 'R10mm') or (indices[ind] == 'CWD'):      
        colbar='BrBG'
    elif (indices[ind] == 'WSDI'):
        colbar='RdBu_r'
    elif (indices[ind] == 'CDD'):
        colbar='BrBG_r'
    else:
        colbar='RdBu_r'

    #if indices[ind] == 'TNx':
        #print np.ma.mean(masked_HB), masked_HB
        #sys.exit()

    # plot difference CTL-GC1A85 # A-CTL
    plotname = outdir +indices[ind]+ '_' +experiments[1]+ '-' + experiments[0]+ '_' +str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = u'\u03b4' + indices[ind] + ' ' + units[ind]  +', '+experiments[1]+ '-' + experiments[0] +', '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffA,masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=masked_HA, masko="TRUE",region="TRUE")
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-CTL # B-CTL
    plotname = outdir +indices[ind]+ '_' +experiments[2]+'-'+ experiments[0]  + '_' +str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname
    title = u'\u03b4' + indices[ind]+ ' ' + units[ind] +', '+experiments[2]+'-'+ experiments[0] +', '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffB,masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=masked_HB, masko="TRUE",region="TRUE")
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir + indices[ind]+ '_' +experiments[2]+'-'+experiments[1]+ '_' +str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname
    title = u'\u03b4' + indices[ind]+ ' '  + units[ind] +', '+experiments[2]+'-'+experiments[1] +', '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffBA,masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=masked_HBA, masko="TRUE",region="TRUE")
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
