#!/usr/bin/python
'''
File Name : plot_panel_1_jgr.py
Creation Date : 14-07-2015
Last Modified : Tue 14 Jul 2015 14:45:51 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : plot panel for figure 1 temp extremes in jgr extremes GCCMIP5 paper


'''
# Load modules for this script

import numpy as np
import netCDF4 as nc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mc
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap, addcyclic
from pylab import *
from subprocess import call
import utils
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'

A1_start = 1971
A1_end = 2000
A2_start = 2056
A2_end = 2085

plottype= ".eps"

###
# read data
###
pathA = indir + 'TXx_diffs_ENSMEAN_ANN_'+str(A1_start)+'-'+str(A1_end)+'.nc'
print pathA
ifile = nc.Dataset(pathA)
lats = ifile.variables['lat'][:]
lons = ifile.variables['lon'][:]
diffA = ifile.variables['ExpA-CTL']
sigA = ifile.variables['sigExpA-CTL']

pathB = indir + 'TXx_diffs_ENSMEAN_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathB
ifile = nc.Dataset(pathB)
diffB = ifile.variables['ExpB-ExpA']
sigB = ifile.variables['sigExpB-ExpA']

pathC = indir + 'TX90p_diffs_ENSMEAN_ANN_'+str(A1_start)+'-'+str(A1_end)+'.nc'
print pathC
ifile = nc.Dataset(pathC)
diffC = ifile.variables['ExpA-CTL']
sigC = ifile.variables['sigExpA-CTL']

pathD = indir + 'TX90p_diffs_ENSMEAN_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathD
ifile = nc.Dataset(pathD)
diffD = ifile.variables['ExpB-ExpA']
sigD = ifile.variables['sigExpB-ExpA']

pathE = indir + 'HWD_diffs_ENSMEAN_ANN_'+str(A1_start)+'-'+str(A1_end)+'.nc'
print pathE
ifile = nc.Dataset(pathE)
diffE = ifile.variables['ExpA-CTL']
sigE = ifile.variables['sigExpA-CTL']

pathF = indir + 'HWD_diffs_ENSMEAN_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathF
ifile = nc.Dataset(pathF)
diffF = ifile.variables['ExpB-ExpA']
sigF = ifile.variables['sigExpB-ExpA']

plotname = outdir + '/panel1_tex_diff_KSsig_ANN'

max_lat = np.amax(lats)
min_lat = np.amin(lats)
max_lon = np.amax(lons)
min_lon = np.amin(lons)

titleA = 'a) TXx [$^\circ$C] SMclim-CTL '+str(A1_start)+'-'+str(A1_end)
titleB = 'b) TXx [$^\circ$C] SMtrend-SMclim '+str(A2_start)+'-'+str(A2_end)
titleC = 'c) TX90p [%] SMclim-CTL '+str(A1_start)+'-'+str(A1_end)
titleD = 'd) TX90p [%] SMtrend-SMclim '+str(A2_start)+'-'+str(A2_end)
titleE = 'e) HWD [days] SMclim-CTL '+str(A1_start)+'-'+str(A1_end)
titleF = 'f) HWD [days] SMtrend-SMclim '+str(A2_start)+'-'+str(A2_end)

diff=[diffA,diffB,diffC,diffD,diffE,diffF]
sig=[sigA,sigB,sigC,sigD,sigE,sigF]
titles=[titleA,titleB,titleC,titleD,titleE,titleF]
colbar='RdBu_r'

plt.close('all')
###
# plotting part using gridspec
###
fig=plt.figure(figsize=(12,8))
gs = gridspec.GridSpec(3,3,width_ratios=[1,1,0.03],hspace=0.1,wspace=0.1,top=0.95,right=0.95,left=0.05,bottom=0.05)

pltno=0
for row in range(0,3):
    for col in range(0,2):
        print pltno
        ax = plt.subplot(gs[row, col])

        if (pltno <= 1):
            levels = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
        elif ((pltno > 1) and (pltno<=3)):
            levels = [-9,-7,-5,-3,-1,1,3,5,7,9]
        else: 
            levels = [-20,-16,-12,-8,-2,2,8,12,16,20]

        m = Basemap(projection='cyl',llcrnrlat=min_lat,urcrnrlat=max_lat, llcrnrlon=-180,urcrnrlon=180,resolution='c')
        m.drawcoastlines()
        if (pltno%2 == 0):
            dpl = 1
        else:
            dpl = 0
        if (row == 2):
            dml = 1
        else:
            dml = 0
        m.drawparallels(np.arange(-90.,91.,30.), labels=[dpl,0,0,0])
        m.drawmeridians(np.arange(-180.,181.,60.), labels=[0,0,0,dml])

        diff[pltno], lonsnew = addcyclic(diff[pltno], lons)
        lons2d, lats2d = np.meshgrid(lonsnew, lats)
        x, y = m(lons2d, lats2d)

        sig[pltno], lonsnew = addcyclic(sig[pltno],lons)

        cmap=plt.get_cmap(colbar)
        norm=mc.BoundaryNorm(levels, cmap.N)

        cs = ax.contourf(x,y,diff[pltno],levels,cmap=cmap,norm=norm,extend='both')
        hat = ax.contourf(x,y,sig[pltno],[-0.5,0.5,2],hatches=[None,'xxx'],alpha=0.0)

        plt.title(titles[pltno],size=15,ha='left',x=0)
    
        if (pltno == 0):
            name=['CNA','SAF','MED','NAU']
            lonmin=[-110,-10,-10.0,110]
            lonmax=[-85,51.99,40,155]
            latmin=[25,-35,30,-30]
            latmax=[50,-11.365,45,-10]
            for r in range(len(name)):
                utils.plot_rectangle(m,lonmin[r],lonmax[r],latmin[r],latmax[r])
                plt.text(lonmin[r]-2,latmin[r],name[r],fontsize=10,fontweight='bold',
                    ha='right',va='bottom',color='k')
            
        if (pltno%2 ==0 ):
            axC = fig.add_subplot(gs[row,2])
            cbar = fig.colorbar(cs,ax=ax,cax=axC)
            cbar.ax.tick_params(labelsize=15)
        
        pltno=pltno+1

#gs.tight_layout(fig)
fig1=plt.gcf()
plt.show()
plt.draw()
fig1.savefig(plotname+plottype,dpi=fig.dpi)
if (plottype=='.eps'):
    call("epstopdf "+plotname+plottype, shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

