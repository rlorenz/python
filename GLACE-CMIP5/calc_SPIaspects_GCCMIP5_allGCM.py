#!/usr/bin/python
'''
File Name : plot_SPI_GCCMIP5_allGCM.py
Creation Date: 17/11/2014
Last Modified: 
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
script that plots different aspects of Standard Precipitation Index
calculated for all GLACE-CMIP5 GCMs

Experiments: CTL, GC1A85, GC1B85

SPI already calculated in R:
/home/z3441306/scripts/plot_scripts/R_scripts/calc_SPI_GCCMIP5.R
saved in:
/srv/ccrc/data32/z3441306/GLACE-CMIP5/$MODEL/$EXP/$MODEL_$EXP_1950-2100_SPI.nc

Use SPI<=-1.245 as threshold, this includes severe and extreme droughts
(probability ~10%)

Gives 5 characteristics of droughts (in alignment with Heat Wave charateristics)
Yearly maximum drought intensity
Yearly average drought intensity
Yearly number of drought months
Yearly number of drought events
Duration of longest drought period per year
'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
from scipy import stats
import numpy as np
import datetime as dt
from netcdftime import utime
import netCDF4 as nc
#from constants import const
import utils
from subprocess import call
call("module load cdo",shell=True)
###
# Define input
###
#models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
models = ['ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']

# start and end of runs (adjust R0_start for models/experiments
# where different within loop)
R0_start = 1950
R0_end = 2100

# define analysis start and end year
A_start = 1971
A_end = 2000

#define format of figures
plottype = ".pdf"

# Loop over models
for mod in range(len(models)):
    print "Model: %s" %(models[mod])
    for exp in range(len(experiments)):
        print "Experiment: %s" %(experiments[exp])
###
        indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod]+'/' +experiments[exp]+'/SPI/'
        outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod]+'/' +experiments[exp]+'/SPI/'

        if (models[mod]=="CESM") and (experiments[exp]=="CTL"):
            R0_start = 1955
        elif (models[mod]=="GFDL"):
            R0_start = 1951
        else:
            R0_start = 1950

        # Load netcdf files
        path0 = indir + models[mod]+'_'+experiments[exp]+'_'+str(R0_start)+'-'+str(R0_end)+'_SPI.nc'

        # read SPI for run, read lats and lons as well
        ifile = netcdf_file(path0)
        var0 = ifile.variables['SPI']
        miss_val=var0._FillValue
        lat = ifile.variables['lat']
        lon = ifile.variables['lon']
        time = ifile.variables['time']
        # print information about variable
        #print type(var0)
        #print var0.shape
        #print ifile.variables['SPI'].dimensions
        ntim = var0.shape[0]
        nlat = var0.shape[1]
        nlon = var0.shape[2]
        #convert time to actual dates
        cdftime = utime(time.units)
        dates = cdftime.num2date(time.data)
        years=np.asarray([dates[i].year for i in xrange(len(dates))])
        dates_A=dates[(years>=A_start) & (years<=A_end)]
        nyears=A_end-A_start+1
        #
        # loop over years
        # define arrays holding metrics
        SPIA_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        SPIM_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        SPIN_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        SPIF_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        SPID_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        SPIT_yearly=np.zeros((nyears,)+var0.shape[1:],np.float64)
        #
        for year in range(nyears):
            this_year=year+A_start
            print "processing year: %s" %(this_year)
            #selecting variables for the running year
            spi_y=var0[years==this_year,:,:].copy()
            #make sure missing values from SPI data are taken care of
            spi_y_masked=np.ma.masked_values(spi_y,miss_val)
            years_y=years[years==this_year]
            # calculate dry spells SPI<=1.5
            exceed=np.zeros(spi_y.shape,dtype=np.bool)
            exceed[spi_y_masked<=-1.245]=True
            spell=np.zeros(spi_y.shape,dtype=np.int)
            # If the period starts with a drought, set that in the spell counter variable
            spell[0,exceed[0,:,:]==True]=1
            #
            #Calculate the spells for the rest of the period
            for t in xrange(1,11):
                # Count the number of consecutive drought days
                spell[t,exceed[t,:,:]==True]=spell[t-1,exceed[t,:,:]==True]+1
	    # Count the length of each of the spells
            spell_final=np.zeros(var0.shape,dtype=np.int)
            #identify negative changes in spell to locate the end of the droughts
            spell_final[np.diff(spell,axis=0)<0]=spell[np.diff(spell,axis=0)<0]
            spell_final[-1,:,:]=spell[-1,:,:]   # The period might end in a drought
            # Define SPI variables
            spi_avg=np.ma.zeros(spi_y.shape,dtype=np.float64)
            spi_peak=np.ma.zeros(spi_y.shape,dtype=np.float64)
            spi=np.ma.zeros(spi_y.shape,dtype=np.float64)
            #
            # Calculate the peak and the average of the drought periods
            for i in xrange(nlat):
                for j in xrange(nlon):
                    for t in xrange(12):
                    #for each time that there is a drought,
                    #locate the beginning and compute the mean and the peak
                        spell_l=spell_final[t,i,j] 
                        if t<=spell_l:
                            spi_peak[t,i,j]=np.ma.min(spi_y_masked[0:t+1,i,j])
                            spi_avg[t,i,j]=np.ma.mean(spi_y_masked[0:t+1,i,j])
                            spi[0:t+1,i,j]=np.ma.copy(spi_y_masked[0:t+1,i,j])
                        else:
                            spi_avg[t,i,j]=np.ma.mean(spi_y_masked[t-spell_l:t+1,i,j])
                            spi[t-spell_l:t+1,i,j]=np.ma.copy(spi_y_masked[t-spell_l:t+1,i,j])
                            spi_peak[t,i,j]=np.ma.min(spi_y_masked[t-spell_l:t+1,i,j])

	    #print np.ma.min(spi), np.ma.max(spi)
	    #print np.ma.min(spi_peak), np.ma.max(spi_peak)
            #
            # pull out 5 characteristics based on 3 arrays calculated above
            spi_avg_masked=np.ma.masked_equal(spi_avg,0)
            spi_peak_masked=np.ma.masked_equal(spi_peak,0)
            #
            SPIA_yearly[year,:,:]=np.ma.min(spi_peak,axis=0)
            SPIM_yearly[year,:,:]=np.ma.mean(spi_peak,axis=0)
            SPIF_yearly[year,:,:]=np.ma.sum(spi_y_masked,axis=0)*100./np.float(np.ma.sum(years_y==A_start+year))
            SPIN_yearly[year,:,:]=np.ma.sum(spell_final>0,axis=0)
            SPID_yearly[year,:,:]=np.ma.max(spell_final,axis=0)
            SPIT_yearly[year,:,:]=np.ma.argmax(spell_final>0,axis=0)
            #SPIM_yearly[SPIM_yearly==0]=-9999
            #SPIA_yearly[SPIA_yearly==0]=-9999
            SPIT_yearly[SPIT_yearly<0]=0
            #print np.ma.min(SPIA_yearly), np.ma.max(SPIA_yearly)
            #print np.ma.min(SPIF_yearly), np.ma.max(SPIF_yearly)
            #print np.ma.min(SPID_yearly), np.ma.max(SPID_yearly)

        #save data as netcdf
        dates_out=[dt.datetime(A_start+x,06,01,00) for x in range(0,nyears)]
        #time_out=nc.date2num(dates_out,units="days since 1970-01-01 00:00:00", '%Y-%m-%d_%H:%M:%S',calendar='standard')
        time_out=nc.date2num(dates_out,units="days since 1970-01-01 00:00:00",calendar='standard')

        fout=nc.Dataset('%s/%s_%s_%s-%s_SPIaspects.nc'%(outdir,models[mod],experiments[exp],A_start,A_end),mode='w')
        fout.createDimension('time',len(dates_out))
        fout.createDimension('lat',lat.shape[0])
        fout.createDimension('lon',lon.shape[0])

        timeout=fout.createVariable('time','f8','time',fill_value=-9999)
        setattr(timeout,'units','days since 1970-01-01 00:00:00')

        latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
        setattr(latout,'Longname','Latitude')
        setattr(latout,'units','degrees_north')

        lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
        setattr(lonout,'Longname','Longitude')
        setattr(lonout,'units','degrees_east')

        SPIAout=fout.createVariable('SPIA','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPIAout,'Longname','Peak of the severest drought per year')
        setattr(SPIAout,'units','-')
        setattr(SPIAout,'description','Peak of the severest drought per year - yearly maximum of each drought peak')

        SPIMout=fout.createVariable('SPIM','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPIMout,'Longname','Average magnitude of the yearly droughts')
        setattr(SPIMout,'units','-')
        setattr(SPIMout,'description','Average magnitude of the yearly droughts - yearly average of drought magnitude')

        SPINout=fout.createVariable('SPIN','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPINout,'Longname','Number of droughts')
        setattr(SPINout,'units','-')
        setattr(SPINout,'description','Number of droughts per year')

        SPIFout=fout.createVariable('SPIF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPIFout,'Longname','Number of drought months')
        setattr(SPIFout,'units','-')
        setattr(SPIFout,'description','Number of drought months - expressed as the percentage relative to the total number of months')

        SPIDout=fout.createVariable('SPID','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPIDout,'Longname','Duration of yearly longest drought')
        setattr(SPIDout,'units','-')
        setattr(SPIDout,'description','Duration of the longest drought per year')

        SPITout=fout.createVariable('SPIT','f8',('time','lat','lon'),fill_value=-9999)
        setattr(SPITout,'Longname','First drought month of the year')
        setattr(SPITout,'units','-')
        setattr(SPITout,'description','First drought month of the year from January')

        timeout[:]=time_out
        latout[:]=lat[:]
        lonout[:]=lon[:]

        SPIAout[:]=SPIA_yearly[:]
        SPIMout[:]=SPIM_yearly[:]
        SPINout[:]=SPIN_yearly[:]
        SPIFout[:]=SPIF_yearly[:]
        SPIDout[:]=SPID_yearly[:]
        SPITout[:]=SPIT_yearly[:]

        # Set global attributes
        setattr(fout,"author","Ruth Lorenz")
        setattr(fout,"contact","r.lorenz@unsw.edu.au")
        setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
        setattr(fout,"comment","Different aspects of SPI from GLACE-CMIP5 experiments")
        setattr(fout,"Script","calc_SPI_aspects_GCCMIP5_allGCMs.py")
        setattr(fout,"Input files located in:", indir)
        fout.close()

        #remap all models to same grid (coarsest from IPSL)
        call("cdo remapcon,/srv/ccrc/data32/z3441306/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc "+outdir+models[mod]+"_"+experiments[exp]+"_"+str(A_start)+"-"+str(A_end)+"_SPIaspects.nc " +outdir+models[mod]+"_"+experiments[exp]+"_"+str(A_start)+"-"+str(A_end)+"_SPIaspects_remapcon.nc",shell=True)
