#!/usr/bin/python
'''
File Name : plot_panel_7_jgr.py
Creation Date : 16-07-2015
Last Modified : Tue 14 Jul 2015 14:45:51 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : plot panel for figure 7 soil moisture in jgr extremes GCCMIP5 paper


'''
# Load modules for this script

import numpy as np
import netCDF4 as nc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mc
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap, addcyclic, maskoceans, shiftgrid
from pylab import *
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'

A1_start = 1971
A1_end = 2000
A2_start = 2056
A2_end = 2085

plottype= ".eps"

###
# read data
###
pathA = indir + 'ACCESS/SM_diffs_ACCESS_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathA
ifile = nc.Dataset(pathA)
latsA = ifile.variables['lat'][:]
lonsA = ifile.variables['lon'][:]
diffA = ifile.variables['ExpB-ExpA']
sigA = ifile.variables['sigExpB-ExpA']
diffA=np.ma.masked_values(diffA,diffA._FillValue)

pathB = indir + 'CESM/SM_diffs_CESM_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathB
ifile = nc.Dataset(pathB)
latsB = ifile.variables['lat'][:]
lonsB = ifile.variables['lon'][:]
diffB = ifile.variables['ExpB-ExpA']
sigB = ifile.variables['sigExpB-ExpA']
diffB=np.ma.masked_values(diffB,diffB._FillValue)

pathC = indir + 'EC-EARTH/SM_diffs_EC-EARTH_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathC
ifile = nc.Dataset(pathC)
latsC = ifile.variables['lat'][:]
lonsC = ifile.variables['lon'][:]
diffC = ifile.variables['ExpB-ExpA']
sigC = ifile.variables['sigExpB-ExpA']
diffC=np.ma.masked_values(diffC,diffC._FillValue)

pathD = indir + 'ECHAM6/SM_diffs_ECHAM6_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathD
ifile = nc.Dataset(pathD)
latsD = ifile.variables['lat'][:]
lonsD = ifile.variables['lon'][:]
diffD = ifile.variables['ExpB-ExpA']
sigD = ifile.variables['sigExpB-ExpA']
diffD=np.ma.masked_values(diffD,diffD._FillValue)

pathE = indir + 'GFDL/SM_diffs_GFDL_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathE
ifile = nc.Dataset(pathE)
latsE = ifile.variables['lat'][:]
lonsE = ifile.variables['lon'][:]
diffE = ifile.variables['ExpB-ExpA']
sigE = ifile.variables['sigExpB-ExpA']
diffE=np.ma.masked_values(diffE,diffE._FillValue)

pathF = indir + 'IPSL/SM_diffs_IPSL_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathF
ifile = nc.Dataset(pathF)
latsF = ifile.variables['lat'][:]
lonsF = ifile.variables['lon'][:]
diffF = ifile.variables['ExpB-ExpA']
sigF = ifile.variables['sigExpB-ExpA']
diffF=np.ma.masked_values(diffF,diffF._FillValue)

pathG = indir + 'ENS_MEAN/SM_diffs_ENSMEAN_ANN_'+str(A2_start)+'-'+str(A2_end)+'.nc'
print pathG
ifile = nc.Dataset(pathG)
latsG = ifile.variables['lat'][:]
lonsG = ifile.variables['lon'][:]
diffG = ifile.variables['ExpB-ExpA']
sigG = ifile.variables['sigExpB-ExpA']
diffG=np.ma.masked_values(diffG,diffG._FillValue)

plotname = outdir + '/panel7_sm_diff_KSsig_ANN'

titleA = 'a) ACCESS'
titleB = 'b) CESM'
titleC = 'c) EC-EARTH'
titleD = 'd) ECHAM6'
titleE = 'e) GFDL'
titleF = 'f) IPSL'
titleG = 'g) ENSMEAN'

diff=[diffA,diffB,diffC,diffD,diffE,diffF,diffG]
sig=[sigA,sigB,sigC,sigD,sigE,sigF,sigG]
titles=[titleA,titleB,titleC,titleD,titleE,titleF,titleG]
lats=[latsA,latsB,latsC,latsD,latsE,latsF,latsG]
lons=[lonsA,lonsB,lonsC,lonsD,lonsE,lonsF,lonsG]

colbar='BrBG'

plt.close('all')
###
# plotting part using gridspec
###
fig=plt.figure(figsize=(10,25))
fig.suptitle('SM [%] SMtrend-SMclim '+str(A2_start)+'-'+str(A2_end), size=15)
gs = gridspec.GridSpec(4,2,width_ratios=[1,1],height_ratios=[1,1,1,1],hspace=0.1,wspace=0.1,top=0.93,right=0.95,left=0.05,bottom=0.05)

pltno=0
for row in range(0,4):
    for col in range(0,2):
        print pltno
        if (pltno<6):
            ax = plt.subplot(gs[row, col])
        else:
            ax = plt.subplot(gs[row, col:])

        levels = [-61,-29,-13,-5,-1,1,5,13,29,61]

        max_lat = np.amax(lats[pltno])
        min_lat = np.amin(lats[pltno])
        max_lon = np.amax(lons[pltno])
        min_lon = np.amin(lons[pltno])

        m = Basemap(projection='cyl',llcrnrlat=min_lat,urcrnrlat=max_lat, llcrnrlon=-180,urcrnrlon=180,resolution='c')
        m.drawcoastlines()
        if (pltno%2 == 0):
            dpl = 1
        else:
            dpl = 0
        if (row == 3):
            dml = 1
        else:
            dml = 0
        m.drawparallels(np.arange(-90.,91.,30.), labels=[dpl,0,0,0])
        m.drawmeridians(np.arange(-180.,181.,60.), labels=[0,0,0,dml])
 
        if (min_lon >= 0) & (max_lon > 180):
            lons_orig = lons[pltno]
            diff[pltno],lons[pltno] =  shiftgrid(180., diff[pltno], lons[pltno],start=False)
            sig[pltno],lons[pltno] =  shiftgrid(180., sig[pltno], lons_orig,start=False)

        diff[pltno], lonsnew = addcyclic(diff[pltno], lons[pltno])
        lons2d, lats2d = np.meshgrid(lonsnew, lats[pltno])
        x, y = m(lons2d, lats2d)

        #diff[pltno] = maskoceans(lons2d, lats2d, diff[pltno])

        sig[pltno], lonsnew = addcyclic(sig[pltno],lons[pltno])
        #sig[pltno] = maskoceans(lons2d, lats2d, sig[pltno])

        cmap=plt.get_cmap(colbar)
        norm=mc.BoundaryNorm(levels, cmap.N)

        cs = ax.contourf(x,y,diff[pltno],levels,cmap=cmap,norm=norm,extend='both')
        if (pltno ==6 ):
            hat = ax.contourf(x,y,sig[pltno],[-0.5,0.5,2],hatches=[None,'///'],alpha=0.0)

        plt.title(titles[pltno],size=15,ha='left',x=0)
    
        if (pltno ==6 ):
            gs01 = gridspec.GridSpecFromSubplotSpec(100, 100, subplot_spec=gs[7])
            axC = fig.add_subplot(gs01[:,55:60])
            cbar = fig.colorbar(cs,ax=ax,cax=axC)
            cbar.ax.tick_params(labelsize=15)
        
        #del lonsnew
        pltno=pltno+1
        if (pltno==7):
            break

#gs.tight_layout(fig)
fig1=plt.gcf()
plt.show()
plt.draw()
fig1.savefig(plotname+plottype,dpi=fig.dpi)
if (plottype=='.eps'):
    call("epstopdf "+plotname+plottype, shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

