#!/usr/bin/python
'''
File Name : plot_SM_timeseries_GCCMIP5_allGCM_regions.py
Creation Date : 12-01-2015
Last Modified : Mon 12 Jan 2015 08:44:02 AEDT
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : plot soil moisture timeseries for all experiments
	and models over analysis regions to help interpretation
	of results


'''
# Load modules for this script
import numpy as np
from scipy.io import netcdf_file
import matplotlib.pyplot as plt
import datetime as dt
import netCDF4 as nc
from netcdftime import utime
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
#models = ['ECHAM6']
experiments = ['CTL','GC1A85','GC1B85']
variables = ['mrso','mrsos']
regions=["CNA","EAS","MED","SAS","NAU","EAF","SAF","NEB"]

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1955
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1951
R1_end = 2100

#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".pdf"

#define function for calculating moving average
def movingaverage (values, window):
    weights = np.ones(int(window))/float(window)
    sma = np.convolve(values, weights, 'valid')
    return sma

# Loop over variables
for v in range(len(variables)):
    print variables[v]
    # Loop over models to read all data
    for mod in range(len(models)):
        if (models[mod] == "ECHAM6") and (variables[v]=="mrsos"): #mrsos does not exist for ECHAM6
            continue
        ###
        # Load netcdf files
        path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ variables[v]+'_monthly_' + models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'01-'+str(R0_end)+'12_maskocean.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/'+ variables[v]+'_monthly_' + models[mod]+'_'+experiments[1]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean.nc'
        path2 = indir + models[mod] +'/'+ experiments[2]+'/'+ variables[v]+'_monthly_' + models[mod]+'_'+experiments[2]+'_1_'+str(R1_start)+'01-'+str(R1_end)+'12_maskocean.nc'
        # read variable for 1st run (ctl), read lats and lons as well
        ifile = netcdf_file(path0)
        var = ifile.variables[variables[v]]
        miss_val=var._FillValue
        if (models[mod] == "EC-EARTH"):
            var0=np.ma.masked_values(var[:,0,:,:],miss_val)
        else:
            var0=np.ma.masked_values(var[:],miss_val)
        lats = ifile.variables['lat'].data
        lons = ifile.variables['lon'].data
        time = ifile.variables['time']
        if (models[mod] == "ECHAM6"):
            dates=time.data
        else:
            cdftime = utime(time.units)
            dates=cdftime.num2date(time.data)
        # print information about variable
        #print type(var0)
        #print var0.shape
        #print ifile.variables[variables[v]].dimensions
        # read variable for 2nd run (expA)
        ifile = netcdf_file(path1)
        varA = ifile.variables[variables[v]]
        miss_valA=varA._FillValue
        # read variable for 3rd run (expB)
        ifile = netcdf_file(path2)
        varB = ifile.variables[variables[v]]
        miss_valB=varB._FillValue
        #first cut expA and expB into same length as CTL
        t_diff=(R0_start-R1_start)*12
        if (models[mod] == "EC-EARTH"):
            varA_cut = np.ma.masked_values(varA[t_diff:,0,:,:],miss_valA)
            varB_cut = np.ma.masked_values(varB[t_diff:,0,:,:],miss_valB)
        else:
            varA_cut = np.ma.squeeze(np.ma.masked_values(varA[t_diff:,:,:],miss_valA))
            varB_cut = np.ma.squeeze(np.ma.masked_values(varB[t_diff:,:,:],miss_valB))

        #loop over regions
        #mask region
        for reg in range(len(regions)):
            print regions[reg]
            mask=np.loadtxt('/home/z3441306/scripts/plot_scripts/areas_txt/'+regions[reg]+'.txt')
            if (regions[reg]=="SAS"):
                maxlon=mask[0,0]
                minlon=mask[4,0]
                maxlat=mask[0,1]
                minlat=mask[3,1]
            else:
                maxlon=mask[0,0]
                minlon=mask[2,0]
                maxlat=mask[0,1]
                minlat=mask[1,1]

            if maxlon<0:
                maxlon=maxlon+360
            elif minlon<0:
                minlon=minlon+360

            mask_ind_lats=np.where((lats<maxlat) & (lats>minlat))[0]
            masked_lats = lats[mask_ind_lats]
            if maxlon > minlon:
                mask_ind_lons=np.where((lons<maxlon) & (lons>minlon))[0]
                masked_lons = lons[mask_ind_lons]
            else:
                mask_ind_lons=np.where((lons<maxlon) | (lons>minlon))[0]
                masked_lons = lons[mask_ind_lons]

            #var0_tmp=np.ma.masked_values(var0[:,mask_ind_lats,:],miss_val0)
            #var0_masked=np.ma.masked_values(var0_tmp[:,:,mask_ind_lons],miss_val0)
            var0_masked=np.ma.copy(var0)
            var0_masked=var0_masked[:,mask_ind_lats][...,mask_ind_lons]

            varA_masked=np.ma.copy(varA_cut)
            varA_masked=varA_masked[:,mask_ind_lats][...,mask_ind_lons]

            varB_masked=np.ma.copy(varB_cut)
            varB_masked=varB_masked[:,mask_ind_lats][...,mask_ind_lons]

             # First we need to convert the latitudes to radians
            latr = np.deg2rad(masked_lats)

            # Use the cosine of the converted latitudes as weights for the average
            weights = np.cos(latr)

            # First find the zonal mean by averaging along the latitude circles
            var0_mean_zonal = np.ma.mean(var0_masked, axis=2)
            varA_mean_zonal = np.ma.mean(varA_masked, axis=2)
            varB_mean_zonal = np.ma.mean(varB_masked, axis=2)

            # Then take the weighted average of those using the weights we calculated earlier
            var0_mean = np.ma.average(var0_mean_zonal, axis=1, weights=weights)
            #print var0_mean.shape, dates.shape
            varA_mean = np.ma.average(varA_mean_zonal, axis=1, weights=weights)
            #print varA_mean.shape
            varB_mean = np.ma.average(varB_mean_zonal, axis=1, weights=weights)
            #print varB_mean.shape

            #calculate 13month moving average
            var0_mov_avg = movingaverage(var0_mean,13)
            varA_mov_avg = movingaverage(varA_mean,13)
            varB_mov_avg = movingaverage(varB_mean,13)
            #print var0_mov_avg.shape
            ntim = dates.shape[0]
            #print ntim
            #plot timeseries
            fig = plt.figure(figsize=(10, 5),dpi=300)
            plt.plot(dates[6:(ntim-6)],var0_mov_avg,'k-',label="CTL",linewidth=2.0)
            plt.plot(dates[6:(ntim-6)],varA_mov_avg,'r-',label="ExpA",linewidth=2.0)
            plt.plot(dates[6:(ntim-6)],varB_mov_avg,'b-',label="ExpB",linewidth=2.0)
            plt.xlabel('Year')
            plt.ylabel('Soil Moisture [kg m$^-2$]') #can use LaTeX for subscripts etc.
            plt.grid(True)

            leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
            leg.draw_frame(False)

            plt.savefig(outdir+models[mod]+'/'+variables[v]+'_'+models[mod]+'_timeseries_30ymoveavg_'+str(R0_start)+'-'+str(R0_end)+'_'+regions[reg]+'.pdf')
