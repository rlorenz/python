#!/usr/bin/python

'''
File Name : SPI_intensity_CTL-EXP_GCCMIP5_ENSMEAN.py
Creation Date: 15 Jan 2015
Last Modified: 21 Jan 2015
Author:  Georgia Tsambos (g.tsambos@gmail.com)

Purpose:
Script that calculates SPI drought intensity, plots maps of the difference in drought intensity and saves the drought intensity to a netcdf.
For a given gridpoint, SPI intensity is calculated by summing the SPI of every month which lies
in a drought period. A drought period is a period of consectutive months in which SPI is consistently < 0, and which contains at least one month with SPI < -1.
McKee et al (1993) suggest that the SPI intensity can be roughly interpreted as a number of months; a drought with drought intensity -n represents n months of drought with intensity -1. 

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution -> need to regrid to common grid. 

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period.

'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
from scipy import stats
import numpy as np
import numpy.ma as ma
from netcdftime import utime
import utils
from subprocess import call
import sys

###
# Define input
###
indir = '/srv/ccrc/data41/z3282408/SPI'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/Australia/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']

# start and end of 1st run in analysis (CTL data starts later than experiments)
#R0_start = 1954
#R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
#R_start = 1950
#R_end = 2100
# define analysis start and end year
A_start = 2055
A_end = 2084
#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"
#define map colours and levels
colbar='BrBG_r'
lev = [-100,-75,-50,-25,0,25,50,75,100]

####
# Before we do all models, need to:

# check that all files have the same time units

#Loop over models
for mod in range(len(models)):
    print models[mod]

    #Define file paths
    if (models[mod] == 'CESM'):
        path0 = indir + '/' + models[mod] + '/' + experiments[0] + '/' + models[mod] + '_' + experiments[0] + '_' + '1955-2100' + '_' + 'SPI_maskocean_remapcon.nc'
    elif (models[mod] == 'GFDL'):
        path0 = indir + '/' + models[mod] + '/' + experiments[0] + '/' + models[mod] + '_' + experiments[0] + '_' + '1951-2100' + '_' + 'SPI_maskocean_remapcon.nc'
    else:
        path0 = indir + '/' + models[mod] + '/' + experiments[0] + '/' + models[mod] + '_' + experiments[0] + '_' + '1950-2100' + '_' + 'SPI_maskocean_remapcon.nc'

    if (models[mod] == 'GFDL'):
        path1 = indir + '/' + models[mod] + '/' + experiments[1] + '/' + models[mod] + '_' + experiments[1] + '_' + '1951-2100' + '_' + 'SPI_maskocean_remapcon.nc'
        path2 = indir + '/' + models[mod] + '/' + experiments[2] + '/' + models[mod] + '_' + experiments[2] + '_' + '1951-2100' + '_' + 'SPI_maskocean_remapcon.nc'
    else:
        path1 = indir + '/' + models[mod] + '/' + experiments[1] + '/' + models[mod] + '_' + experiments[1] + '_' + '1950-2100' + '_' + 'SPI_maskocean_remapcon.nc'
        path2 = indir + '/' + models[mod] + '/' + experiments[2] + '/' + models[mod] + '_' + experiments[2] + '_' + '1950-2100' + '_' + 'SPI_maskocean_remapcon.nc'

    #Read in SPI data
    ifile = netcdf_file(path0)
    spi_0 = ifile.variables['SPI'].data
    lats = ifile.variables['lat'].data
    lons = ifile.variables['lon'].data
    time = ifile.variables['time'].data

    ifile = netcdf_file(path1)
    spi_1 = ifile.variables['SPI'].data

    ifile = netcdf_file(path2)
    spi_2 = ifile.variables['SPI'].data

    #Convert time variable into dates
    if models[mod] == 'ACCESS':
        cdftime = utime("days since 1970-01-01 00:00:00")
    else:
        cdftime = utime("days since 1950-01-01 00:00:00")
    dates = cdftime.num2date(time)
    years = np.asarray([dates[i].year for i in xrange(len(dates))])
    dates_A=dates[(years>=A_start) & (years<=A_end)]
    nyears=A_end-A_start+1

    #Cut variables into analysis period (2081-2100)
    #mask latitudes and longitudes so that only Australia is showing
    ind_start = np.where(years == A_start)[0][0]
    ind_end = np.where(years == A_end)[0][-1]
    spi_0_cut = spi_0[ind_start:ind_end, :, :]
    spi_1_cut = spi_1[ind_start:ind_end, :, :]
    spi_2_cut = spi_2[ind_start:ind_end, :, :]
    mask_ind = np.where((lats < 0) & (lats > -45))[0]
    mask_ind2 = np.where((lons < 165) & (lons > 100))[0]
    masked_lats = lats[mask_ind]
    masked_lons = lons[mask_ind2]

    spi_0_cut_Aust = spi_0_cut[:, mask_ind, :]
    spi_0_cut_Aust = spi_0_cut_Aust[:, :, mask_ind2]
    spi_1_cut_Aust = spi_1_cut[:, mask_ind, :]
    spi_1_cut_Aust = spi_1_cut_Aust[:, :, mask_ind2]
    spi_2_cut_Aust = spi_2_cut[:, mask_ind, :]
    spi_2_cut_Aust = spi_2_cut_Aust[:, :, mask_ind2]

    #Mask missing values (entered as -999)
    spi_0_cut_Aust = ma.array(spi_0_cut_Aust, mask = np.less(spi_0_cut_Aust, -998))
    spi_1_cut_Aust = ma.array(spi_1_cut_Aust, mask = np.less(spi_1_cut_Aust, -998))
    spi_2_cut_Aust = ma.array(spi_2_cut_Aust, mask = np.less(spi_2_cut_Aust, -998))

    ####
    # Calculate total SPI intensity at each gridpoint over 2081-2100 
    ####
    # a logical array indicating when SPI < 0
    neg_0 = np.ma.less(spi_0_cut_Aust, 0)
    neg_1 = np.ma.less(spi_1_cut_Aust, 0)
    neg_2 = np.ma.less(spi_2_cut_Aust, 0)
    # a logical array indicating when SPI < -1
    dry_0 = np.ma.less(spi_0_cut_Aust, -1)
    dry_1 = np.ma.less(spi_1_cut_Aust, -1)
    dry_2 = np.ma.less(spi_2_cut_Aust, -1)
    # an logical array to indicate which SPI values belong to a drought period
    # (to be filled in later: dimensions and masked values imported from spi_0_cut_Aust
    drought_0 = np.empty_like(spi_0_cut_Aust) 
    drought_1 = np.empty_like(spi_1_cut_Aust)
    drought_2 = np.empty_like(spi_2_cut_Aust)
    #drought_0.fill(-1)
    #drought_1.fill(False)
    #drought_2.fill(False)

    time_length = ind_end - ind_start

    ####
    # fill in the drought array
    ####
    # loop for experiment
    for h in range(len(experiments)):
        print experiments[h]

        if h == 0:
            neg = neg_0
            dry = dry_0
            drought = drought_0
        elif h == 1:
            neg = neg_1
            dry = dry_1
            drought = drought_1
        elif h == 2:
            neg = neg_2
            dry = dry_2
            drought = drought_2

        # loop for gridpoints
        for i in range(len(mask_ind)):
            for j in range(len(mask_ind2)):

                time = 0  

                while time < time_length:  

                    ### OPTION 1: (conservative)
                    # if there are any masked values, break out of the time loop
                    # use this if you want to exclude analysis of land gridpoints with
                    # any missing values
                    ###
                    #if neg[time,i,j] is ma.masked:
                    #    break

                    ### OPTION 2: (more relaxed)
                    # just skip over any masked values
                    ###
                    while (time < time_length) and (neg[time,i,j] is ma.masked):
                        time = time + 1
                        continue

                    #if the SPI is >= 0, set the drought index to 0

                    while (time < time_length - 1) and (neg[time,i,j] == 0): 
                        drought[time,i,j] = 0
                        time = time + 1
                    if (time == time_length - 1) and (neg[time,i,j] == 0):
                        drought[time,i,j] = 0
                        time = time + 1

                    # if SPI is below 0
                    if (time < time_length) and (neg[time,i,j] == 1):
                        length = 0
                        while (time + length < time_length) and (neg[(time + length),i,j] == 1):
                            length = length + 1
                        # if every corresponding dry index is 0, set the drought index to 0
                        if np.ma.sum(dry[time:(time+length),i,j], axis=0) == 0:
                            drought[time:(time+length),i,j].fill(0)
                        # otherwise, set the drought index to 1
                        else:
                            drought[time:(time+length),i,j].fill(1)
                        time = time + length

        # out of gridpoint loop

        if h == 0:
            neg_0 = neg.astype(int)
            dry_0 = dry.astype(int)
            drought_0 = drought.astype(float)
        elif h == 1:
            neg_1 = neg
            dry_1 = dry
            drought_1 = drought.astype(float)
        elif h == 2:
            neg_2 = neg
            dry_2 = dry
            drought_2 = drought.astype(float)

    # out of experiment loop

    # Uncomment this to check that the drought array has been calculated properly;
    # this particular gridpoint contains missing values
    #for i in range(0, 24):
    #    print i * 10
    #    print neg_0[(i*10):(i+1)*10,12,10]
    #    print dry_0[(i*10):(i+1)*10,12,10]
    #    print drought_0[(i*10):(i+1)*10,12,10]

    ####
    # Calculate total drought intensity  at each gridpoint
    ####
    
    intensity_0 = np.empty([len(mask_ind),len(mask_ind2)])

    for i in range(len(mask_ind)):
        for j in range(len(mask_ind2)):
            intensity_0[i,j] = np.ma.dot(spi_0_cut_Aust[:,i,j], drought_0[:,i,j])
    intensity_0 = np.ma.absolute(intensity_0)
    intensity_1 = np.empty([len(mask_ind),len(mask_ind2)])

    for i in range(len(mask_ind)):
        for j in range(len(mask_ind2)):
            intensity_1[i,j] = np.ma.dot(spi_1_cut_Aust[:,i,j], drought_1[:,i,j])
    intensity_1 = np.ma.absolute(intensity_1)
    intensity_2 = np.empty([len(mask_ind),len(mask_ind2)])

    for i in range(len(mask_ind)):
        for j in range(len(mask_ind2)):
            intensity_2[i,j] = np.ma.dot(spi_2_cut_Aust[:,i,j], drought_2[:,i,j])
    intensity_2 = np.ma.absolute(intensity_2)

    #define differences
    intensity_A = intensity_1 - intensity_0
    intensity_B = intensity_2 - intensity_0
    intensity_AB = intensity_2 - intensity_1

    intensity_A = ma.masked_invalid(intensity_A)
    intensity_B = ma.masked_invalid(intensity_B)
    intensity_AB = ma.masked_invalid(intensity_AB)

    #Use a KS-test to test whether the differences in the distributions of SPI over 2081-2100 are 
    #statistically significant between the different experiments
    ks_A = np.ndarray((2,len(masked_lats),len(masked_lons)))
    ks_B = np.ndarray((2,len(masked_lats),len(masked_lons)))
    ks_AB = np.ndarray((2,len(masked_lats),len(masked_lons)))
    H_A = np.ndarray((len(masked_lats),len(masked_lons)))
    H_B = np.ndarray((len(masked_lats),len(masked_lons)))
    H_AB = np.ndarray((len(masked_lats),len(masked_lons)))

    for x in range(len(masked_lats)):
        for y in range(len(masked_lons)):
            ks_A[:,x,y] = stats.ks_2samp(spi_0_cut_Aust[:,x,y], spi_1_cut_Aust[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_B[:,x,y] = stats.ks_2samp(spi_0_cut_Aust[:,x,y], spi_2_cut_Aust[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0
            ks_AB[:,x,y] = stats.ks_2samp(spi_1_cut_Aust[:,x,y], spi_2_cut_Aust[:,x,y])
            if ks_AB[1,x,y] < sig:
                H_AB[x,y] = 1
            else:
                H_AB[x,y] = 0

    #Save the differences so that they can be used outside the model loop
    #(to calculate ensemble mean)
    if mod == 0:
        mod_intensity_A = np.empty([len(models), len(mask_ind),len(mask_ind2)])
        mod_intensity_B = np.empty([len(models), len(mask_ind),len(mask_ind2)])
        mod_intensity_AB = np.empty([len(models), len(mask_ind),len(mask_ind2)])

    mod_intensity_A[mod,:,:] = intensity_A
    mod_intensity_B[mod,:,:] = intensity_B
    mod_intensity_AB[mod,:,:] = intensity_AB

###
#end of model loop
###


####
# Plot the maps for the ensemble mean
###

#define the ensemble mean
ens_intensity_A = np.empty([len(mask_ind),len(mask_ind2)])
ens_intensity_B = np.empty([len(mask_ind),len(mask_ind2)])
ens_intensity_AB = np.empty([len(mask_ind),len(mask_ind2)])

ens_intensity_A = np.ma.mean(mod_intensity_A, axis = 0)
ens_intensity_B = np.ma.mean(mod_intensity_B, axis = 0)
ens_intensity_AB = np.ma.mean(mod_intensity_AB, axis = 0)


# plot difference CTL-A
diffA = experiments[1]+ '-' + experiments[0]

plotname = outdir+ 'ENSMEAN' + '/' + 'SPIint' + '_' + 'ENSMEAN' + '_' + diffA + '_' +str(A_start)+'-'+str(A_end)

title = u'\u03b4' + 'SPI intensity (months)'  +', '+ 'ENSMEAN' + ', ' + diffA +', '+str(A_start)+'-'+str(A_end)
utils.draw(ens_intensity_A, masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=H_A, masko="TRUE")

utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

# plot difference CTL-B
diffB = experiments[2]+ '-' + experiments[0]

plotname =outdir+ 'ENSMEAN' + '/' + 'SPIint' + '_' + 'ENSMEAN'+ '_' + diffB + '_' +str(A_start)+'-'+str(A_end)

title = u'\u03b4' + 'SPI intensity (months)'  +', '+ 'ENSMEAN' + ', ' + diffB +', '+str(A_start)+'-'+str(A_end)
utils.draw(ens_intensity_B, masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=H_B, masko="TRUE")

utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

# plot difference B-A
diffAB = experiments[2]+ '-' + experiments[1]

plotname =outdir+ 'ENSMEAN' + '/' + 'SPIint' + '_' + 'ENSMEAN'+ '_' + diffAB + '_' +str(A_start)+'-'+str(A_end)

title = u'\u03b4' + 'SPI intensity (months)'  +', '+ 'ENSMEAN' + ', ' + diffAB +', '+str(A_start)+'-'+str(A_end)
utils.draw(ens_intensity_AB, masked_lats, masked_lons, title = title, colors=colbar,levels=lev, sig=H_AB, masko="TRUE")

utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

####
# Save to netcdf
###

dates_out=[dt.datetime(A_start+x,06,01,00) for x in range(0,nyears)]
#time_out=nc.date2num(dates_out,units="days since 1970-01-01 00:00:00", '%Y-%m-%d_%H:%M:%S',calendar='standard')
time_out=nc.date2num(dates_out,units="days since 1970-01-01 00:00:00",calendar='standard')

fout=nc.Dataset('%s/SPI_intensity_GCCMIP5ENS_allEXP_%s-%s.nc'%(outdir,A_start,A_end),mode='w')
fout.createDimension('time',len(dates_out))
fout.createDimension('lat',lat.shape[0])
fout.createDimension('lon',lon.shape[0])

timeout=fout.createVariable('time','f8','time',fill_value=-9999)
setattr(timeout,'units','days since 1970-01-01 00:00:00')

latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
setattr(latout,'Longname','Latitude')
setattr(latout,'units','degrees_north')

lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
setattr(lonout,'Longname','Longitude')
setattr(lonout,'units','degrees_east')

SPII_expA_out=fout.createVariable('SPII_expA-CTL','f8',('time','lat','lon'),fill_value=-9999)
setattr(SPII_expA_out,'Longname','SPI intensity difference ExpB-CTL')
setattr(SPII_expA_out,'units','-')
setattr(SPII_expA_out,'description','Intensity of all drought periods (SPI<0) with at least one month with SPI<-1, added up')

SPII_expB_out=fout.createVariable('SPII_expB-CTL','f8',('time','lat','lon'),fill_value=-9999)
setattr(SPII_expB_out,'Longname','SPI intensity difference ExpB-CTL')
setattr(SPII_expB_out,'units','-')
setattr(SPII_expB_out,'description','Intensity of all drought periods (SPI<0) with at least one month with SPI<-1, added up')

SPII_expAB_out=fout.createVariable('SPII_expB-expA','f8',('time','lat','lon'),fill_value=-9999)
setattr(SPII_expAB_out,'Longname','SPI intensity difference ExpB-ExpA')
setattr(SPII_expAB_out,'units','-')
setattr(SPII_expAB_out,'description','Intensity of all drought periods (SPI<0) with at least one month with SPI<-1, added up')


timeout[:]=time_out
latout[:]=lat[:]
lonout[:]=lon[:]

SPII_expA_out[:]=ens_intensity_A[:]
SPII_expB_out[:]=ens_intensity_B[:]
SPII_expAB_out[:]=ens_intensity_AB[:]


# Set global attributes
setattr(fout,"author","Ruth Lorenz")
setattr(fout,"contact","r.lorenz@unsw.edu.au")
setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout,"comment","SPI intensity from GLACE-CMIP5 experiments, ensemble mean")
setattr(fout,"Script","SPI_intensity_CTL-EXP_GCCMIP5_ENSMEAN.py")
setattr(fout,"Input files located in:", indir)
fout.close()

