#!/usr/bin/python
'''
File Name : plot_SPI_GCCMIP5_allGCM.py
Creation Date : 08-01-2015
Last Modified : Wed 14 Jan 2015 16:13:12 AEDT
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : plot differences in SPI aspects for GLACE-CMIP5

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period

'''
# Load modules for this script
import pdb 
from scipy import stats
import netCDF4 as nc
import numpy as np
import utils
import os
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'

models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
indices = ['SPIA','SPIM','SPIN','SPIF','SPID','SPIT']

# define analysis start and end year
A_start = 1971
A_end = 2000

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # remap all models to coarsest grid
        # Load netcdf files
        path0 = indir + models[mod] +'/'+ experiments[0]+'/SPI/'+ models[mod]+'_'+experiments[0]+'_'+str(A_start)+'-'+str(A_end)+'_SPIaspects_remapcon.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/SPI/'+ models[mod]+'_'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)+'_SPIaspects_remapcon.nc'
        path2 = indir + models[mod] +'/'+ experiments[2]+'/SPI/'+ models[mod]+'_'+experiments[2]+'_'+str(A_start)+'-'+str(A_end)+'_SPIaspects_remapcon.nc'

        # read Annual value for 1st run (ctl), read lats, lons, time first to initialize variables
        print path0
        ifile = nc.Dataset(path0)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time = ifile.variables['time'][:]

        if mod == 0:
            var0 = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varB = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffB = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float) 
            diffBA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float) 

        var = ifile.variables[indices[ind]]
        miss_val=var.missing_value
        var0[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        if (indices[ind]!='SPIN') and (indices[ind]!='SPIF'):
            units = ifile.variables[indices[ind]].units
        else:
            units = ' '
        ifile.close()
        # read Annual value  for 2nd run (expA)
        ifile = nc.Dataset(path1)
        var = ifile.variables[indices[ind]]
        miss_val=var.missing_value
        varA[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        # read Annual value for 3rd run (expB)
        ifile = nc.Dataset(path2)
        var = ifile.variables[indices[ind]]
        miss_val=var.missing_value
        varB[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        ###
        #calculate difference between ctl and experiments
        diffA[mod,:,:,:] = varA[mod,:,:,:] - var0[mod,:,:,:]
        diffB[mod,:,:,:] = varB[mod,:,:,:] - var0[mod,:,:,:]
        diffBA[mod,:,:,:] = varB[mod,:,:,:] - varA[mod,:,:,:]

    #calculate ensemble mean
    var0_ens = np.ma.mean(var0, axis=0)
    varA_ens = np.ma.mean(varA, axis=0)
    varB_ens = np.ma.mean(varB, axis=0)
    diffA_ens = np.ma.mean(diffA, axis=0)
    diffB_ens = np.ma.mean(diffB, axis=0)
    diffBA_ens = np.ma.mean(diffBA, axis=0)

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_A = np.empty((2,len(lats),len(lons)))
    ks_B = np.empty((2,len(lats),len(lons)))
    ks_BA = np.empty((2,len(lats),len(lons)))
    H_A = np.empty((len(lats),len(lons)))
    H_B = np.empty((len(lats),len(lons)))
    H_BA = np.empty((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_A[:,x,y] = stats.ks_2samp(var0_ens[:,x,y], varA_ens[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_B[:,x,y] = stats.ks_2samp(var0_ens[:,x,y], varB_ens[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0
            ks_BA[:,x,y] = stats.ks_2samp(varA_ens[:,x,y], varB_ens[:,x,y])
            if ks_BA[1,x,y] < sig:
                H_BA[x,y] = 1
            else:
                H_BA[x,y] = 0

    #average data over time to plot map
    diffA_ens_tp_avg = np.ma.mean(diffA_ens, axis=0)
    diffB_ens_tp_avg = np.ma.mean(diffB_ens, axis=0)
    diffBA_ens_tp_avg = np.ma.mean(diffBA_ens, axis=0)

    #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    masked_diffA = diffA_ens_tp_avg[mask_ind,:]
    masked_HA = H_A[mask_ind,:]

    masked_diffB = diffB_ens_tp_avg[mask_ind,:]
    masked_HB = H_B[mask_ind,:]

    masked_diffBA = diffBA_ens_tp_avg[mask_ind,:]
    masked_HBA = H_BA[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if (indices[ind] == 'SPIA'):
        lev = [-0.55,-0.45,-0.35,-0.25,-0.15,-0.05,0.05,0.15,0.25,0.35,0.45,0.55]
    elif (indices[ind] == 'SPID'):
        lev = [-1.1,-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9,1.1]
    elif (indices[ind] == 'SPIF'):
        #lev = [-16.5,-13.6,-10.5,-7.5,-4.5,-1.5,1.5,4.5,7.5,10.5,13.5,16.5]
        lev = [-33,-27,-21,-15,-9,-3,3,9,15,21,27,33]
    elif (indices[ind] == 'SPIM'):
        #lev = [-0.055,-0.045,-0.035,-0.025,-0.015,-0.005,0.005,0.015,0.025,0.035,0.045,0.055]
        lev = [-0.55,-0.45,-0.35,-0.25,-0.15,-0.05,0.05,0.15,0.25,0.35,0.45,0.55]
    elif (indices[ind] == 'SPIN'):
        lev = [-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11]
    elif (indices[ind] == 'SPIT'):
        lev = [-1.1,-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9,1.1]

    colbar='RdBu_r'

    # plot difference GC1A85-CTL
    plotname = outdir + experiments[1]+'/SPI/'+indices[ind]+'_diff_'+experiments[1]+'-CTL_KSsig_ANN_'+str(A_start)+'-'+str(A_end)
    if (os.access(outdir+ experiments[1]+'/SPI/',os.F_OK)==False):
            os.mkdir(outdir+ experiments[1]+'/SPI/')
    #print plotname + plottype
    title = indices[ind]+' ['+units+'] '+experiments[1]+'-CTL '+str(A_start)+'-'+str(A_end)
    utils.draw(masked_diffA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-CTL
    plotname = outdir + experiments[2]+'/SPI/'+indices[ind]+'_diff_'+experiments[2]+'-CTL_KSsig_ANN_'+str(A_start)+'-'+str(A_end)
    if (os.access(outdir+ experiments[2]+'/SPI/',os.F_OK)==False):
            os.mkdir(outdir+ experiments[2]+'/SPI/')
    #print plotname
    title = indices[ind]+' ['+units+'] '+experiments[2]+'-CTL '+str(A_start)+'-'+str(A_end)
    utils.draw(masked_diffB,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HB)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir + experiments[2]+'/SPI/'+indices[ind]+'_diff_'+experiments[2]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)+'-'+str(A_end)
    #print plotname
    title = indices[ind]+' ['+units+'] '+experiments[2]+'-'+experiments[1]+' '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffBA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HBA)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
