#!/usr/bin/python
'''
File Name : plot_EHF_GCCMIP5_allGCM.py
Creation Date : 16-12-2014
Last Modified : Thu 16 Jul 2015 09:44:40 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : plot differences in EHF aspects for GLACE-CMIP5

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period

'''
# Load modules for this script
import pdb 
from scipy import stats
import netCDF4 as nc
import datetime as dt
import numpy as np
import utils
import os
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'

models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
#indices = ['HWA_EHF','HWM_EHF','HWN_EHF','HWF_EHF','HWD_EHF','HWT_EHF']
indices = ['HWD']
# define analysis start and end year
A_start = 2056
A_end = 2085

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # remap all models to coarsest grid
        # Load netcdf files
        path0 = indir + models[mod] +'/'+ experiments[0]+'/EHF/'+ models[mod]+'_'+experiments[0]+'_'+str(A_start)+'-'+str(A_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/EHF/'+ models[mod]+'_'+experiments[1]+'_'+str(A_start)+'-'+str(A_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path2 = indir + models[mod] +'/'+ experiments[2]+'/EHF/'+ models[mod]+'_'+experiments[2]+'_'+str(A_start)+'-'+str(A_end)+'_EHFheatwaves_yearly_remapcon.nc'

        # read Annual value for 1st run (ctl), read lats, lons, time first to initialize variables
        print path0
        ifile = nc.Dataset(path0)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time = ifile.variables['time'][:]

        if mod == 0:
            var0 = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varB = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffB = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float) 
            diffBA = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float) 

        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        #print var.shape, type(var), var.missing_value

        var0[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        #print var0.shape, type(var0)

        if (indices[ind]!='HWN') and (indices[ind]!='HWF'):
            units = ifile.variables[indices[ind]+'_EHF'].units
        else:
            units = ' '

        ifile.close()

        # read Annual value  for 2nd run (expA)
        ifile = nc.Dataset(path1)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varA[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        # read Annual value for 3rd run (expB)
        ifile = nc.Dataset(path2)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varB[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        ###
        #calculate difference between ctl and experiments
        diffA[mod,:,:,:] = varA[mod,:,:,:] - var0[mod,:,:,:]
        diffB[mod,:,:,:] = varB[mod,:,:,:] - var0[mod,:,:,:]
        diffBA[mod,:,:,:] = varB[mod,:,:,:] - varA[mod,:,:,:]

    #calculate ensemble mean
    var0_ens = np.ma.mean(var0, axis=0)
    varA_ens = np.ma.mean(varA, axis=0)
    varB_ens = np.ma.mean(varB, axis=0)
    diffA_ens = np.ma.mean(diffA, axis=0)
    diffB_ens = np.ma.mean(diffB, axis=0)
    diffBA_ens = np.ma.mean(diffBA, axis=0)

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_A = np.ma.empty((2,len(lats),len(lons)))
    ks_B = np.ma.empty((2,len(lats),len(lons)))
    ks_BA = np.ma.empty((2,len(lats),len(lons)))
    H_A = np.ma.empty((len(lats),len(lons)))
    H_B = np.ma.empty((len(lats),len(lons)))
    H_BA = np.ma.empty((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_A[:,x,y] = stats.ks_2samp(var0_ens[:,x,y], varA_ens[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_B[:,x,y] = stats.ks_2samp(var0_ens[:,x,y], varB_ens[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0
            ks_BA[:,x,y] = stats.ks_2samp(varA_ens[:,x,y], varB_ens[:,x,y])
            if ks_BA[1,x,y] < sig:
                H_BA[x,y] = 1
            else:
                H_BA[x,y] = 0

    #average data over time to plot map
    var0_ens_tp_avg = np.ma.mean(var0_ens, axis=0)
    diffA_ens_tp_avg = np.ma.mean(diffA_ens, axis=0)
    diffB_ens_tp_avg = np.ma.mean(diffB_ens, axis=0)
    diffBA_ens_tp_avg = np.ma.mean(diffBA_ens, axis=0)

    #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    masked_var0 = var0_ens_tp_avg[mask_ind,:]
    
    masked_diffA = np.ma.copy(diffA_ens_tp_avg)
    masked_diffA = masked_diffA[mask_ind,:]

    masked_HA = np.ma.copy(H_A)
    masked_HA = masked_HA[mask_ind,:]

    masked_diffB = np.ma.copy(diffB_ens_tp_avg)
    masked_diffB = masked_diffB[mask_ind,:]
    masked_HB = np.ma.copy(H_B)
    masked_HB = masked_HB[mask_ind,:]

    masked_diffBA = np.ma.copy(diffBA_ens_tp_avg)
    masked_diffBA = masked_diffBA[mask_ind,:]
    masked_HBA = np.ma.copy(H_BA)
    masked_HBA = masked_HBA[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if (indices[ind] == 'HWA'):
        lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]
    elif (indices[ind] == 'HWD'):
        lev = [-20,-16,-12,-8,-2,2,8,12,16,20]
        #lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]
    elif (indices[ind] == 'HWF'):
        lev = [-13.6,-10.5,-7.5,-4.5,-1.5,1.5,4.5,7.5,10.5,13.5]
    elif (indices[ind] == 'HWM'):
        lev = [-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9]
    elif (indices[ind] == 'HWN'):
        lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
    elif (indices[ind] == 'HWT'):
        lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]

    colbar='RdBu_r'

    # plot CTL
    plotname = outdir + experiments[0]+'/EHF/'+indices[ind]+'_CTL_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units+'] CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_var0,masked_lats, lons, title = title)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1A85-CTL
    plotname = outdir + experiments[1]+'/EHF/'+indices[ind]+'_diff_'+experiments[1]+'-CTL_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (os.access(outdir+ experiments[1]+'/EHF/',os.F_OK)==False):
            os.mkdir(outdir+ experiments[1]+'/EHF/')
    #print plotname + plottype
    title = indices[ind]+' ['+units+'] SMclim-CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (indices[ind] == 'HWD') and (A_start==1971):
        utils.draw(masked_diffA,masked_lats, lons, title = title, subpanel="e)", colors=colbar,levels=lev, sig=masked_HA)
    else:
    	utils.draw(masked_diffA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-CTL
    plotname = outdir + experiments[2]+'/EHF/'+indices[ind]+'_diff_'+experiments[2]+'-CTL_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (os.access(outdir+ experiments[2]+'/EHF/',os.F_OK)==False):
            os.mkdir(outdir+ experiments[2]+'/EHF/')
    #print plotname
    title = indices[ind]+' ['+units+'] SMtrend-CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffB,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HB)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir + experiments[2]+'/EHF/'+indices[ind]+'_diff_'+experiments[2]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname
    title = indices[ind]+' ['+units+'] SMtrend-SMclim '+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (indices[ind] == 'HWD') and (A_start==2056):
        utils.draw(masked_diffBA,masked_lats, lons, title = title, subpanel="f)", colors=colbar,levels=lev, sig=masked_HA)
    else:
    	utils.draw(masked_diffBA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HBA)
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    #also save ensemble differences in netcdf for later panelling
    fout=nc.Dataset('%s/%s_diffs_ENSMEAN_ANN_%s-%s.nc'%(outdir,indices[ind],str(A_start)[:4],str(A_end)[:4]),mode='w')
    fout.createDimension('lat',masked_lats.shape[0])
    fout.createDimension('lon',lons.shape[0])

    latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
    setattr(latout,'Longname','Latitude')
    setattr(latout,'units','degrees_north')

    lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
    setattr(lonout,'Longname','Longitude')
    setattr(lonout,'units','degrees_east')

    diffAout=fout.createVariable('ExpA-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(diffAout,'Longname','Difference between ExpA and CTL')
    setattr(diffAout,'units',units[ind])
    setattr(diffAout,'description','ExpA-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigAout=fout.createVariable('sigExpA-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(sigAout,'Longname','Significance in difference between ExpA and CTL')
    setattr(sigAout,'units','-')
    setattr(sigAout,'description','Statistical significance ExpA-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBout=fout.createVariable('ExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(diffBout,'Longname','Difference between ExpB and CTL')
    setattr(diffBout,'units',units[ind])
    setattr(diffBout,'description','ExpB-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigBout=fout.createVariable('sigExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(sigBout,'Longname','Significance in difference between ExpB and CTL')
    setattr(sigBout,'units','-')
    setattr(sigBout,'description','Statistical significance ExpB-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBAout=fout.createVariable('ExpB-ExpA','f8',('lat','lon'),fill_value=-9999)
    setattr(diffBAout,'Longname','Difference between ExpB and ExpA')
    setattr(diffBAout,'units',units[ind])
    setattr(diffBAout,'description','ExpB-ExpA '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])
    
    sigBAout=fout.createVariable('sigExpB-ExpA','f8',('lat','lon'),fill_value=-9999)
    setattr(sigBAout,'Longname','Significance in difference between ExpB and ExpA')
    setattr(sigBAout,'units','-')
    setattr(sigBAout,'description','Statistical significance ExpB-ExpA '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    latout[:]=masked_lats[:]
    lonout[:]=lons[:]
    diffAout[:]=masked_diffA[:]
    diffBout[:]=masked_diffB[:]
    diffBAout[:]=masked_diffBA[:]
    sigAout[:]=masked_HA[:]
    sigBout[:]=masked_HB[:]
    sigBAout[:]=masked_HBA[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
    setattr(fout,"contact","r.lorenz@unsw.edu.au")
    setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout,"Script","plot_EHF_GCCMIP5_allGCM.py")
    setattr(fout,"Input files located in:", indir)
    fout.close()
