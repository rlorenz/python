#!/usr/bin/python
'''
File Name : calc_coupling_strength_GCCMIP5.py
Creation Date : 28-07-2015
Last Modified : Tue 28 Jul 2015 11:51:56 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : calculate Omega coupling strength for GLACE-CMIP5 models


'''

import numpy as np
import datetime as dt
import netCDF4 as nc
from scipy import signal
import os
from netcdftime import utime

###
# Define input
###
#models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
models = ['EC-EARTH','ECHAM6','IPSL']
experiments = ['CTL','GC1B85']
varname = ['tas','tasmax','tasmin','pr']

# start and end of runs (adjust syfile for models/experiments
# where different within loop)
syfile=1950
eyfile=2100
# define analysis start and end year
syear=2071
eyear=2100

#Loop over variables
for v in range(len(varname)):
    # Loop over models
    for mod in range(len(models)):

        indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/'
        outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/'+experiments[1]+'/coupling/'
        if (os.access(outdir,os.F_OK)==False):
            os.mkdir(outdir)

        # Load netcdf files   
        if (models[mod]=="CESM") and (experiments[exp]=="CTL"):
            syfile = 1955
            filectl=nc.Dataset(indir+ experiments[0]+ '/'+varname[v]+'_daily_CCSM4_'+experiments[0]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            fileexp=nc.Dataset(indir+ experiments[1]+ '/'+varname[v]+'_daily_CCSM4_'+experiments[1]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
        elif (models[mod]=="GFDL"):
            syfile = 1951
            filectl=nc.Dataset(indir+ experiments[0]+ '/'+varname[v]+'_daily_'+ models[mod]+'_'+experiments[0]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            fileexp=nc.Dataset(indir+ experiments[1]+ '/'+varname[v]+'_daily_'+ models[mod]+'_'+experiments[1]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
        else:
            syfile = 1950
            filectl=nc.Dataset(indir+ experiments[0]+ '/'+varname[v]+'_daily_'+ models[mod]+'_'+experiments[0]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            fileexp=nc.Dataset(indir+ experiments[1]+ '/'+varname[v]+'_daily_'+ models[mod]+'_'+experiments[1]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')

        time = filectl.variables['time']
        cdftime = utime(time.units)
        dates_ctl=cdftime.num2date(time)
        years_ctl=np.asarray([dates_ctl[i].year for i in xrange(len(dates_ctl))])
        var_ctl = filectl.variables[varname[v]][(years_ctl>=syear) & (years_ctl<=eyear)]
        miss_val_ctl=filectl.variables[varname[v]]._FillValue
        new_time = filectl.variables['time'][(years_ctl>=syear) & (years_ctl<=eyear)]
        dates_ctl=cdftime.num2date(new_time)
        filectl.close()

        time = fileexp.variables['time']
        cdftime = utime(time.units)
        dates_exp=cdftime.num2date(time)
        years_exp=np.asarray([dates_exp[i].year for i in xrange(len(dates_exp))])
        var_exp = fileexp.variables[varname[v]][(years_exp>=syear) & (years_exp<=eyear)]
        miss_val_exp=fileexp.variables[varname[v]]._FillValue
        new_time = fileexp.variables['time'][(years_exp>=syear) & (years_exp<=eyear)]
        dates_exp=cdftime.num2date(new_time)

        #check dimensions
        if (var_exp.shape!=var_ctl.shape):
            print var_exp_cut.shape, var_ctl_cut.shape
            sys.exit('CTL and EXP do not have the same dimensions, exiting')
        if (var_exp.shape[0]!=len(dates_exp)):
            print var_exp.shape[0],len(dates_exp)
            sys.exit('EXP has wrong time dimension, exiting')
        if (var_ctl.shape[0]!=len(dates_ctl)):
            print var_ctl.shape[0],len(dates_ctl)
            sys.exit('EXP has wrong time dimension, exiting')
        if (miss_val_ctl!=miss_val_exp):
            print('Warning: CTl and EXP have different _FillValue')

        lat=fileexp.variables['lat'][:]
        lon=fileexp.variables['lon'][:]
        fileexp.close()
        
        #get time info
        nyears=eyear-syear+1
        #years=np.asarray([dates_ctl[i].year for i in xrange(len(dates_ctl))])
        month=np.asarray([dates_ctl[i].month for i in xrange(len(dates_ctl))])
        day=np.asarray([dates_ctl[i].day for i in xrange(len(dates_ctl))])
        #detrend timeseries
        var_exp_dtr = signal.detrend(var_exp, axis=0)
        var_ctl_dtr = signal.detrend(var_ctl, axis=0)

        #loop over seasons
        for j in range(0,3):
            if (j==0):
                seas = 'DJF'
                ind_seas_ctl = np.where((month==1) | ((month==2) & (day!=29)) | ((month==12) & (day>=6)))
                ind_seas_exp = np.where((month==1) | ((month==2) & (day!=29)) | ((month==12) & (day>=6)))
            elif (j==1):
                seas = 'MAM'
                ind_seas_ctl = np.where(((month==3) & (day>=8)) | (month==4) | (month==5))
                ind_seas_exp = np.where(((month==3) & (day>=8)) | (month==4) | (month==5))
            elif (j==2):
                seas = 'JJA'
                ind_seas_ctl = np.where(((month==6) & (day>=8)) | (month==7) | (month==8))
                ind_seas_exp = np.where(((month==6) & (day>=8)) | (month==7) | (month==8))
            else:
                seas = 'SON'
                ind_seas_ctl = np.where(((month==9) & (day>=7)) | (month==10) | (month==11))
                ind_seas_exp = np.where(((month==9) & (day>=7)) | (month==10) | (month==11))

            var_ctl_seas = var_ctl_dtr[ind_seas_ctl,:,:]
            var_exp_seas = var_exp_dtr[ind_seas_exp,:,:]

            ntim_seas = var_ctl_seas.shape[0]
            ntim_6d=ntim_seas/6
            #calculate 6-daily means
            temp_ctl_6dmean=np.ma.empty([ntim_6d,len(lat),len(lon)],dtype=float)
            temp_exp_6dmean=np.ma.empty([ntim_6d,len(lat),len(lon)],dtype=float)
            ind_t=0
            for tt in range(0,ntim_6d,6):
                temp_ctl_6dmean[ind_t,:,:]=np.ma.mean(temp_ctl_seas[tt:tt+5,:,:],axis=0)
                temp_exp_6dmean[ind_t,:,:]=np.ma.mean(temp_exp_seas[tt:tt+5,:,:],axis=0)
                ind_t=ind_t+1

            # Calculate mean climatology
            ndays=ntim_6d/nyears
            temp_ctl_yr = np.reshape(temp_ctl_6dmean,(nyears,ndays,len(lat),len(lon)))
            temp_exp_yr = np.reshape(temp_exp_6dmean,(nyears,ndays,len(lat),len(lon)))
            temp_ctl_clim = np.ma.mean(temp_ctl_yr,axis=0)
            temp_exp_clim = np.ma.mean(temp_exp_yr,axis=0)

            # Calculate variances
            var_ctl = np.ma.var(temp_ctl_clim,axis=0,dtype=np.float64)
            var_exp = np.ma.var(temp_exp_clim,axis=0,dtype=np.float64)
            var_ctl_full = np.ma.var(temp_ctl_6dmean,axis=0,dtype=np.float64)
            var_exp_full = np.ma.var(temp_exp_6dmean,axis=0,dtype=np.float64)

            if (j==0 ):
                omega_ctl=np.ndarray([4,len(lat),len(lon)])
                omega_exp=np.ndarray([4,len(lat),len(lon)])
                coup=np.ndarray([4,len(lat),len(lon)])

            omega_ctl[j,:,:]=((nyears*var_ctl)-var_ctl_full)/(nyears-1)
            omega_exp[j,:,:]=((nyears*var_exp)-var_exp_full)/(nyears-1)
            coup[j,:,:]=omega_exp[j,:,:]-omega_ctl[j,:,:]

        # save data to netcdf
        fout=nc.Dataset('%s/GCCMIP5_omega_coupstr_%s_%s_%s_%s-%s.nc'%(outdir,experiments[1],experiments[0],varname[v],str(syfile),str(eyfile)),mode='w')
        fout.createDimension('lat',lat.shape[0])
        fout.createDimension('lon',lon.shape[0])
        fout.createDimension('time',4)

        latout=fout.createVariable('lat','f8',('lat'),fill_value=miss_val_ctl)
        setattr(latout,'Longname','Latitude')
        setattr(latout,'units','degrees_north')

        lonout=fout.createVariable('lon','f8',('lon'),fill_value=miss_val_ctl)
        setattr(lonout,'Longname','Longitude')
        setattr(lonout,'units','degrees_east')

        out_ctl=fout.createVariable('omega_'+experiments[0],'f8',('time','lat','lon'),fill_value=miss_val_ctl)
        setattr(out_ctl,'Longname','Omega '+experiments[0])
        setattr(out_ctl,'units','-')
        setattr(out_ctl,'description','Omega for '+experiments[0]+' during '+str(syfile)+' to '+str(eyfile))

        out_exp=fout.createVariable('omega_'+experiments[1],'f8',('time','lat','lon'),fill_value=miss_val_ctl)
        setattr(out_exp,'Longname','Omega '+experiments[1])
        setattr(out_exp,'units','-')
        setattr(out_exp,'description','Omega for '+experiments[1]+' during '+str(syfile)+' to '+str(eyfile))

        out_coup=fout.createVariable('coup','f8',('time','lat','lon'),fill_value=miss_val_ctl)
        setattr(out_coup,'Longname','Coupling strength')
        setattr(out_coup,'units','-')
        setattr(out_coup,'description','Coupling strength for '+experiments[1]+' and ' +experiments[0]+' during '+str(syfile)+' to '+str(eyfile))

        latout[:]=lat[:]
        lonout[:]=lon[:]
        out_ctl[:]=omega_ctl[:]
        out_exp[:]=omega_exp[:]
        out_coup[:]=coup[:]

        # Set global attributes
        setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
        setattr(fout,"contact","r.lorenz@unsw.edu.au")
        setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
        setattr(fout,"Script","calc_coupling_strength_GCCMIP5.py")
        setattr(fout,"Input files located in:", indir)
        fout.close()
        
