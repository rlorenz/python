#!/usr/bin/python

'''
File Name : plot_climdex_CTL-EXP_GCCMIP5_ENSMEAN.py
Creation Date: 24 Oct 2014
Last Modified: 17/11/2014
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from GLACE-CMIP5 project
Data hosted at ETH IAC
Downloaded to storm servers: /srv/ccrc/data32/z3441306/GLACE-CMIP5/
by Ruth Lorenz

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data32/z3441306/GLACE-CMIP5/$model/$experiment/climdex_index/*_remapcon.nc
these files are already remapped to common grid for all models using "cdo remapcon"
Coarsest grid of all GCMs is used (96x96 gridpoints from IPSL)

call within outdir because of pdfcrop!
'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
import netCDF4 as nc
import datetime as dt
from scipy import stats
import numpy as np
import utils
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
indices = ['TX90p','TN90p','TNx','TXx','WSDI','R95p','R10mm','CDD','CWD']
units=['%','%','$^\circ$C','$^\circ$C','days','mm','days','days','days']
normalize="FALSE"

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1954
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1950
R1_end = 2100
# define analysis start and end year
A_start = 19710100
A_end = 20000100

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # Load netcdf files
        path0 = indir + models[mod] +'/'+ experiments[0]+'/climdex_index/'+ models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'-'+str(R0_end)+'_'+indices[ind]+'_remapcon.nc'
        path1 = indir + models[mod] +'/'+ experiments[1]+'/climdex_index/'+ models[mod]+'_'+experiments[1]+'_1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'_remapcon.nc'
        path2 = indir + models[mod] +'/'+ experiments[2]+'/climdex_index/'+ models[mod]+'_'+experiments[2]+'_1_'+str(R1_start)+'-'+str(R1_end)+'_'+indices[ind]+'_remapcon.nc'
        # read Annual value of ETCCDI Index for 1st run (ctl), read lats and lons as well
        ifile = netcdf_file(path0)
        var = ifile.variables['Annual']
        miss_val=var.missing_value
        var0 = np.ma.masked_values(var[:],miss_val)
        lats = ifile.variables['lat'].data
        lons = ifile.variables['lon'].data
        time = ifile.variables['time'].data
        # print information about variable
        #print type(var0)
        #print var0.shape
        #print ifile.variables['Annual'].dimensions
        # read Annual value of ETCCDI Index for 2nd run (expA)
        ifile = netcdf_file(path1)
        var1 = ifile.variables['Annual']
        miss_val1=var1.missing_value
        varA = np.ma.masked_values(var1[:],miss_val1)
        # read Annual value of ETCCDI Index for 3rd run (expB)
        ifile = netcdf_file(path2)
        var2 = ifile.variables['Annual']
        miss_val2=var2.missing_value
        varB = np.ma.masked_values(var2[:],miss_val2)
        ###
        # normalize indices which are not normalized yet
        # normalize by removing mean and standard deviation of CTL, first cut expA and expB into same length
        t_diff=R0_start-R1_start
        varA_cut = varA[t_diff:,:,:]
        varB_cut = varB[t_diff:,:,:]
        ntim = varB_cut.shape[0]
        #normalize ctl, expA and expB
        if mod == 0:
            var0_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            varA_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            varB_norm = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            diffA = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float)
            diffB = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float) 
            diffBA = np.ma.empty([len(models),ntim,len(lats),len(lons)],dtype=float) 
        #if (indices[ind] == 'TXx') or (indices[ind] =='TNx') or (indices[ind] =='R10mm') or (indices[ind] =='CDW') or (indices[ind] =='CDD'):
        if (normalize == "TRUE"):
        #Take the mean and std at each grid point
            print ("Normalize the data")
            mean0 = np.ma.mean(var0, axis=0)
            std0 = np.ma.std(var0, axis=0)
            # normalize by mean and std from CTL
            var0_norm[mod,:,:,:] = (var0 - mean0) / std0
            varA_norm[mod,:,:,:] = (varA_cut - mean0) / std0
            varB_norm[mod,:,:,:] = (varB_cut - mean0) / std0
        else:
            var0_norm[mod,:,:,:] = np.ma.copy(var0)
            varA_norm[mod,:,:,:] = np.ma.copy(varA_cut)
            varB_norm[mod,:,:,:] = np.ma.copy(varB_cut)
        #calculate normalized difference between ctl and experiments
        diffA[mod,:,:,:] = varA_norm[mod,:,:,:] - var0_norm[mod,:,:,:]
        diffB[mod,:,:,:] = varB_norm[mod,:,:,:] - var0_norm[mod,:,:,:]
        diffBA[mod,:,:,:] = varB_norm[mod,:,:,:] - varA_norm[mod,:,:,:]

    #calculate ensemble mean
    var0_ens = np.ma.mean(var0_norm, axis=0)
    varA_ens = np.ma.mean(varA_norm, axis=0)
    varB_ens = np.ma.mean(varB_norm, axis=0)
    diffA_ens = np.ma.mean(diffA, axis=0)
    diffB_ens = np.ma.mean(diffB, axis=0)
    diffBA_ens = np.ma.mean(diffBA, axis=0)

    # cut ensemble means into analysis period
    ind1 = np.where(time==A_start)[0][0]
    ind2 = np.where(time==A_end)[0][0]+1
    var0_ens_tp = var0_ens[ind1:ind2,:,:]
    varA_ens_tp = varA_ens[ind1:ind2,:,:]
    varB_ens_tp = varB_ens[ind1:ind2,:,:]
    diffA_ens_tp = diffA_ens[ind1:ind2,:,:]
    diffB_ens_tp = diffB_ens[ind1:ind2,:,:]
    diffBA_ens_tp = diffBA_ens[ind1:ind2,:,:]

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_A = np.ndarray((2,len(lats),len(lons)))
    ks_B = np.ndarray((2,len(lats),len(lons)))
    ks_BA = np.ndarray((2,len(lats),len(lons)))
    H_A = np.ndarray((len(lats),len(lons)))
    H_B = np.ndarray((len(lats),len(lons)))
    H_BA = np.ndarray((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_A[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varA_ens_tp[:,x,y])
            if ks_A[1,x,y] < sig:
                H_A[x,y] = 1
            else:
                H_A[x,y] = 0
            ks_B[:,x,y] = stats.ks_2samp(var0_ens_tp[:,x,y], varB_ens_tp[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0
            ks_BA[:,x,y] = stats.ks_2samp(varA_ens_tp[:,x,y], varB_ens_tp[:,x,y])
            if ks_BA[1,x,y] < sig:
                H_BA[x,y] = 1
            else:
                H_BA[x,y] = 0

    #average data over time to plot map
    var0_ens_tp_avg = np.ma.mean(var0_ens_tp, axis=0)
    diffA_ens_tp_avg = np.ma.mean(diffA_ens_tp, axis=0)
    diffB_ens_tp_avg = np.ma.mean(diffB_ens_tp, axis=0)
    diffBA_ens_tp_avg = np.ma.mean(diffBA_ens_tp, axis=0)

    #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    masked_var0 = var0_ens_tp_avg[mask_ind,:]

    masked_diffA = diffA_ens_tp_avg[mask_ind,:]
    masked_HA = H_A[mask_ind,:]

    masked_diffB = diffB_ens_tp_avg[mask_ind,:]
    masked_HB = H_B[mask_ind,:]

    masked_diffBA = diffBA_ens_tp_avg[mask_ind,:]
    masked_HBA = H_BA[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if A_end < 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif indices[ind] == 'R95p':
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif indices[ind] == 'WSDI':
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif (indices[ind] == 'CWD'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        else:
            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
    elif A_end > 20500100:
        if (indices[ind] == 'TXx') or (indices[ind] == 'TNx'):
            lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
        elif (indices[ind] == 'TX90p') or (indices[ind] == 'TN90p'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        elif indices[ind] == 'R95p':
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif indices[ind] == 'WSDI':
            lev = [-45,-35,-25,-15,-5,5,15,25,35,45]
        elif (indices[ind] == 'CWD'):
            lev = [-9,-7,-5,-3,-1,1,3,5,7,9]
        else:
            lev = [-12,-9,-6,-3,-1,1,3,6,9,12]
#[-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9]

    # define colorbar depending on index
    if  (indices[ind] == 'R95p') or (indices[ind] == 'R10mm') or (indices[ind] == 'CWD'):      
        colbar='BrBG'
    elif (indices[ind] == 'WSDI'):
        colbar='RdBu_r'
    elif (indices[ind] == 'CDD'):
        colbar='BrBG_r'
    else:
        colbar='RdBu_r'

    # plot CTL
    plotname = outdir + experiments[0]+'/climdex_figures/'+indices[ind]+'_CTL_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units[ind]+'] CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_var0,masked_lats, lons, title = title)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1A85-CTL
    plotname = outdir + experiments[1]+'/climdex_figures/'+indices[ind]+'_diff_'+experiments[1]+'-CTL_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname + plottype
    title = indices[ind]+' ['+units[ind]+'] SMclim-CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (indices[ind] == 'TXx') and (A_start==19710100):
        utils.draw_reg(masked_diffA,masked_lats, lons, title = title, subpanel = "a)", colors=colbar,levels=lev, sig=masked_HA, name=['CNA','SAF','MED','NAU'],lonmin=[-110,-10,-10.0,110],lonmax=[-85,51.99,40,155],latmin=[25,-35,30,-30],latmax=[50,-11.365,45,-10])
    elif (indices[ind] == 'TX90p') and (A_start==19710100):
	utils.draw(masked_diffA,masked_lats, lons, title = title, subpanel="c)", colors=colbar,levels=lev, sig=masked_HA)
    else :
        utils.draw(masked_diffA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HA)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-CTL
    plotname = outdir + experiments[2]+'/climdex_figures/'+indices[ind]+'_diff_'+experiments[2]+'-CTL_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname
    title = indices[ind]+' ['+units[ind]+'] SMtrend-CTL '+str(A_start)[:4]+'-'+str(A_end)[:4]
    utils.draw(masked_diffB,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HB)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    # plot difference GC1B85-GC1A85
    plotname = outdir + experiments[2]+'/climdex_figures/'+indices[ind]+'_diff_'+experiments[2]+'-'+experiments[1]+'_KSsig_ANN_'+str(A_start)[:4]+'-'+str(A_end)[:4]
    #print plotname
    title = indices[ind]+' ['+units[ind]+'] SMtrend-SMclim '+str(A_start)[:4]+'-'+str(A_end)[:4]
    if (indices[ind] == 'TXx') and (A_start==20560100):
        utils.draw(masked_diffBA,masked_lats, lons, title = title, subpanel="b)", colors=colbar,levels=lev, sig=masked_HA)
    elif (indices[ind] == 'TX90p') and (A_start==20560100):
        utils.draw(masked_diffBA,masked_lats, lons, title = title, subpanel="d)", colors=colbar,levels=lev, sig=masked_HA)
    else :
    	utils.draw(masked_diffBA,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HBA)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    #also save ensemble differences in netcdf for later panelling
    fout=nc.Dataset('%s/%s_diffs_ENSMEAN_ANN_%s-%s.nc'%(outdir,indices[ind],str(A_start)[:4],str(A_end)[:4]),mode='w')
    fout.createDimension('lat',masked_lats.shape[0])
    fout.createDimension('lon',lons.shape[0])

    latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
    setattr(latout,'Longname','Latitude')
    setattr(latout,'units','degrees_north')

    lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
    setattr(lonout,'Longname','Longitude')
    setattr(lonout,'units','degrees_east')

    diffAout=fout.createVariable('ExpA-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(diffAout,'Longname','Difference between ExpA and CTL')
    setattr(diffAout,'units',units[ind])
    setattr(diffAout,'description','ExpA-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigAout=fout.createVariable('sigExpA-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(sigAout,'Longname','Significance in difference between ExpA and CTL')
    setattr(sigAout,'units','-')
    setattr(sigAout,'description','Statistical significance ExpA-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBout=fout.createVariable('ExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(diffAout,'Longname','Difference between ExpB and CTL')
    setattr(diffAout,'units',units[ind])
    setattr(diffAout,'description','ExpB-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    sigBout=fout.createVariable('sigExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(sigBout,'Longname','Significance in difference between ExpB and CTL')
    setattr(sigBout,'units','-')
    setattr(sigBout,'description','Statistical significance ExpB-CTL '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    diffBAout=fout.createVariable('ExpB-ExpA','f8',('lat','lon'),fill_value=-9999)
    setattr(diffAout,'Longname','Difference between ExpB and ExpA')
    setattr(diffAout,'units',units[ind])
    setattr(diffAout,'description','ExpB-ExpA '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])
    
    sigBAout=fout.createVariable('sigExpB-ExpA','f8',('lat','lon'),fill_value=-9999)
    setattr(sigBAout,'Longname','Significance in difference between ExpB and ExpA')
    setattr(sigBAout,'units','-')
    setattr(sigBAout,'description','Statistical significance ExpB-ExpA '+indices[ind]+' from '+str(A_start)[:4]+' to ' +str(A_end)[:4])

    latout[:]=masked_lats[:]
    lonout[:]=lons[:]
    diffAout[:]=masked_diffA[:]
    diffBout[:]=masked_diffB[:]
    diffBAout[:]=masked_diffBA[:]
    sigAout[:]=masked_HA[:]
    sigBout[:]=masked_HB[:]
    sigBAout[:]=masked_HBA[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
    setattr(fout,"contact","r.lorenz@unsw.edu.au")
    setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout,"Script","plot_climdex_CTL-EXP_GCCMIP5_ENSMEAN.py")
    setattr(fout,"Input files located in:", indir)
    fout.close()
