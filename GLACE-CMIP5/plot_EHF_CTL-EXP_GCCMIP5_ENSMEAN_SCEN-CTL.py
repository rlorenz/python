#!/usr/bin/python

'''
File Name : plot_EHF_CTL-EXP_GCCMIP5_ENSMEAN_SCEN-CTL.py
Creation Date: 21 Apr 2015
Last Modified: 21/04/2015
Author: Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose:
Script that reads and analyses netcdfs from GLACE-CMIP5 project
Data hosted at ETH IAC
Downloaded to storm servers: /srv/ccrc/data32/z3441306/GLACE-CMIP5/
by Ruth Lorenz

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL
all have different resolution and biases -> need to regrid to common grid and
normalize results

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares CTL and experiments in one time period

call within outdir because of pdfcrop!
'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
from scipy import stats
import netCDF4 as nc
import datetime as dt
import numpy as np
import utils
from subprocess import call
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'
#models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
models = ['CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
experiments = ['CTL','GC1A85','GC1B85']
#indices = ['HWA','HWM','HWN','HWF','HWD','HWT']
indices = ['HWD']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1954
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = 1950
R1_end = 2100
# define analysis start and end year
A1_start = 1971
A1_end = 2000

A2_start = 2056
A2_end = 2085

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over indices
for ind in range(len(indices)):
    print indices[ind]
    # Loop over models to read all data
    for mod in range(len(models)):
        ###
        # Load netcdf files
        path0_ctl = indir + models[mod] +'/'+ experiments[0]+'/EHF/'+ models[mod]+'_'+experiments[0]+'_'+str(A1_start)+'-'+str(A1_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path1_ctl = indir + models[mod] +'/'+ experiments[1]+'/EHF/'+ models[mod]+'_'+experiments[1]+'_'+str(A1_start)+'-'+str(A1_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path2_ctl = indir + models[mod] +'/'+ experiments[2]+'/EHF/'+ models[mod]+'_'+experiments[2]+'_'+str(A1_start)+'-'+str(A1_end)+'_EHFheatwaves_yearly_remapcon.nc'

        path0_scen = indir + models[mod] +'/'+ experiments[0]+'/EHF/'+ models[mod]+'_'+experiments[0]+'_'+str(A2_start)+'-'+str(A2_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path1_scen = indir + models[mod] +'/'+ experiments[1]+'/EHF/'+ models[mod]+'_'+experiments[1]+'_'+str(A2_start)+'-'+str(A2_end)+'_EHFheatwaves_yearly_remapcon.nc'
        path2_scen = indir + models[mod] +'/'+ experiments[2]+'/EHF/'+ models[mod]+'_'+experiments[2]+'_'+str(A2_start)+'-'+str(A2_end)+'_EHFheatwaves_yearly_remapcon.nc'

        # read Annual value for 1st run (ctl), read lats, lons, time first to initialize variables
        print path0_ctl
        ifile = nc.Dataset(path0_ctl)
        lats = ifile.variables['lat'][:]
        lons = ifile.variables['lon'][:]
        time = ifile.variables['time'][:]

        if mod == 0:
            var0_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varA_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varB_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffA_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffB_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffBA_ctl = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            var0_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varA_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            varB_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffA_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffB_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)
            diffBA_scen = np.ma.empty([len(models),len(time),len(lats),len(lons)],dtype=float)


        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        #print var.shape, type(var), var.missing_value

        var0_ctl[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        #print var0.shape, type(var0)

        if (indices[ind]!='HWN') and (indices[ind]!='HWF'):
            units = ifile.variables[indices[ind]+'_EHF'].units
        else:
            units = ' '

        ifile.close()

        ifile = nc.Dataset(path0_scen)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        var0_scen[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        # read Annual value  for 2nd run (expA)
        ifile = nc.Dataset(path1_ctl)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varA_ctl[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        ifile = nc.Dataset(path1_scen)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varA_scen[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
 
        # read Annual value for 3rd run (expB)
        ifile = nc.Dataset(path2_ctl)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varB_ctl[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()
        ifile = nc.Dataset(path2_scen)
        var = ifile.variables[indices[ind]+'_EHF']
        miss_val=var.missing_value
        varB_scen[mod,:,:,:] = np.ma.masked_values(var[:],miss_val)
        ifile.close()

        #calculate difference between ctl and experiments
        diffA_ctl[mod,:,:,:] = varA_ctl[mod,:,:,:] - var0_ctl[mod,:,:,:]
        diffB_ctl[mod,:,:,:] = varB_ctl[mod,:,:,:] - var0_ctl[mod,:,:,:]
        diffBA_ctl[mod,:,:,:] = varB_ctl[mod,:,:,:] - varA_ctl[mod,:,:,:]
        diffA_scen[mod,:,:,:] = varA_scen[mod,:,:,:] - var0_scen[mod,:,:,:]
        diffB_scen[mod,:,:,:] = varB_scen[mod,:,:,:] - var0_scen[mod,:,:,:]
        diffBA_scen[mod,:,:,:] = varB_scen[mod,:,:,:] - varA_scen[mod,:,:,:]

    #calculate ensemble mean
    var0_ctl_ens = np.ma.mean(var0_ctl, axis=0)
    varA_ctl_ens = np.ma.mean(varA_ctl, axis=0)
    varB_ctl_ens = np.ma.mean(varB_ctl, axis=0)
    diffA_ctl_ens = np.ma.mean(diffA_ctl, axis=0)
    diffB_ctl_ens = np.ma.mean(diffB_ctl, axis=0)
    diffBA_ctl_ens = np.ma.mean(diffBA_ctl, axis=0)
    var0_scen_ens = np.ma.mean(var0_scen, axis=0)
    varA_scen_ens = np.ma.mean(varA_scen, axis=0)
    varB_scen_ens = np.ma.mean(varB_scen, axis=0)
    diffA_scen_ens = np.ma.mean(diffA_scen, axis=0)
    diffB_scen_ens = np.ma.mean(diffB_scen, axis=0)
    diffBA_scen_ens = np.ma.mean(diffBA_scen, axis=0)

    diffB_scen_ctl = diffB_scen_ens - diffB_ctl_ens

    #test if ensemble mean difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    ks_B = np.ndarray((2,len(lats),len(lons)))
    H_B = np.ndarray((len(lats),len(lons)))
    for x in range(len(lats)):
        for y in range(len(lons)):
            ks_B[:,x,y] = stats.ks_2samp(diffB_ctl_ens[:,x,y], diffB_scen_ens[:,x,y])
            if ks_B[1,x,y] < sig:
                H_B[x,y] = 1
            else:
                H_B[x,y] = 0

    #average data over time to plot map
    diffB_scen_ctl_avg = np.ma.mean(diffB_scen_ctl, axis=0)

    #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    masked_diffB = diffB_scen_ctl_avg[mask_ind,:]
    masked_HB = H_B[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)
    # save as eps because problem with hatching when using pdf (defined in header)
    # define levels for contour plots
    if (indices[ind] == 'HWA'):
        lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]
    elif (indices[ind] == 'HWD'):
        lev = [-20,-16,-12,-8,-2,2,8,12,16,20]
        #lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]
    elif (indices[ind] == 'HWF'):
        lev = [-13.6,-10.5,-7.5,-4.5,-1.5,1.5,4.5,7.5,10.5,13.5]
    elif (indices[ind] == 'HWM'):
        lev = [-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9]
    elif (indices[ind] == 'HWN'):
        lev = [-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5]
    elif (indices[ind] == 'HWT'):
        lev = [-22.5,-17.5,-12.5,-7.5,-2.5,2.5,7.5,12.5,17.5,22.5]

    colbar='RdBu_r'

    # plot difference GC1B85-CTL SCEN-CTL
    plotname = outdir + experiments[2]+'/EHF/'+indices[ind]+'_diff_'+experiments[2]+'-CTL_KSsig_ANN_'+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)+'_noACCESS'
    #print plotname
    title = indices[ind]+' ['+units+'] SMtrend--CTL '+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)
    utils.draw(masked_diffB,masked_lats, lons, title = title, colors=colbar,levels=lev, sig=masked_HB)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    #also save ensemble differences in netcdf for later panelling
    fout=nc.Dataset('%s/%s_diffs_ENSMEAN_noACCESS_ANN_%s--%s.nc'%(outdir,indices[ind],str(A2_start)+'-'+str(A2_end),str(A1_start)+'-'+str(A1_end)),mode='w')
    fout.createDimension('lat',masked_lats.shape[0])
    fout.createDimension('lon',lons.shape[0])

    latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
    setattr(latout,'Longname','Latitude')
    setattr(latout,'units','degrees_north')

    lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
    setattr(lonout,'Longname','Longitude')
    setattr(lonout,'units','degrees_east')

    diffBout=fout.createVariable('ExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(diffBout,'Longname','Difference between ExpB and CTL')
    setattr(diffBout,'units',units[ind])
    setattr(diffBout,'description','ExpB-CTL '+indices[ind]+' SCEN-CTL')

    sigBout=fout.createVariable('sigExpB-CTL','f8',('lat','lon'),fill_value=-9999)
    setattr(sigBout,'Longname','Significance in difference between ExpB and CTL')
    setattr(sigBout,'units','-')
    setattr(sigBout,'description','Statistical significance ExpB-CTL '+indices[ind]+' SCEN-CTL')

    latout[:]=masked_lats[:]
    lonout[:]=lons[:]
    diffBout[:]=masked_diffB[:]
    sigBout[:]=masked_HB[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
    setattr(fout,"contact","r.lorenz@unsw.edu.au")
    setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout,"Script","plot_EHF_CTL-EXP_GCCMIP5_ENSMEAN_SCEN-CTL.py")
    setattr(fout,"Input files located in:", indir)
    fout.close()
 
