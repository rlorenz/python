#!/usr/bin/python
'''
File Name : plot_SM_std_GCCMIP5_ENSMEAN_SCEN-CTL.py
Creation Date : 21-01\7-2015
Last Modified : 
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : Plot difference in soil moisture variability between
          SCEN and CTL periods

'''
# Load modules for this script
import numpy as np
from scipy.io import netcdf_file
from scipy import stats
import datetime as dt
import netCDF4 as nc
import utils
from subprocess import call
from netcdftime import utime

###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/'
#models = ['CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
models = ['CESM','EC-EARTH','ECHAM6','GFDL']
experiments = ['CTL']
variables = ['mrso']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1955
R0_end = 2100

# define analysis start and end year
A1_start = 1971
A1_end = 2000

A2_start = 2056
A2_end = 2085

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over models to read all data
for mod in range(len(models)):
# Load netcdf files
    path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'01-'+str(R0_end)+'12_maskocean_remapcon.nc'

# read variable for 1st run (ctl), read lats and lons as well
    ifile = netcdf_file(path0)
    var = ifile.variables[variables[0]]
    miss_val=var._FillValue
    if (models[mod] == "EC-EARTH"):
        var0=np.ma.masked_values(var[:,0,:,:],miss_val)
    else:
        var0=np.ma.masked_values(var[:],miss_val)
    lats = ifile.variables['lat'].data
    lons = ifile.variables['lon'].data
    time = ifile.variables['time']
    if (models[mod] == "ECHAM6"):
        dates=time.data
        years=np.floor(dates/10000)
    else:
        cdftime = utime(time.units)
        dates=cdftime.num2date(time.data)
        years=np.asarray([dates[i].year for i in xrange(len(dates))])
    ifile.close()

    # cut data into analysis period
    nyears1 = A1_end-A1_start+1
    ind1 = np.where(years==A1_start)[0][0]
    ind2 = np.where(years==A1_end)[0][0]+12

    nyears2 = A2_end-A2_start+1
    ind3 = np.where(years==A2_start)[0][0]
    ind4 = np.where(years==A2_end)[0][0]+12

    if mod==0:
        var0_tp1=np.ma.empty([len(models),nyears1*12,len(lats),len(lons)],dtype=float)
        var0_tp2=np.ma.empty([len(models),nyears2*12,len(lats),len(lons)],dtype=float)

    var0_tp1_tmp = np.ma.copy(var0)
    var0_tp1[mod,:,:,:] = var0_tp1_tmp[ind1:ind2,:,:]
    var0_tp2_tmp = np.ma.copy(var0)
    var0_tp2[mod,:,:,:] = var0_tp2_tmp[ind3:ind4,:,:]

    #average data over time
    var0_mean = np.ma.mean(var0, axis=0)

    ntim = var0_tp2.shape[0]
    #calculate standard deviation from annual means
    if (nyears1 != nyears2): print('Warning: length of time periods is different tp1:'+nyears1+', tp2:'+nyears2)
    if mod==0:
        var0_tp1_amean = np.ndarray((len(models),nyears1,len(lats),len(lons)))
        var0_tp2_amean = np.ndarray((len(models),nyears2,len(lats),len(lons)))
        var0_tp1_std_mean = np.ndarray((len(models),len(lats),len(lons)))
        var0_tp2_std_mean = np.ndarray((len(models),len(lats),len(lons)))

    t = 0
    for ind in range(0,ntim-1,12):
        var0_tp1_amean[mod,t,:,:] = np.ma.mean(var0_tp1[mod,ind:ind+12,:,:], axis=0)
        var0_tp2_amean[mod,t,:,:] = np.ma.mean(var0_tp2[mod,ind:ind+12,:,:], axis=0)
        t = t+1
        ind = ind+12
    #std over years
    var0_tp1_std_mean[mod,:,:] = np.ma.std(var0_tp1_amean[mod,:,:,:], axis=0)
    var0_tp2_std_mean[mod,:,:] = np.ma.std(var0_tp2_amean[mod,:,:,:], axis=0)
    print  var0_tp1_std_mean.shape,  var0_tp2_std_mean.shape
    if mod==0:
        diff_tp2_tp1=np.ma.empty([len(models),len(lats),len(lons)],dtype=float)

    diff_tp2_tp1[mod,:,:] = (var0_tp2_std_mean[mod,:,:]-var0_tp1_std_mean[mod,:,:])

#calculate ensemble mean
diff_tp2_tp1_ens=np.ma.mean(diff_tp2_tp1, axis=0)
var0_tp1_std_mean_ens=np.ma.mean(var0_tp1_std_mean, axis=0)
var0_tp2_std_mean_ens=np.ma.mean(var0_tp2_std_mean, axis=0)
print diff_tp2_tp1_ens.shape, var0_tp1_std_mean_ens.shape, var0_tp2_std_mean_ens.shape
# check where at least 5 out of 6 models agree on sign of change
H_agree=np.ndarray((len(lats),len(lons)))

for x in range(len(lats)):
    for y in range(len(lons)):
        neg = sum(np.ma.count(np.ma.where(diff_tp2_tp1[:,x,y]<0),0))
        pos = sum(np.ma.count(np.ma.where(diff_tp2_tp1[:,x,y]>0),0))
        agree=max(pos,neg)
        if agree >= 5:
            H_agree[x,y]=1
        else:
            H_agree[x,y]=0

#mask very high northern and southern latitudes for plotting
mask_ind = np.where((lats < 80) & (lats > -60))[0]
masked_lats = lats[mask_ind]

masked_diff_tp2_tp1_ens = diff_tp2_tp1_ens[mask_ind,:]

masked_agree_H = H_agree[mask_ind,:]
masked_var0_std_tp1_ens = var0_tp1_std_mean_ens[mask_ind,:]
masked_var0_std_tp2_ens = var0_tp2_std_mean_ens[mask_ind,:]

print masked_var0_std_tp1_ens.shape, np.amin(masked_var0_std_tp1_ens), np.amax(masked_var0_std_tp1_ens) 
print np.amin(masked_diff_tp2_tp1_ens), np.amax(masked_diff_tp2_tp1_ens)
# plot data using utils.py (based on Jeffs from python workshop)
lev = [0,5,10,15,30,40,50,60,70,80,90,100]
colbar='YlOrBr'
# plot std tp1
plotname = outdir + experiments[0]+'/'+variables[0]+'_std_CTL_'+str(A1_start)+'-'+str(A1_end)
print plotname + plottype
title = 'Ensemble soil moisture variability (std) CTL '+str(A1_start)+'-'+str(A1_end)
utils.draw(masked_var0_std_tp1_ens,masked_lats, lons, title = title, colors=colbar,levels=lev)
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

# plot std tp2
plotname = outdir + experiments[0]+'/'+variables[0]+'_std_CTL_'+str(A2_start)+'-'+str(A2_end)
print plotname + plottype
title = 'Ensemble soil moisture variability (std) CTL '+str(A2_start)+'-'+str(A2_end)
utils.draw(masked_var0_std_tp2_ens,masked_lats, lons, title = title, colors=colbar,levels=lev)
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)
     
# plot difference tp2-tp1
colbar='RdBu_r'
lev_d = [-110,-90,-70,-50,-30,-10,10,30,50,70,90,110]
#lev_d = [-1.1,-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9,1.1]
#lev_d = [-0.325,-0.275,-0.225,-0.175,-0.125,-0.075,-0.025,0.025,0.075,0.125,0.175,0.225,0.275,0.325]
#lev_d = [-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11]
plotname = outdir + experiments[0]+'/'+variables[0]+'_diff_std_CTL_'+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)
print plotname + plottype
title = 'Ensemble soil moisture variability (std) CTL '+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)
utils.draw(masked_diff_tp2_tp1_ens,masked_lats, lons, title = title, colors=colbar,levels=lev_d,sig=masked_agree_H )
#utils.plt.show()
utils.plt.savefig(plotname+plottype)
call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

#also save ensemble differences in netcdf for later panelling
fout=nc.Dataset('%s/SM_std_ENSMEAN_ANN_%s-%s--%s-%s.nc'%(outdir,str(A2_start),str(A2_end),str(A1_start),str(A1_end)),mode='w')
fout.createDimension('lat',masked_lats.shape[0])
fout.createDimension('lon',lons.shape[0])

latout=fout.createVariable('lat','f8',('lat'),fill_value=miss_val)
setattr(latout,'Longname','Latitude')
setattr(latout,'units','degrees_north')

lonout=fout.createVariable('lon','f8',('lon'),fill_value=miss_val)
setattr(lonout,'Longname','Longitude')
setattr(lonout,'units','degrees_east')

std1out=fout.createVariable('SMstd_tp1','f8',('lat','lon'),fill_value=miss_val)
setattr(std1out,'Longname','SM variability')
setattr(std1out,'units','-')
setattr(std1out,'description','SM standard deviation from '+str(A1_start)+' to ' +str(A1_end))

std2out=fout.createVariable('SMstd_tp2','f8',('lat','lon'),fill_value=miss_val)
setattr(std1out,'Longname','SM variability')
setattr(std1out,'units','-')
setattr(std1out,'description','SM standard deviation from '+str(A2_start)+' to ' +str(A2_end))

diffstdout=fout.createVariable('SMstd_tp2-tp1','f8',('lat','lon'),fill_value=miss_val)
setattr(diffstdout,'Longname','Difference in SM variability')
setattr(diffstdout,'units','-')
setattr(diffstdout,'description','Difference in SM standard deviation from '+str(A2_start)+' to ' +str(A2_end)+' minus ' +str(A1_start)+' to ' +str(A1_end))

sigout=fout.createVariable('agree_tp2-tp1','f8',('lat','lon'),fill_value=miss_val)
setattr(sigout,'Longname','Agreement in Ensemble')
setattr(sigout,'units','-')
setattr(sigout,'description','Agreement in Ensemble for change in SM standard deviation from '+str(A2_start)+' to ' +str(A2_end)+' minus ' +str(A1_start)+' to ' +str(A1_end))

latout[:]=masked_lats[:]
lonout[:]=lons[:]
std1out[:]=masked_var0_std_tp1_ens[:]
std2out[:]=masked_var0_std_tp2_ens[:]
diffstdout[:]=masked_diff_tp2_tp1_ens[:]
sigout[:]=masked_agree_H[:]

# Set global attributes
setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
setattr(fout,"contact","r.lorenz@unsw.edu.au")
setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
setattr(fout,"Script","plot_SM_std_GCCMIP5_ENSMEAN_SCEN-CTL.py")
setattr(fout,"Input files located in:", indir)
fout.close()
