#!/usr/bin/python
'''
File Name : plot_SM_diff_GC1B85-GC1A85.py
Creation Date : 14-01-2015
Last Modified : Tue 07 Apr 2015 22:12:29 AEST
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : Plot difference in soil moisture between ExpB and ExpA
	for individual models to help interpreting results


'''
# Load modules for this script
import numpy as np
from scipy.io import netcdf_file
from scipy import stats
import datetime as dt
import netCDF4 as nc
import utils
from subprocess import call
from netcdftime import utime

###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/'
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
#models = ['EC-EARTH']
experiments = ['CTL']
variables = ['mrso']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = 1955
R0_end = 2100

# define analysis start and end year
A1_start = 1971
A1_end = 2000

A2_start = 2056
A2_end = 2085

#choose significance level for KS-test
sig = 0.05
#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".eps"

# Loop over models to read all data
for mod in range(len(models)):
# Load netcdf files
    path0 = indir + models[mod] +'/'+ experiments[0]+'/'+ variables[0]+'_monthly_' + models[mod]+'_'+experiments[0]+'_1_'+str(R0_start)+'01-'+str(R0_end)+'12_maskocean.nc'

# read variable for 1st run (ctl), read lats and lons as well
    ifile = netcdf_file(path0)
    var = ifile.variables[variables[0]]
    miss_val=var._FillValue
    #print miss_val
    if (models[mod] == "EC-EARTH"):
        var0=np.ma.masked_values(var[:,0,:,:],miss_val)
    else:
        var0=np.ma.masked_values(var[:],miss_val)
    lats = ifile.variables['lat'].data
    lons = ifile.variables['lon'].data
    time = ifile.variables['time']
    if (models[mod] == "ECHAM6"):
        dates=time.data
        years=np.floor(dates/10000)
    else:
        cdftime = utime(time.units)
        dates=cdftime.num2date(time.data)
        years=np.asarray([dates[i].year for i in xrange(len(dates))])
    ifile.close()
    #print 'var0:', var0.shape, np.amin(var0), np.amax(var0)
    # cut data into analysis period
    var0_tp1 = np.ma.copy(var0)
    var0_tp2 = np.ma.copy(var0)

    nyears1 = A1_end-A1_start+1
    ind1 = np.where(years==A1_start)[0][0]
    ind2 = np.where(years==A1_end)[0][0]+12
    var0_tp1 = var0_tp1[ind1:ind2,:,:]

    nyears2 = A2_end-A2_start+1
    ind3 = np.where(years==A2_start)[0][0]
    ind4 = np.where(years==A2_end)[0][0]+12
    var0_tp2 = np.ma.copy(var0)
    var0_tp2 = var0_tp2[ind3:ind4,:,:]

    var0_mean = np.ma.mean(var0, axis=0)
    #print 'var0_tp1:', type(var0_tp1), var0_tp1.shape, np.amin(var0_tp1), np.amax(var0_tp1)
    #print 'var0_tp2:', type(var0_tp2), var0_tp2.shape, np.amin(var0_tp2), np.amax(var0_tp2)
    ntim = var0_tp2.shape[0]
    #calculate standard deviation from annual means
    if (nyears1 != nyears2): print('Warning: length of time periods is different tp1:'+nyears1+', tp2:'+nyears2)
    var0_tp1_amean = np.ndarray((nyears1,len(lats),len(lons)))
    var0_tp2_amean = np.ndarray((nyears2,len(lats),len(lons)))
    t = 0
    for ind in range(0,ntim-1,12):
        var0_tp1_amean[t,:,:] = np.ma.mean(var0_tp1[ind:ind+12,:,:], axis=0)
        var0_tp2_amean[t,:,:] = np.ma.mean(var0_tp2[ind:ind+12,:,:], axis=0)
        t = t+1
        ind = ind+12
    #print 'var0_tp2_std', var0_tp2_std.shape, np.amin(var0_tp2_std), np.amax(var0_tp2_std)

    #average std over years
    var0_tp1_std_mean = np.ma.std(var0_tp1_amean, axis=0)
    var0_tp2_std_mean = np.ma.std(var0_tp2_amean, axis=0)
    print 'var0_tp2_std_mean:', var0_tp2_std_mean.shape, np.amin(var0_tp2_std_mean), np.amax(var0_tp2_std_mean)

    #test if difference is statistically significant
    #ks_2samp gives 2 values: KS statistic and two tailed p-value
    #if p-value is below signigicant level we can reject the hypothesis
    #that the distributions of the two samples are the same
    #ks = np.ndarray((2,len(lats),len(lons)))
    #H = np.ndarray((len(lats),len(lons)))

    #for x in range(len(lats)):
    #    for y in range(len(lons)):
    #        ks[:,x,y] = stats.ks_2samp(var0_tp1_std[:,x,y], var0_tp2_std[:,x,y])
    #        if ks[1,x,y] < sig:
    #            H[x,y] = 1
    #        else:
    #            H[x,y] = 0


    # difference normalized by overall mean
    #diff_tp2_tp1 = (var0_tp2_std_mean-var0_tp1_std_mean)/var0_mean
    diff_tp2_tp1 = (var0_tp2_std_mean-var0_tp1_std_mean)
    print 'diff_tp2_tp1:', diff_tp2_tp1.shape, np.amin(diff_tp2_tp1), np.amax(diff_tp2_tp1)

    #mask where not statistically significant
    #diff_tp2_tp1_sig=np.ma.masked_where(H==0,diff_tp2_tp1)

   #mask very high northern and southern latitudes for plotting
    mask_ind = np.where((lats < 80) & (lats > -60))[0]
    masked_lats = lats[mask_ind]

    #masked_diff_tp2_tp1_sig = diff_tp2_tp1_sig[mask_ind,:]
    masked_diff_tp2_tp1 = diff_tp2_tp1[mask_ind,:]

    #masked_H = H[mask_ind,:]

    masked_std_tp1 = var0_tp1_std_mean[mask_ind,:]
    masked_std_tp2 = var0_tp2_std_mean[mask_ind,:]

    # plot data using utils.py (based on Jeffs from python workshop)  
    # plot variability in CTL
    lev = [0,5,10,15,30,40,50,60,70,80,90,100,110]
    colbar='YlOrBr'
    plotname = outdir +models[mod]+'/'+ experiments[0]+'/'+variables[0]+'_std_CTL_'+str(A1_start)+'-'+str(A1_end)
    print plotname + plottype
    title = models[mod]+' soil moisture variability (std) CTL '+str(A1_start)+'-'+str(A1_end)
    utils.draw(masked_std_tp1,masked_lats, lons, title = title, colors=colbar,levels=lev)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    plotname = outdir +models[mod]+'/'+ experiments[0]+'/'+variables[0]+'_std_CTL_'+str(A2_start)+'-'+str(A2_end)
    print plotname + plottype
    title = models[mod]+' soil moisture variability (std) CTL '+str(A2_start)+'-'+str(A2_end)
    utils.draw(masked_std_tp2,masked_lats, lons, title = title, colors=colbar,levels=lev)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    colbar='RdBu_r'
    lev_d = [-110,-90,-70,-50,-30,-10,10,30,50,70,90,110]
    #lev_d = [-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11]
    #lev_d = [-0.325,-0.275,-0.225,-0.175,-0.125,-0.075,-0.025,0.025,0.075,0.125,0.175,0.225,0.275,0.325]
    plotname = outdir +models[mod]+'/'+ experiments[0]+'/'+variables[0]+'_diff_std_CTL_'+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)
    print plotname + plottype
    title = models[mod]+' soil moisture variability (std) CTL '+str(A2_start)+'-'+str(A2_end)+'--'+str(A1_start)+'-'+str(A1_end)
    utils.draw(masked_diff_tp2_tp1,masked_lats, lons, title = title, colors=colbar,levels=lev_d)
    #utils.plt.show()
    utils.plt.savefig(plotname+plottype)
    call("ps2pdf "+plotname+plottype+" "+plotname+".pdf",shell=True)
    call("pdfcrop "+plotname+".pdf "+plotname+".pdf",shell=True)

    #also save ensemble differences in netcdf for later panelling
    fout=nc.Dataset('%s/%s/SM_std_%s_ANN_%s-%s--%s-%s.nc'%(outdir,models[mod],models[mod],str(A2_start),str(A2_end),str(A1_start),str(A1_end)),mode='w')
    fout.createDimension('lat',masked_lats.shape[0])
    fout.createDimension('lon',lons.shape[0])

    latout=fout.createVariable('lat','f8',('lat'),fill_value=miss_val)
    setattr(latout,'Longname','Latitude')
    setattr(latout,'units','degrees_north')

    lonout=fout.createVariable('lon','f8',('lon'),fill_value=miss_val)
    setattr(lonout,'Longname','Longitude')
    setattr(lonout,'units','degrees_east')

    std1out=fout.createVariable('SMstd_tp1','f8',('lat','lon'),fill_value=miss_val)
    setattr(std1out,'Longname','SM variability')
    setattr(std1out,'units','-')
    setattr(std1out,'description','SM standard deviation from '+str(A1_start)+' to ' +str(A1_end))

    std2out=fout.createVariable('SMstd_tp2','f8',('lat','lon'),fill_value=miss_val)
    setattr(std2out,'Longname','SM variability')
    setattr(std2out,'units','-')
    setattr(std2out,'description','SM standard deviation from '+str(A2_start)+' to ' +str(A2_end))

    diffstdout=fout.createVariable('SMstd_tp2-tp1','f8',('lat','lon'),fill_value=miss_val)
    setattr(diffstdout,'Longname','Difference in SM variability')
    setattr(diffstdout,'units','-')
    setattr(diffstdout,'description','Difference in SM standard deviation from '+str(A2_start)+' to ' +str(A2_end)+'minus' +str(A1_start)+' to ' +str(A1_end))

    #sigout=fout.createVariable('sig_tp2-tp1','f8',('lat','lon'),fill_value=miss_val)
    #setattr(sigout,'Longname','Significance between tp2-tp1')
    #setattr(sigout,'units','-')
    #setattr(sigout,'description','Statistical significance SM std from '+str(A2_start)+' to ' +str(A2_end)+'minus' +str(A1_start)+' to ' +str(A1_end))

    latout[:]=masked_lats[:]
    lonout[:]=lons[:]
    std1out[:]=masked_std_tp1[:]
    std2out[:]=masked_std_tp2[:]
    diffstdout[:]=masked_diff_tp2_tp1[:]
    #sigout[:]=masked_H[:]

    # Set global attributes
    setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
    setattr(fout,"contact","r.lorenz@unsw.edu.au")
    setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
    setattr(fout,"Script","plot_SM_std_GCCMIP5_allGCM_SCEN-CTL.py")
    setattr(fout,"Input files located in:", indir)
    fout.close()
