#!/usr/bin/python
'''
File Name : plot_EHF_GCCMIP5_allGCM.py
Creation Date : 06-12-2014
Last Modified : Sun 08 Mar 2015 20:37:43 AEDT
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose :
Calculate Excees Heat Factor heatwaves (Perkins and Alexander, 2012 JCLIM; Perkins et al.,2012 GRL)

Yearly maximum heatwave intensity 
Yearly average heatwave intensity 
Yearly number of heatwave days
Yearly number of heatwaves
Duration of yearly longest heatwave

uses compute_EHFheatwaves.py from Dani Argueso, based on Sarah Perkinss matlab script EHF_index.m

calculated for all GLACE-CMIP5 GCMs

Experiments: CTL, GC1A85, GC1B85

'''
import numpy as np
import datetime as dt
import netCDF4 as nc
#from constants import const
from compute_EHFheatwaves import compute_EHF
import glob as glob
import sys
import os
from subprocess import call
call("module load cdo",shell=True)
###
# Define input
###
models = ['ACCESS','CESM','EC-EARTH','ECHAM6','GFDL','IPSL']
#models = ['ACCESS']
experiments = ['CTL','GC1A85','GC1B85']
#experiments = ['CTL']

# start and end of runs (adjust syfile for models/experiments
# where different within loop)
syfile=1950
eyfile=2100
# define analysis start and end year
syear=2056
eyear=2085
### BASE PERIOD ####
# using a base period for the calculation of the percentiles that is different from the main period
# a threshold file over that period must be provided.
sy_base=1961 
ey_base=1990
seas='yearly'
modified=True #Either using Sarah's or the original version. Differences in the calculation of percentiles

# Loop over models
for mod in range(len(models)):
    for exp in range(len(experiments)):
###
        indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/'+experiments[exp]+'/'
        outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/'+experiments[exp]+'/EHF/'
        if (os.access(outdir,os.F_OK)==False):
            os.mkdir(outdir)

	thfile='/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/'+experiments[0]+'/EHF/'+models[mod]+'_'+experiments[0]+'_'+str(sy_base)+'-'+str(ey_base)+'_EHFheatwaves_'+seas+'.nc' #use percentiles from CTL for all experiments
	#thfile=None

	if ((sy_base!=syear) or (ey_base!=eyear)) and thfile==None:
  		sys.exit('For a base period different from the computation period a threshold file is required \n It wasn\'t provided. Please provide a threshold file')

        #filemask=nc.Dataset('/srv/ccrc/data32/z3441306/GLACE-CMIP5/'+ models[mod] +'/landfrac_gt09.nc')
        #if (models[mod]=="ACCESS"):
        #  lm=filemask.variables['sftlf'][0,0,:,:]
        #elif (models[mod]=="EC-EARTH"):
        #  lm=filemask.variables['sftlf'][0,:,:]
        #elif (models[mod]=="ECHAM6"):
        #  lm=filemask.variables['cover_type'][0,:,:]
        #else:
        #  lm=filemask.variables['sftlf'][:]
        #print lm.shape

        # Load netcdf files   
        if (models[mod]=="CESM") and (experiments[exp]=="CTL"):
            syfile = 1955
            filen=nc.Dataset(indir+ 'tasmin_daily_CCSM4_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            filex=nc.Dataset(indir+ 'tasmax_daily_CCSM4_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
        elif (models[mod]=="GFDL"):
            syfile = 1951
            filen=nc.Dataset(indir+ "tasmin_daily_"+ models[mod]+'_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            filex=nc.Dataset(indir+ "tasmax_daily_"+ models[mod]+'_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
        else:
            syfile = 1950
            filen=nc.Dataset(indir+ "tasmin_daily_"+ models[mod]+'_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')
            filex=nc.Dataset(indir+ "tasmax_daily_"+ models[mod]+'_'+experiments[exp]+'_1_'+str(syfile)+'01-'+str(eyfile)+'12.nc')

        lat=filex.variables['lat'][:]
        lon=filex.variables['lon'][:]
        #############################
        d1 = dt.datetime(syfile,1,1,0,0)
        d2 = dt.datetime(eyfile,12,31,0,0)
        diff = d2 - d1
        dates=np.asarray([d1 + dt.timedelta(i) for i in range(diff.days+1)])
        years=np.asarray([dates[i].year for i in xrange(len(dates))])

        dates=dates[(years>=syear) & (years<=eyear)]
        nyears=eyear-syear+1
        #################################################
        ########### WRITTING OUT NCFILES#################
        #################################################
        datesout= [dt.datetime(syear+x,06,01,00) for x in range(0,nyears)]
        time=nc.date2num(datesout,units='days since %s' %(nc.datetime.strftime(dt.datetime(syear+x,01,01,00), '%Y-%m-%d_%H:%M:%S')),calendar='standard')

        print '%s/%s_%s_%s-%s_EHFheatwaves_%s.nc'%(outdir,models[mod],experiments[exp],syear,eyear,seas)
        fout=nc.Dataset('%s/%s_%s_%s-%s_EHFheatwaves_%s.nc'%(outdir,models[mod],experiments[exp],syear,eyear,seas),mode='w')
        fout.createDimension('time',len(datesout))
        fout.createDimension('DoY',365)
        fout.createDimension('lat',lat.shape[0])
        fout.createDimension('lon',lon.shape[0])

        timeout=fout.createVariable('time','f8','time',fill_value=-9999)
        setattr(timeout,'units','days since %s' %(nc.datetime.strftime(dt.datetime(syear+x,01,01,00), '%Y-%m-%d_%H:%M:%S')))

        latout=fout.createVariable('lat','f8',('lat'),fill_value=-9999)
        setattr(latout,'Longname','Latitude')
        setattr(latout,'units','degrees_north')

        lonout=fout.createVariable('lon','f8',('lon'),fill_value=-9999)
        setattr(lonout,'Longname','Longitude')
        setattr(lonout,'units','degrees_east')

        if modified==True:
  
            prcout=fout.createVariable('PRCTILE90','f8',('DoY','lat','lon'),fill_value=-9999)
            setattr(prcout,'Longname','Percentile 90th')
            setattr(prcout,'units','K')
            setattr(prcout,'description','Percentile 90th for each calendar day using %s-day window' %(str(15)))
        else:
  
            prcout=fout.createVariable('PRCTILE95','f8',('DoY','lat','lon'),fill_value=-9999)
            setattr(prcout,'Longname','Percentile 95th')
            setattr(prcout,'units','K')
            setattr(prcout,'description','Percentile 95th over the entire base_period')

        HWAout=fout.createVariable('HWA_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWAout,'Longname','Peak of the hottest heatwave per year')
        setattr(HWAout,'units','K')
        setattr(HWAout,'description','Peak of the hottest heatwave per year - yearly maximum of each heatwave peak')

        HWMout=fout.createVariable('HWM_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWMout,'Longname','Average magnitude of the yearly heatwave')
        setattr(HWMout,'units','K')
        setattr(HWMout,'description','Average magnitude of the yearly heatwave - yearly average of heatwave magnitude')

        HWNout=fout.createVariable('HWN_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWNout,'Longname','Number of heatwaves')
        setattr(HWNout,'units','')
        setattr(HWNout,'description','Number of heatwaves per year')

        HWFout=fout.createVariable('HWF_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWFout,'Longname','Number of heatwave days')
        setattr(HWFout,'units','')
        setattr(HWFout,'description','Number of heatwave days - expressed as the percentage relative to the total number of days')

        HWDout=fout.createVariable('HWD_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWDout,'Longname','Duration of yearly longest heatwave')
        setattr(HWDout,'units','days')
        setattr(HWDout,'description','Duration of the longest heatwave per year')

        HWTout=fout.createVariable('HWT_EHF','f8',('time','lat','lon'),fill_value=-9999)
        setattr(HWTout,'Longname','First heat wave day of the year')
        setattr(HWTout,'units','day')
        setattr(HWTout,'description','First heat wave day of the year from 1st of Jan')

        timeout[:]=time[:]
        latout[:]=lat[:]
        lonout[:]=lon[:]

        tasmin=filen.variables['tasmin'][(years>=syear) & (years<=eyear),:,:]
        tasmax=filex.variables['tasmax'][(years>=syear) & (years<=eyear),:,:]

        HWA_EHF_yearly,HWM_EHF_yearly,HWF_EHF_yearly,HWN_EHF_yearly,HWD_EHF_yearly,HWT_EHF_yearly,pctcalc=compute_EHF(tasmax,tasmin,dates=dates,modified=modified,thres_file=thfile,season=seas)

        #pctcalc[:,lm<1]=-9999
        #HWA_EHF_yearly[:,lm<1]=-9999
        #HWM_EHF_yearly[:,lm<1]=-9999
        #HWN_EHF_yearly[:,lm<1]=-9999
        #HWF_EHF_yearly[:,lm<1]=-9999
        #HWD_EHF_yearly[:,lm<1]=-9999
        #HWT_EHF_yearly[:,lm<1]=-9999

        prcout[:]=pctcalc[:]
        HWAout[:]=HWA_EHF_yearly[:]
        HWMout[:]=HWM_EHF_yearly[:]
        HWNout[:]=HWN_EHF_yearly[:]
        HWFout[:]=HWF_EHF_yearly[:]
        HWDout[:]=HWD_EHF_yearly[:]
        HWTout[:]=HWT_EHF_yearly[:]

# Set global attributes
        setattr(fout,"author","Ruth Lorens @ ARCCSS + CCRC, UNSW, Australia")
        setattr(fout,"contact","r.lorenz@unsw.edu.au")
        setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
        if thfile!=None:
            setattr(fout,"comment","File generated using the base period %s-%s and using the threshold file: %s" %(sy_base,ey_base,thfile))
        else:
            setattr(fout,"comment","File generated using the base period %s-%s and threshold file was generated (it did not exist previously)" %(sy_base,ey_base))

            setattr(fout,"Script","plot_EHF_GCCMIP5_allGCM.py")
            setattr(fout,"Input files located in:", indir)
        fout.close()

        #remap all models to same grid (coarsest from IPSL)
        call("cdo remapcon,/srv/ccrc/data32/z3441306/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc "+outdir+models[mod]+"_"+experiments[exp]+"_"+str(syear)+"-"+str(eyear)+"_EHFheatwaves_"+seas+".nc " +outdir+models[mod]+"_"+experiments[exp]+"_"+str(syear)+"-"+str(eyear)+"_EHFheatwaves_"+seas+"_remapcon.nc",shell=True)
