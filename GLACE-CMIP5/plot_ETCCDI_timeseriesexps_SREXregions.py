#!/usr/bin/python

'''
File Name : timeseriesexps.py
Creation Date: 11 Dec 2014
Last Modified: 11 Dec 2014
Author: Georgia Tsambos (g.tsambos@gmail.com)

Purpose:
Script that reads and analyses netcdfs from GLACE-CMIP5 project
Data hosted at ETH IAC
Downloaded to storm servers: /srv/ccrc/data32/z3441306/GLACE-CMIP5/
by Ruth Lorenz

6 GCMs are : ACCESS, CESM, EC-EARTH, ECHAM6, GFDL, IPSL

Experiments: CTL, GC1A85, GC1B85
(see Seneviratne et al. 2013, GRL for more info)
This script compares index  trends in different models over time

Read climdex indices calculated with /home/z3441306/fclimdex/fclimdex_readperc/fclimdex.exe
in /srv/ccrc/data32/z3441306/GLACE-CMIP5/$model/$experiment/climdex_index/*_maskocean.nc
these files have masked the ocean coordinates

call within outdir because of pdfcrop!
'''

# Load modules for this script
import pdb 
from scipy.io import netcdf_file
from scipy import stats
import numpy as np
import numpy.ma as ma
import pandas as pd
from pandas import Series
import matplotlib
import matplotlib.pyplot as plt
from subprocess import call
import sys
###
# Define input
###
indir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5/'
outdir = '/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/climdex/'
models = ['ACCESS', 'CESM', 'EC-EARTH', 'ECHAM6', 'IPSL','GFDL']
experiments = ['CTL','GC1A85','GC1B85']
indices = ['TX90p','TN90p','TNx','TXx','WSDI','R95p','R10mm','CDD','CWD']

# start and end of 1st run in analysis (CTL data starts later than experiments)
R0_start = np.array([1949, 1954, 1949, 1949, 1949, 1950])
R0_end = 2100
# start and end of 2nd and 3rd run in analysis (ExpA and ExpB have same time periods)
R1_start = np.array([1950, 1950, 1950, 1950, 1950, 1950])
R1_end = 2100

#define regions
regions=["MED","CNA","NAU","SAF"]
lat_max = np.array([45,50,  -10, -11.365])
lat_min =  np.array([30,25,  -30, -35])
lon_max =  np.array([40,-85,  155, 51.99])
lon_min = np.array([-10,-110,  110, -10])

#define format of figures, use eps because of problems with hatching when using .pdf
plottype = ".pdf"

#Loop over indices
for ind in range(len(indices)):
    print indices[ind]

    #Loop over regions
    for reg in range(len(regions)):
        print regions[reg]

        #Loop over experiments
        for exp in range(len(experiments)):
            print experiments[exp]
            #Loop over models
            for mod in range(len(models)):
                print models[mod]
            ###
            # Load netcdf files
                path0 = indir + models[mod] + '/' + experiments[0] + '/climdex_index/' + models[mod] + '_' + experiments[0] + '_1_' + str(R0_start[mod]) + '-' + str(R0_end) + '_' + indices[ind] + '_maskocean.nc'# using the masked ocean files
                path1 = indir + models[mod] + '/' + experiments[1] + '/climdex_index/' + models[mod] + '_' + experiments[1] + '_1_' + str(R1_start[mod]) + '-'+ str(R1_end) + '_' + indices[ind] + '_maskocean.nc'   
                path2 = indir + models[mod] + '/' + experiments[2] + '/climdex_index/' + models[mod] + '_' + experiments[2] + '_1_' + str(R1_start[mod]) + '-'+ str(R1_end) + '_' + indices[ind] + '_maskocean.nc'
                #Read annual data, (and time, lats and lons just once)
                if exp == 0:
                    ifile = netcdf_file(path0)
                    var = ifile.variables['Annual'].data
                    lats = ifile.variables['lat'].data
                    lons = ifile.variables['lon'].data
                    time = ifile.variables['time'].data
                    print min(lons), max(lons)
                if exp == 1:
                    ifile = netcdf_file(path1)
                    var = ifile.variables['Annual'].data
                if exp == 2:
                    ifile = netcdf_file(path2)
                    var = ifile.variables['Annual'].data
                ####
               #Cut var so the analysis runs in the same time across all models (for ease of comparison) ie. from 1954-2100
                if exp == 0:
                    t_diff = 1954 - R0_start[mod]
                else:
                    t_diff = 1954 - R1_start[mod]
                var_cut = var[t_diff:,:,:]
                ntim = var_cut.shape[0]# number of years in analysis

                #Isolate regional coordinates
                if (min(lons) >= 0) & (max(lons)>180) :
                    new_lonmin = lon_min[reg]+180
                    lon_min[reg]=new_lonmin
                    new_lonmax = lon_max[reg]+180
                    lon_max[reg]=new_lonmax

                mask_ind_lats = np.where((lats < lat_max[reg]) & (lats > lat_min[reg]))[0] 
                mask_ind_lons = np.where((lons > lon_min[reg]) & (lons <  lon_max[reg]))[0]
                var_cut_reg = var_cut[:, mask_ind_lats,:]
                var_cut_reg = var_cut_reg[:, :, mask_ind_lons]

                #Mask missing values (entered as -9.99900024e+02)
                var_cut_reg = ma.array(var_cut_reg, mask = np.less(var_cut_reg, -998))
                ####
                #Calculate average regional data
                if mod == 0:
                    var_mean = np.ndarray((ntim, len(models)))
                #first need to flatten lat and lon arrays so np.ma.mean can be used(means can only be calculated over one axis)
                var_cut_reg_reshaped = np.reshape(var_cut_reg, (ntim, len(mask_ind_lats) * len(mask_ind_lons)))
                var_mean[:, mod] = np.ma.mean(var_cut_reg_reshaped, axis=1)
            #Calculate the ensemble average
            var_mean_ens = np.ndarray(ntim)
            var_mean_ens = np.mean(var_mean, axis = 1)

            #after looping through models, save the means from the experiment
            if exp == 0:
                mod_means = np.ndarray((ntim, len(experiments)))
            mod_means[:, exp] = var_mean_ens

        #draw a time series for each index
     #   print mod_means[:,0]
     #   print mod_means[:,1]
     #   print mod_means[:,2]
     #   sys.exit()
        x = np.linspace(1954, 2101, 147)
        rm = 'TRUE' #change rolling mean window
        mod_means_0 = mod_means[:,0]
        mod_means_0 = pd.stats.moments.rolling_mean(mod_means_0, 30, 30, center=rm)
        #fit0 = np.polyfit(x,mod_means_0,1)
        #p0 = np.poly1d(fit0)
        mod_means_1 = mod_means[:,1]
        mod_means_1 = pd.stats.moments.rolling_mean(mod_means_1, 30, 30, center=rm)
        #fit1 = np.polyfit(x,mod_means_1,1)
        #p1 = np.poly1d(fit1)        
        mod_means_2 = mod_means[:,2]
        mod_means_2 = pd.stats.moments.rolling_mean(mod_means_2, 30, 30, center=rm)
        #fit2 = np.polyfit(x,mod_means_2,1)
        #p2 = np.poly1d(fit2)

        plt.plot(x, mod_means_0, ls='-',c='k',lw=2)
        #plt.plot(x,p0(mod_means_0),ls='--',c='k',lw=1)
        plt.plot(x, mod_means_1, ls='-',c='r',lw=2)
        #plt.plot(x,p1(mod_means_1),ls='--',c='r',lw=1)
        plt.plot(x, mod_means_2, ls='-',c='b',lw=2)
        #plt.plot(x,p2(mod_means_2),ls='--',c='b',lw=1)
        plt.title('%s'%indices[ind] + ' ensemble average over time in %s'%regions[reg] + ', 30 yr rolling mean')
        #axes
        if indices[ind]=='TXx' :
            plt.axis([1950, 2100, 0, 50])
        elif indices[ind]=='TNx' :
            plt.axis([1950, 2100, 0, 35])
        elif indices[ind]=='WSDI':
            plt.axis([1950, 2100, 0, 160])
        elif indices[ind]=='R95p':
            plt.axis([1950, 2100, 0, 240])
        elif indices[ind]=='R10mm':
            plt.axis([1950, 2100, 0, 40])
        elif indices[ind]=='CDD':
            plt.axis([1950, 2100, 0, 150])
        elif indices[ind]=='CWD':
            plt.axis([1950, 2100, 0, 30])
        else:
            plt.axis([1950, 2100, 0, 80])
        plt.legend(['CTL', 'GC1A85', 'GC1B85'], loc=2, fontsize = 12)
       # mod1 = plt.plot([])
        plt.savefig(outdir+ '%s'%  indices[ind]+ '_' + '%s'%regions[reg] + '_TSEXP' + '.pdf')
        plt.close()
