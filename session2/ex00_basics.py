#!/usr/bin/python

'''
A clean source file starts with some lines explaining its purpose and its creator

JFE - 19 Mar 2013
This file is the first file used during 22 March 2013 Python sessions.

Three quotation marks allows to write comments on several lines. They also allow 
to use quotation marks "" or '' within a comment or a string variable.
'''

# Anything written after a hash is a comment and not treated by the interpreter

# Avoid commenting at the end of a line, prefer comments above it

# We can then import a library, e.g. os
import os, sys
# We can also import and give a nick if the library name is too long
import numpy as np
# We can import a specific part of a library and assign it a nick as well
from datetime import datetime as dt



#Example

# Assign a value to the variable a
a=1 

# Print the variable
print a

# Assign  values to several variables
b,c=2,3
# Several statements on one line
d=4;e=5

print b,c,d,e

#
# Operators to compare are: ==, >, <, >=, <=, !=,
#

print d!=e

#Python is case sensitive
print B

