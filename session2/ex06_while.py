#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents the "while" loops in Python

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

##      
## WHILE
##

#
# The "while" loop continues until a condition is satisfied
#

ii=0
print 'Before loop ii=',ii

#
# A loop is not contained in brackets by is controlled by the indentation
#
while ii<10:
    ii+=1
print 'After loop ii=',ii
raw_input('\nPress <enter> to continue\n')


#
# One can exit a while loop with a break statement'
#
jj=0
print 'Before loop jj=',jj
while jj<10:
    jj+=1
    break
print 'After loop jj=',jj

