#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents the different classes of variables in plain Python and how to
change it. 

raw_input is just used to make the script pause until the <enter> key has been hit.
'''

##                   
## Basic data types 
##                    

#
#1st integer
#
a=1
print a, type(a)
raw_input('\nPress <enter> to continue\n')

#
#2nd float
#
b=3.65
print b, type(b)
raw_input('\nPress <enter> to continue\n')

#
#3rd boolean
#
c=True
print c,type(c)
raw_input('\nPress <enter> to continue\n')

#
#4th string
#
d='Some text'
print d,type(d)
raw_input('\nPress <enter> to continue\n')

#
#5th long integer
#
e=19840813L
print e,type(e)
raw_input('\nPress <enter> to continue\n')

#6th complex
f=2+3j
print f,type(f)
raw_input('\nPress <enter> to continue\n')


##                     
## Changing data types 
##                      

#
# int to float
#
a1=float(a)
print a1, type(a1)
raw_input('\nPress <enter> to continue\n')

#
# float to int
#
b1=int(b)
print b1, type(b1)
raw_input('\nPress <enter> to continue\n')

#
# number to string
#
b2=str(b)
print b2, type(b2)
raw_input('\nPress <enter> to continue\n')

#
# string to number
#
b3=float(b2)
print b3, type(b3)


