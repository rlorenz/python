#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents the "if" loops in Python

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

##      
## IF / ELIF / ELSE
##

#
# The if/elif/else loop test for conditions is satisfied'
#

age=int(input('Enter your age\n> '))

#First condition
if age > 40:
    print 'You are older than 40.'
#else if statement: if age not
elif 30<age<=40:
    print 'You are between 30 and 40.'
else:
    print 'You are younger than 30.'
raw_input('\nPress <enter> to continue\n')
    
#
# One line version of if/else
#  
text = 'You are older than 40.' if age > 40 else 'You are not older than 40.'
print text





