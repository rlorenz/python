#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents how to define a function in Python

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

##
## FUNCTIONS
##

#
# Functions take arguments and return a result
#
def my_age(x):
    'This function returns a nice looking string with your age'
    return 'My age is %2d' % x

print my_age(28)
raw_input('\nPress <enter> to continue\n')   

print my_age.__doc__, '\n'
raw_input('\nPress <enter> to continue\n')   

#
# One can assign a default value to an argument
#
def my_default_age(x=15):
    '''
    This function returns a nice looking string with your age.
    If no argument is given, it returns the default value.
    '''
    return 'My age is %2d' % x
     
print my_default_age()
print my_default_age(28)
raw_input('\nPress <enter> to continue\n')   

print my_default_age.__doc__, '\n'
raw_input('\nPress <enter> to continue\n')   

#
# One can imagine much complicated functions...
#
def m_triffid(smois, wilt=0.2):
    '''
    TRIFFID soil moisture-respiration function
    See Table S2 of Falloon et al., 2011 GBC 10.1029/2010GB003938
    '''

    sopt=0.5*(1.0+wilt)             
    if smois>sopt:
        mdf=1.0-0.8*(smois-sopt)    
    elif wilt<=smois and smois<=sopt:
        mdf=0.2+0.8*((smois-wilt)/(sopt-wilt))
    elif smois<wilt:
        mdf=0.2
    return mdf
    
print m_triffid(.4)
print m_triffid(.6,.3)
raw_input('\nPress <enter> to continue\n')

#
# One can change the order of the argument by explicitly naming them 
#
print m_triffid(wilt=.5,smois=.2)


