#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents the Python string object and useful methods

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

##
## STRINGS 
##

#
# Strings are similar to list of characters
#
abc='abcdef'
print abc[0],abc[-1]
raw_input('\nPress <enter> to continue\n')   

#
# Special characters \n \t
#
abc='a\nb'
print abc
raw_input('\nPress <enter> to continue\n')   

abc='a\tb'
print abc
raw_input('\nPress <enter> to continue\n')

#
# strings can be concatenated 
#
ab='ab'
cd='cd'
charact=ab+cd
print charact
raw_input('\nPress <enter> to continue\n')


#
# string objects contain useful methods
#

#
#split returns a list of elements separated by a chosen delimiter
#
abc='a;b;c;d;e;f'
print abc.split(';')
raw_input('\nPress <enter> to continue\n')

#
#strip returns the same string without leading and ending spaces or \n, \t
#
abc='abcdef     \n'
print abc,len(abc)
print abc.strip(), len(abc.strip())
raw_input('\nPress <enter> to continue\n')

#
# One can test whether a string starts or ends with a specific character
#
abc='abcdef'
print abc.startswith('a')
print abc.startswith('f')
print abc.endswith('a')
print abc.endswith('f')
raw_input('\nPress <enter> to continue\n')

#
# One can easily format integer and floats in a string 
#
long_name='a-very-long-name'
print '%s-and-what-follows' % (long_name)
print '%5.3f' % (3.1567456)
print '%5d' % (13)
print '%05d' % (13)
print '%014.5e' % (13)
raw_input('\nPress <enter> to continue\n')
#
# Try yourself to do more
#
print abc.__doc__, '\n' 
print dir(abc)

