#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents operations with integers in plain Python. 

raw_input is just used to make the script pause until the <enter> key has been hit.
'''

#
# First, define integers
#
a=6
b=8
print 'a =',a,'; b =',b
raw_input('\nPress <enter> to continue\n')

#
# Second, perform some operations
#
print 'sum', a+b
print 'difference', a-b
print 'product', a*b
print 'power', a**b  
raw_input('\nPress <enter> to continue\n')

#
#/!\ DIVISION WITH INTEGERS ONLY RETURNS THE INTEGER PART /!\
#
print 'a/b', a/b
print 'b/a', b/a
print 'rest', b%a
raw_input('\nPress <enter> to continue\n')

#
# You can transform one of the variable to a float
#
print float(a)/b
raw_input('\nPress <enter> to continue\n')

#
# Generally any operation involving a float and an integer returns a float
#
c=a+1.5
print c,type(c)


