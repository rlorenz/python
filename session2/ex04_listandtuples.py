#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents two types of data containers in plain Python: list and tuple.

raw_input is just used to make the script pause until the <enter> key has been hit.
'''

##
## Lists
##

#
#create a list with square brackets []
#
a=[1,2,3,4,5,6,7,8,9]
print a, type(a)
raw_input('\nPress <enter> to continue\n')

#
#/!\ A LIST IS NOT AN ARRAY OR MATRIX /!\
#
print a*2
raw_input('\nPress <enter> to continue\n')

#
# One can query the sum and the length of a list'
#
print len(a)
print sum(a)
raw_input('\nPress <enter> to continue\n')

#
# Accessing elements of a list: python indexing starts with 0'
#
print a[1]
print a[0]
print a[-1]
print a[3:6]
raw_input('\nPress <enter> to continue\n')

#
# One can replace elements in a list by any type of data'
#
a[0]=5
print a[0]
a[4]=['another','list','with','some','elements']
print a[4]
raw_input('\nPress <enter> to continue\n')

#
# One can also add data to a list
#
a.append(2013)
print 'a[-1]=',a[-1]
raw_input('\nPress <enter> to continue\n')

#
# One can reverse the order
#
a.reverse()
print a 
raw_input('\nPress <enter> to continue\n')

#
# One can sort the elements of a list
#
a.sort()
print a
raw_input('\nPress <enter> to continue\n')

#
# One can query the position of an element
#
print a.index(6)
raw_input('\nPress <enter> to continue\n')

#
# One can do much more!
#
print a.__doc__, '\n'
print dir(a)
raw_input('\nPress <enter> to continue\n')


##
## Tuples
##

#
# Create a tuple with brackets ()
#
b=(1,2,3,4,5,6,7,8,9)
print b, type(b)
raw_input('\nPress <enter> to continue\n')

#
# One can transform a tuple into a list and vice-versa
#
b=list(b)
a=tuple(a)
print a,type(a)
print b,type(b)

#Tuples are basically list that you cannot modify
#Try yourself: a[0]=20



