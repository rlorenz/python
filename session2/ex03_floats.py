#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents operations with floats in plain Python.

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

#
#First define two floats
#
a=1.5
b=-2.3
print 'a =',a,'; b =',b
raw_input('\nPress <enter> to continue\n')

#
#Second perform easy operations
#
print 'sum', a+b
print 'difference', a-b
print 'product', a*b
print 'power', a**b
raw_input('\nPress <enter> to continue\n')

#
#Python provides the integer ratio corresponding to a float
#
print 'integer ratio', (a**b).as_integer_ratio()
raw_input('\nPress <enter> to continue\n')

#
# Explore more yourself...
#
print a.__doc__, '\n' 
print dir(a)


