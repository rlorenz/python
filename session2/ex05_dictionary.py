#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents one of the best features of Python: the dictionary

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

#
# Create a dictionary with curly brackets
#
robert={}
print robert, type(robert)

#
#A dictionary is data container that is indexed by keys rather than numbers
#Anything can be a key except lists because you can modify them
#
robert['variable']=4
robert[65]=range(10)
robert[3.14]='pi'
robert[(0,1,3)]='something'
print 'robert=', robert
raw_input('\nPress <enter> to continue\n')

#
# You can get a list of the keys by using the method keys()
#
print robert.keys()
raw_input('\nPress <enter> to continue\n')

#
# Explore more yourself...
#
print robert.__doc__, '\n' 
print dir(robert)
