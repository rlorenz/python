#!/usr/bin/python

'''
JFE - 21 Mar 2013
This file presents the "for" loops in Python

raw_input is just used to make the script pause until the <enter> key has been hit. 
'''

##      
## FOR
##

#
# The for loop iterates over a range of values
#

a=[11,12,13,14,15,16,17,18,19]
print a
raw_input('\nPress <enter> to continue\n')   

#
# Easy for
#

for value in a:
    print value
raw_input('\nPress <enter> to continue\n')   

#
# Iterate by index
#

idlist=range(len(a))
print idlist
raw_input('\nPress <enter> to continue\n')  

for ii in idlist:
    print a[ii] 
raw_input('\nPress <enter> to continue\n')  

#
# Iterate by index and return value
#

for ii,value in enumerate(a):
    print ii,value
