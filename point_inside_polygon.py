#!/usr/bin/python
'''
File Name : point_inside_polygon.py
Creation Date : 07-11-2015
Last Modified : Wed 25 Nov 2015 12:08:55 AEDT
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : function to determine if a point is inside a polygon
	for regional analysis e.g. SREX regions
'''

def point_inside_polygon(lon,lat,border):
	n = len(border)
	inside=False
	p1lat,p1lon=border[0]

	for i in range(n+1):
		p2lat,p2lon=border[i % n]
		if lon > min(p1lon,p2lon):
			if lon <= max(p1lon,p2lon):
				if lat <= max(p1lat,p2lat):
					if p1lon != p2lon:
						xinters = (lon-p1lon)*(p2lat-p1lat)/(p2lon-p1lon)+p1lat
					if p1lat == p2lat or lat <= xinters:
						inside = not inside
		p1lat,p1lon = p2lat,p2lon
	    
	return inside
