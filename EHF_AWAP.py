#!/usr/bin/env python
"""EHF_AWAP.py script
Author: Daniel Argueso (d.argueso@unsw.edu.au) at CoECSS, UNSW, and CCRC, UNSW. Australia.

Script to calculate Excees Heat Factor heatwaves (Perkins and Alexander, 2012 JCLIM; Perkins et al.,2012 GRL)
It takes AWAP daily files (minimum and maximum temperature) and gives a netcdf with 5 characteristics of heatwaves:


Yearly maximum heatwave intensity 
Yearly average heatwave intensity 
Yearly number of heatwave days
Yearly number of heatwaves
Duration of yearly longest heatwave

It also provides also: 

90th percentile of mean temperature for each calendar day using a n-day window

Script based on Sarah's matlab script EHF_index.m


Created: 09 September 2013
Modified: 15 October 2013: The computation cannot be performed over the entire domain at ones. It is carried out by latitude (one line at a time)
"""

import numpy as np
import datetime as dt
import netCDF4 as nc
from constants import const
from compute_EHFheatwaves import compute_EHF
import glob as glob
import sys



#### USER MODIFICATION #####
#dir_in="/home/z3393020/Analyses/AWAP/AWAP0.5deg/"
dir_in="/home/z3393020/Analyses/AWAP/downloaded/DAILY"
RCM=""
GCM="AWAP"
#simid="AWAP0.05deg"
simid="AWAP0.05deg"
syear=1990
eyear=2009
syfile=1990
eyfile=2009
dir_out="/home/z3393020/Analyses/AWAP/heatwaves/EHF/"

### BASE PERIOD ####
# using a base period for the calculation of the percentiles that is different from the main period
# a threshold file over that period must be provided.
sy_base=1990 
ey_base=2009
seas='summer'
modified=True #Either using Sarah's or the original version. Differences in the calculation of percentiles
#thfile='/home/z3393020/Analyses/AWAP/heatwaves/EHF/CCRC_NARCliM_1961-1990_EHFheatwaves_summer_AWAP0.5deg.nc'
thfile=None

if ((sy_base!=syear) or (ey_base!=eyear)) and thfile==None:
  sys.exit('For a base period different from the computation period a threshold file is required \n It wasn\'t provided. Please provide a threshold file')

###########################

# def sel_files(filelist,syear,eyear):
#     years=np.array([fname[-7:-3] for fname in filelist],np.int64)
#     selec_files=[filelist[i] for i,n in enumerate(years) if  ((n>= syear) & (n<= eyear))]
#     return selec_files
# 
# files_min=sorted(glob.glob("%s/AWAP_TX_1911-2011_0.5deg.nc" %(dir_in)))
# files_max=sorted(glob.glob("%s/AWAP_TN_1911-2011_0.5deg.nc" %(dir_in)))
# 
# filesn=sel_files(files_min,syear,eyear)
# filesx=sel_files(files_max,syear,eyear)
# 
# fileref=nc.Dataset(filesx[0],'r')
# lat=fileref.variables['lat'][:]
# lon=fileref.variables['lon'][:]
# 
# filen=nc.MFDataset(filesn,'r')
# filex=nc.MFDataset(filesx,'r')

# filemask=nc.Dataset("%s/AWAP_Land-Sea-Mask_0.5deg.nc" %(dir_in))
# lm=filemask.variables['LSM'][:]
# filen=nc.Dataset("%s/AWAP_TN_1911-2013_0.5deg.nc" %(dir_in))
# filex=nc.Dataset("%s/AWAP_TX_1911-2013_0.5deg.nc" %(dir_in))
# lat=filex.variables['lat'][:]
# lon=filex.variables['lon'][:]

# 
# filemask=nc.Dataset("/home/z3393020/Analyses/AWAP/Masks/AWAP_Land-Sea-Mask_0.5deg.nc")
# lm=filemask.variables['LSM'][:]

filemask=nc.Dataset("/home/z3393020/Analyses/AWAP/Masks/AWAP_Land-Sea-Mask_0.05deg.nc")
lm=filemask.variables['LSM'][:]

filen=nc.Dataset("%s/tmin.1990-2009.nc" %(dir_in))
filex=nc.Dataset("%s/tmax.1990-2009.nc" %(dir_in))
lat=filex.variables['lat'][:]
lon=filex.variables['lon'][:]


#############################

d1 = dt.datetime(syfile,1,1,0,0)
d2 = dt.datetime(eyfile,12,31,0,0)
diff = d2 - d1
dates=np.asarray([d1 + dt.timedelta(i) for i in range(diff.days+1)])
years=np.asarray([dates[i].year for i in xrange(len(dates))])

dates=dates[(years>=syear) & (years<=eyear)]
nyears=eyear-syear+1



#################################################
########### WRITTING OUT NCFILES#################
#################################################
datesout= [dt.datetime(syear+x,06,01,00) for x in range(0,nyears)]
time=nc.date2num(datesout,units='days since %s' %(nc.datetime.strftime(dt.datetime(syear+x,01,01,00), '%Y-%m-%d_%H:%M:%S')),calendar='standard')

print '%s/CCRC_NARCliM_%s-%s_EHFheatwaves_%s_%s%s.nc'%(dir_out,syear,eyear,seas,RCM,simid)
fout=nc.Dataset('%s/CCRC_NARCliM_%s-%s_EHFheatwaves_%s_%s%s.nc'%(dir_out,syear,eyear,seas,RCM,simid),mode='w')
fout.createDimension('time',len(datesout))
fout.createDimension('DoY',365)
fout.createDimension('lat',lat.shape[0])
fout.createDimension('lon',lon.shape[0])

timeout=fout.createVariable('Times','f8','time',fill_value=const.missingval)
setattr(timeout,'units','days since %s' %(nc.datetime.strftime(dt.datetime(syear+x,01,01,00), '%Y-%m-%d_%H:%M:%S')))

latout=fout.createVariable('lat','f8',('lat'),fill_value=const.missingval)
setattr(latout,'Longname','Latitude')
setattr(latout,'units','degrees_north')

lonout=fout.createVariable('lon','f8',('lon'),fill_value=const.missingval)
setattr(lonout,'Longname','Longitude')
setattr(lonout,'units','degrees_east')


if modified==True:
  
  prcout=fout.createVariable('PRCTILE90','f8',('DoY','lat','lon'),fill_value=const.missingval)
  setattr(prcout,'Longname','Percentile 90th')
  setattr(prcout,'units','degC')
  setattr(prcout,'description','Percentile 90th for each calendar day using %s-day window' %(str(15)))
else:
  
  prcout=fout.createVariable('PRCTILE95','f8',('DoY','lat','lon'),fill_value=const.missingval)
  setattr(prcout,'Longname','Percentile 95th')
  setattr(prcout,'units','degC')
  setattr(prcout,'description','Percentile 95th over the entire base_period')

HWAout=fout.createVariable('HWA_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWAout,'Longname','Peak of the hottest heatwave per year')
setattr(HWAout,'units','degC2')
setattr(HWAout,'description','Peak of the hottest heatwave per year - yearly maximum of each heatwave peak')

HWMout=fout.createVariable('HWM_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWMout,'Longname','Average magnitude of the yearly heatwave')
setattr(HWMout,'units','degC2')
setattr(HWMout,'description','Average magnitude of the yearly heatwave - yearly average of heatwave magnitude')

HWNout=fout.createVariable('HWN_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWNout,'Longname','Number of heatwaves')
setattr(HWNout,'units','')
setattr(HWNout,'description','Number of heatwaves per year')

HWFout=fout.createVariable('HWF_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWFout,'Longname','Number of heatwave days')
setattr(HWFout,'units','')
setattr(HWFout,'description','Number of heatwave days - expressed as the percentage relative to the total number of days')

HWDout=fout.createVariable('HWD_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWDout,'Longname','Duration of yearly longest heatwave')
setattr(HWDout,'units','days')
setattr(HWDout,'description','Duration of the longest heatwave per year')

HWTout=fout.createVariable('HWT_EHF','f8',('time','lat','lon'),fill_value=const.missingval)
setattr(HWTout,'Longname','First heat wave day of the year')
setattr(HWTout,'units','day')
setattr(HWTout,'description','First heat wave day of the year from 1st of Jan')

timeout[:]=time[:]
latout[:]=lat[:]
lonout[:]=lon[:]


tasmin=filen.variables['tmin'][(years>=syear) & (years<=eyear),:,:]
tasmax=filex.variables['tmax'][(years>=syear) & (years<=eyear),:,:]

#newtasmin=np.ones((len(dates),)+tasmin.shape[1:],dtype=np.float64)*const.missingval
#newtasmax=np.ones((len(dates),)+tasmax.shape[1:],dtype=np.float64)*const.missingval\

#newtasmin[0:tasmin.shape[0],:,:]=tasmin
#newtasmax[0:tasmax.shape[0],:,:]=tasmax

HWA_EHF_yearly,HWM_EHF_yearly,HWF_EHF_yearly,HWN_EHF_yearly,HWD_EHF_yearly,HWT_EHF_yearly,pctcalc=compute_EHF(tasmax,tasmin,dates=dates,modified=modified,thres_file=thfile,season=seas)

pctcalc[:,lm<1]=const.missingval
HWA_EHF_yearly[:,lm<1]=const.missingval
HWM_EHF_yearly[:,lm<1]=const.missingval
HWN_EHF_yearly[:,lm<1]=const.missingval
HWF_EHF_yearly[:,lm<1]=const.missingval
HWD_EHF_yearly[:,lm<1]=const.missingval
HWT_EHF_yearly[:,lm<1]=const.missingval

prcout[:]=pctcalc[:]
HWAout[:]=HWA_EHF_yearly[:]
HWMout[:]=HWM_EHF_yearly[:]
HWNout[:]=HWN_EHF_yearly[:]
HWFout[:]=HWF_EHF_yearly[:]
HWDout[:]=HWD_EHF_yearly[:]
HWTout[:]=HWT_EHF_yearly[:]



# Set global attributes
setattr(fout,"author","Daniel Argueso @ CCRC, UNSW, Australia")
setattr(fout,"contact","d.argueso@unsw.edu.au")
setattr(fout,"creation date",dt.datetime.today().strftime('%Y-%m-%d'))
if thfile!=None:
  setattr(fout,"comment","File generated using the base period %s-%s and using the threshold file: %s" %(sy_base,ey_base,thfile))
else:
  setattr(fout,"comment","File generated using the base period %s-%s and threshold file was generated (it did not exist previously)" %(sy_base,ey_base))

setattr(fout,"Script","EHF_AWAP.py")
setattr(fout,"Input files located in:", dir_in)
fout.close()

    




